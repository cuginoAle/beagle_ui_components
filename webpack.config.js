var path = require('path');
const webpack = require('webpack');



module.exports = {
    entry: {
        gh_pages:__dirname + "/gh_pages/src_javascript/main.js"
    },
    devtool: 'source-map',
    output: {
        path: __dirname + "/gh_pages/javascript",
        filename: "[name].js"
    },
    devtool: 'cheap-module-source-map',

    node:{
        console: false,
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
    },
    module: {
        loaders: [{
            test: /\.js$/,
            exclude: /(node_modules|app.js|build)/,
            loader: 'babel-loader?cacheDirectory'
        }]
    }
};
