import React from 'react';
import {render} from 'react-dom';
import styled from 'styled-components';

import {Tab_menu,ddpConn} from '../../src/index';
import Components_list from './components/components_list';
import Dataloaders_list from './components/dataloaders_list';
import Helpers_list from './components/helpers_list';


// alternatively we can name-space the components using the following syntax:
// import Ui from '@tesco_ux/ui_components';
// es. <Ui.Button />


//global variable to set the symbol-defs used by Svg_icon (yuk)
//this is needed only for this page! 
window.svgFilePath="public/assets/symbol-defs.svg"

let Tabs=[
        {
            id:0,
            caption:"Components",
            className:"components",
            contentToRender:(<Components_list className="tab_content" />)

        },
        {
            id:1,
            caption:"Modules",
            className:"modules",
            contentToRender:null
        },
        {
            id:2,
            caption:"Data loader",
            className:"data",
            contentToRender:(<Dataloaders_list className="tab_content" />)
        },
        {
            id:3,
            caption:"Helpers",
            className:"helpers",
            contentToRender:(<Helpers_list className="tab_content" />)
        }
    ];



class Gallery extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Gallery';
        this.state={
            currentTab:Tabs[0]
        };

        this.setCurrentTab=this.setCurrentTab.bind(this);
    }

    setCurrentTab(tab){
        this.setState({currentTab:tab})
    }

    render() {
        Tabs=Tabs.map(({id,caption,className,contentToRender})=>{
            return {
                id,
                caption,
                className,
                contentToRender,
                onClick:()=>{this.setCurrentTab({id,caption,contentToRender})},
                selected:id==this.state.currentTab.id
            }
        })


        return (
            <div className='gallery'>
                <Tab_menu tabs={Tabs} />
                {(this.state.currentTab.contentToRender)}
            </div>
        )
    }
}


document.addEventListener('DOMContentLoaded', ()=>{
    let wrapper=document.querySelector("#dynamic_content");
    render(<Gallery />,wrapper); 
}, false);

