import React from 'react';

import {
    $,$$,getKey,
    Accordion,
    Badge,
    Button,
    Caption,

    Code_highlighter
} from '../../../src/index.js';

import Section from './section';


function Helpers_list(props){

    let classes=["helpers_list"];
    props.className && classes.push(props.className);

    return (
        <div className={classes.join(" ")}>

            <Section>
                <Badge caption="$" />
                <Code_highlighter keepIndentation>{`
Shorthand for document.querySelector
usage: $(selector,parent)

optional: parent set the serch context (document by default )

};

`}</Code_highlighter>
            </Section>
        </div>
    )
}


export default Helpers_list