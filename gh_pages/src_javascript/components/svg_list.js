import React from 'react';
import styled from 'styled-components';
import {svg} from "../../../src/components/assets/symbol-defs_export.js";
import {Svg_icon,getKey} from "../../../src/index";

function Svg_list(props){
    let Wrapper=styled.div`
            ul {
                display        : flex;
                flex-wrap      : wrap;
                font-size      : 24px;
                justify-content: space-between;
                list-style-type: none;
            }

            li {
                margin    :10px;
                text-align:center;

                .label {
                    background-color:#f0f0f0;
                    display         :block;
                    font-size       :12px;
                    margin          :3px 0;
                    padding         : 1px 5px;
                }
            }
        `,
        classes=["svg_list"];

        props.className && classes.push(props.className);

    
    let ids_list=[];


    svg.split('<symbol id="').forEach((match,i)=>{
        if(i>0){
            ids_list.push(match.split('"')[0])            
        }
    })

    return (
        <Wrapper className={classes.join(" ")} >
            <ul>
                {ids_list.map((icon)=>{
                                    return(
                                        <li key={getKey()}>
                                        <Svg_icon icon={icon} svgFile="public/assets/symbol-defs.svg" />
                                        <span className="label">{icon}</span>
                                        </li>
                                    )
                                })}
            </ul>
        </Wrapper>
    )
}
export default Svg_list