import React from 'react';

import {
    $,$$,getKey,
    Accordion,
    Badge,
    Button,
    Caption,

    Code_highlighter
} from '../../../src/index.js';

import Section from './section';

function Dataloaders_list(props){

    let classes=["components_list"];
    props.className && classes.push(props.className);

    return (
        <div className={classes.join(" ")}>

            <Section>
                <Badge caption="ddpCon" />
                <p>The ddpCon object creates a connection over secure web-socket to the ddpApi server ("wss://nowledge.xyz/websocket")</p>
                <p>It exposes the "exec" method to call methods on the server </p>
                <Code_highlighter keepIndentation>{`
import {ddpConn} from "@tesco_ux/ui_components/ddpConn";


//to log the user in
ddpConn.exec("sign_in",[{email,password}],(message)=>{
    CustomerName=message.result.content.CustomerName;
    ...
});


//to use Athena APIs (see Athena's doc)
let configObj = {
    query: "milk",
    offer: true,
    limit: 10,
    responseSet: [
        'results',
        'totals'
    ]
};
ddpConn.exec("athena",configObj,(err, data)=>{
    if (!err) {
        var productList = _.map(data.content.uk.ghs.products.results, function(item, index) {
        ...
    }else {
        ...
    }

    }
})


`}</Code_highlighter>
            </Section>

            <Section>
                <p>Most of the times you don't need to use the <b>ddpCon</b> object. If you need direct access to the DB collections you can use the <b>Data</b> object:</p>
            </Section>

            <Section>
                <Badge caption="Data" />
                <p><b>Data</b> object exposes the following collections:</p>
                <ul>
                    <li>Products</li>
                    <li>Taxonomies</li>
                    <li>TopKeywords</li>
                </ul>
            </Section>

            <Section>
                <Badge caption="Athena" />
                <p></p>
            </Section>

            <Section>
                <p>The <b>Products</b> collection allows us to search for products and fetch product details. <br/>
                All its methods implement the following params by default:</p>
<pre>{`{ 
    limit :10,
    skip :0
}`}</pre>
                <p>It exposes the following publications:</p>
                <dl>
                    <dt>all</dt>
                    <dd>Returns all the products in the DB</dd>

                    <dt>byKeyword</dt>
                    <dd>Search for products matching the passed keyword</dd>

                    <dt>byIsleId</dt>
                    <dd>Search for products belonging to the passed Isle id</dd>

                    <dt>byShelfId</dt>
                    <dd>Search for products belonging to the passed Shelf id</dd>                    
                    
                </dl>

                <Code_highlighter keepIndentation>{`
import {Data} from "@tesco_ux/ui_components";


Data.Products.all({limit:20,skip:20},(result)=>{
    console.log("Showing page 2");

    let productList=results.map((product)=>{
        return (<Product {product}/>)
    })
    ...
})

Data.Products.byKeyword({keyword:'milk',limit:20},(result)=>{
    console.log("Found " + result.length + " products")
})

Data.Products.byIsleId({isleId:'13'},(result)=>{
    console.log("Found " + result.length + " products")
})

Data.Products.byShelfId({shelfId:'133'},(result)=>{
    console.log("Found " + result.length + " products")
})

                 `}</Code_highlighter>

            </Section>





            <Section>
                <p>The <b>TopKeywords</b> collection allows us to search for the top searched keywords<br/>
                A common use-case would be the auto-suggest feature implementation</p>

                <p>It exposes the following publications:</p>
                <dl>
                    <dt>popularSearches</dt>
                    <dd>Returns all the searched terms containing the passed keyword</dd>                    
                </dl>

                <Code_highlighter keepIndentation>{`
import {Data} from "@tesco_ux/ui_components";


Data.TopKeywords.popularSearches({keyword:'milk',limit:20},(result)=>{
    
    let keywordsList=results.map((keyword)=>{
        return (<p>{Keyword}<p/>)
    })
    ...
})

                 `}</Code_highlighter>

            </Section>


        </div>
    )
}


export default Dataloaders_list;