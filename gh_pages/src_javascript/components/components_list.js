import React from 'react';
import Svg_list from './svg_list';

import {
    $,$$,getKey,
    Accordion,
    Badge,
    Button,
    Caption,
    Checkbox,
    Link,
    Svg_icon,
    Code_highlighter,
    IOS_selector,
    Modal_dialog,
    Tab_menu,
    Drawer,
    Spinner,
    Tesco_logo,
    Message_box,
    SearchBox,
    SearchBoxWithSuggestions,
    Datalist,
    Card
} from '../../../src/index.js';

import Section from './section';

function Components_list(props){

    let classes=["components_list"];
    props.className && classes.push(props.className);
    
    return (

        <div className={classes.join(" ")}>
            <Section>            
                <Tesco_logo svgFile="public/assets/symbol-defs.svg" style={{fontSize:"6em"}}/>
                <Code_highlighter>{`
                    <Tesco_logo style={{fontSize:"6em"}}/>
                `}
                </Code_highlighter>
            </Section>

            <Section>
                <Button caption="Simple button" />
                <div>- or -</div> 
                <Button>
                    <Svg_icon svgFile="public/assets/symbol-defs.svg" icon="calendar2"/>
                    <Caption>Icon & button</Caption>
                </Button>
                <Code_highlighter>{`
                    <Button caption="Simple button" />
            - or -
                    <Button>
                        <Svg_icon svgFile="public/assets/symbol-defs.svg" icon="calendar2"/>
                        <Caption>Icon & button</Caption>
                    </Button>
                `}
                </Code_highlighter>            
            </Section>

            <Section>
                <SearchBox input={{placeholder:"Search..."}} />
                <Code_highlighter keepIndentation>{`
let props={
    input:{
        value:"...",
        placeholder:"Search...",
    },
    datalistId: -> Datalist element ID,
    form:{
        onSubmit:()=>{...}
    }                        
}

<SearchBox {...props}/>
                `}
                </Code_highlighter>

                <br/>
                <p>Showing suggestions through the datalist</p>
                <SearchBoxWithSuggestions input={{placeholder:"Search for products..."}} />
                
                

            </Section>


            <Section>
                <Button type="clear" onClick={()=>{
                        Drawer.show(
                            //content
                            (
                                <Card noborders>
                                    <p>...and this is my content:</p>
                                    <Badge caption="Hello!" />
                                    <br/>
                                    <Button type="clear" onClick={Drawer.close} caption="x" />
                                </Card>
                            ),
                            "left"
                        )
                }}>
                    <Caption>Drawer</Caption>
                    <Svg_icon svgFile="public/assets/symbol-defs.svg" icon="calendar2"/>
                </Button>
                <Code_highlighter keepIndentation>{`
In your Js:
Drawer.show(<Your_component />,from)

from can be either "top" (default) "left", "right" or "bottom"

`}
                </Code_highlighter>            
            </Section>




            <Section>            
                <Spinner/>
                <Code_highlighter>{`
                    <Spinner/>
                `}
                </Code_highlighter>
            </Section>

            <Section>
                <Button type="secondary">
                    <Caption>Secondary button</Caption>
                    <Svg_icon svgFile="public/assets/symbol-defs.svg" icon="calendar2"/>
                </Button>
                <Code_highlighter>{`
                    <Button type="secondary">
                        <Caption>Secondary button</Caption>
                        <Svg_icon svgFile="public/assets/symbol-defs.svg" icon="calendar2"/>
                    </Button>
                `}
                </Code_highlighter>            
            </Section> 

            <Section>
                <Button type="clear">
                    <Svg_icon svgFile="public/assets/symbol-defs.svg" icon="calendar2"/>
                    <Caption>Clear button</Caption>
                </Button>
                <Code_highlighter>{`
                    <Button type="clear">
                        <Svg_icon svgFile="public/assets/symbol-defs.svg" icon="calendar2"/>
                        <Caption>Clear button</Caption>
                    </Button>
                `}
                </Code_highlighter>            
            </Section>            

            <Section>
                <p>This is an icon <Svg_icon svgFile="public/assets/symbol-defs.svg" style={{fontSize:"28px",verticalAlign:"bottom"}} icon="basket" /></p>
                <Code_highlighter style={{marginBottom:"20px"}}>{`
                    <p>This is an icon <Svg_icon svgFile="public/assets/symbol-defs.svg" icon="basket"/></p>
                    `}
                </Code_highlighter>

                <p>Available icons:</p>
                <Svg_list />
            </Section>

            <Section>
                <Accordion caption="I am an accordion...">
                    <div>
                        <p>...and this is my content:</p>
                        <p>Eiusmod veniam do enim qui esse dolore nostrud eu dolore esse reprehenderit eu quis in dolor voluptate labore in nostrud non commodo ad velit in culpa labore consequat excepteur officia in ad sed labore in ex occaecat ut qui fugiat deserunt ullamco labore et sed aliqua veniam occaecat adipisicing tempor dolor ullamco cupidatat commodo et duis incididunt cupidatat est cupidatat irure fugiat non non mollit officia ex sunt veniam nostrud enim in est qui aliquip consequat esse ea magna elit id irure laboris consequat aliqua in exercitation et sint minim deserunt et sunt labore qui nostrud aliqua reprehenderit sed ut nisi consectetur eu deserunt esse esse nisi cillum ut do aliqua sunt in nostrud commodo excepteur in dolore sint est sed voluptate amet anim duis sunt laborum sed labore irure dolore nisi veniam et labore reprehenderit enim dolor ut dolore occaecat eu exercitation excepteur proident consectetur elit esse sed do ea dolore et occaecat laborum commodo magna ut sunt laborum sed in in est officia amet anim in ad fugiat tempor elit laboris ad consequat nostrud proident deserunt excepteur esse veniam proident qui dolore occaecat nostrud dolor commodo quis tempor voluptate cillum nostrud elit et sit nostrud in in minim adipisicing labore magna in ut velit aliquip reprehenderit adipisicing sit est nulla in aliquip eiusmod reprehenderit elit ut ad ut eiusmod aliquip aliqua esse sint tempor sunt dolor excepteur.</p>                    
                    </div>
                </Accordion>
                <Code_highlighter>{`
                    <Accordion caption="I am an accordion...">
                        <div>
                            <p>...and this is my content:</p>
                            <p>Eiusmod veniam do enim qui esse dolore nostrud eu dolore esse reprehenderit eu quis in dolor voluptate labore in nostrud non commodo ad velit in culpa labore consequat excepteur officia in ad sed labore in ex occaecat ut qui fugiat deserunt ullamco labore et sed aliqua veniam occaecat adipisicing tempor dolor ullamco cupidatat commodo et duis incididunt cupidatat est cupidatat irure fugiat non non mollit officia ex sunt veniam nostrud enim in est qui aliquip consequat esse ea magna elit id irure laboris consequat aliqua in exercitation et sint minim deserunt et sunt labore qui nostrud aliqua reprehenderit sed ut nisi consectetur eu deserunt esse esse nisi cillum ut do aliqua sunt in nostrud commodo excepteur in dolore sint est sed voluptate amet anim duis sunt laborum sed labore irure dolore nisi veniam et labore reprehenderit enim dolor ut dolore occaecat eu exercitation excepteur proident consectetur elit esse sed do ea dolore et occaecat laborum commodo magna ut sunt laborum sed in in est officia amet anim in ad fugiat tempor elit laboris ad consequat nostrud proident deserunt excepteur esse veniam proident qui dolore occaecat nostrud dolor commodo quis tempor voluptate cillum nostrud elit et sit nostrud in in minim adipisicing labore magna in ut velit aliquip reprehenderit adipisicing sit est nulla in aliquip eiusmod reprehenderit elit ut ad ut eiusmod aliquip aliqua esse sint tempor sunt dolor excepteur.</p>                    
                        </div>
                    </Accordion>
                `}
                </Code_highlighter> 
            </Section>


            <Section>
                <Button type="clear" onClick={()=>{
                        Modal_dialog.show({
                            //content
                            children:
                                (
                                    <Card noborders>
                                        <p>...and this is my content:</p>
                                        <Badge caption="Hello!" />
                                    </Card>
                                ),
                            align:"center",                           //align [center || bottom]
                            onCLose:()=>console.log("closed!"),         //[onClose callback]
                            onBgClick:()=>Modal_dialog.close()            //[on BG click]
                        })
                }}>
                    <Caption>Modal dialog</Caption>
                    <Svg_icon svgFile="public/assets/symbol-defs.svg" icon="calendar2"/>
                </Button>
                <Code_highlighter keepIndentation>{`
In your Js:
Modal_dialog.show({children:<Your_component />,align,onClose,onBgClick})
            
align can be either "bottom" (default) or "center"

`}
                </Code_highlighter>            
            </Section> 

            
            <Section>
                <Button type="clear" onClick={()=>Message_box.show(<Caption style={{color:"white"}}>Hello, I am a Message box! {(new Date()).getTime()}</Caption>,5000,()=>console.log("closed"))}>
                    <Caption>Message box</Caption>
                    <Svg_icon svgFile="public/assets/symbol-defs.svg" icon="pin"/>
                </Button>
                <Code_highlighter keepIndentation>{`
In your Js:
Message_box.show(<Your_component />,ms,onClose)

I.e.
Message_box.show(<Caption>Hello, I am a Message box!</Caption>,5000)

The optional ms param sets the milliseconds after which the message-box closes it self.
If defined, onClose is triggered after the message-box has slided away.

`}
                </Code_highlighter>   
            </Section>

            <Section>
                <IOS_selector height="4" title="Choose an option" entries={["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]} initialPosition="2" />
                <Code_highlighter>{`
                    <IOS_selector height="4" title="Choose an option" entries={["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]} initialPosition="2" />                        
                `}
                </Code_highlighter>
            </Section>

            <Section>
                <Badge caption="I am a badge!" />
                <Code_highlighter>{`
                    <Badge caption="I am a badge!" />
                `}
                </Code_highlighter>
            </Section>

            <Section>
                <Checkbox label="Please check me out ;)" />
                <Code_highlighter>{`
                    <Checkbox label="Please check me out ;)" />
                `}
                </Code_highlighter>                
            </Section>

            <Section>
                <Link caption="I am a link" href="#link" icon="right" />
                <Code_highlighter>{`
                    <Link caption="I am a link" href="#link" icon="right" />
                `}
                </Code_highlighter>                
            </Section>

            <Section>
                <Card removable>
                    <p>This is a card... <br/>Basically it is a box showing a border with rounded corners!</p>
                    <p>If you specify "removable" you'll see a nice Hide button on the top-right corner.</p>
                    <p>"noborders" will... well, hide the borders!</p>
                </Card>
                <Code_highlighter>{`
                    <Card removable>
                        <p>This is a card... <br/>Basically it is a box showing a border with rounded corners!</p>
                        <p>If you specify "removable" you'll see a nice Hide button on the top-right corner.</p>
                    </Card>
                `}
                </Code_highlighter>                
            </Section>

            <Section>
                <Card theme="green">
                    <p>It also comes in green....</p>
                </Card>
                <Code_highlighter>{`
                    <Card theme="green">
                        <p>It also comes in green....</p>
                    </Card>
                `}
                </Code_highlighter>                
            </Section>

            <Section>
                <Card theme="yellow">
                    <p>... or yellow. What a perfect match with your socks!</p>
                </Card>
                <Code_highlighter>{`
                    <Card theme="yellow">
                        <p>... or yellow. What a perfect match with your socks!</p>
                    </Card>
                `}
                </Code_highlighter>                
            </Section>
        </div>
    )
}



export default Components_list;