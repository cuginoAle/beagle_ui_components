import styled from 'styled-components';
import React from 'react';

function Section(props){
    let Wrapper=styled.div`
            border:1px solid #eee;
            boxShadow:rgba(0, 0, 0, 0.1) 0px 9px 12px -6px;
            margin:20px;
            padding:20px;
            backgroundColor:#fff;
            
            .code_highlighter {
                margin-top:20px;
            }
        `,
        classes=["section"];

    props.className && classes.push(props.className)

    return (
        <Wrapper className={classes.join(" ")}>
            {props.children}
        </Wrapper>
    )
}

export default Section