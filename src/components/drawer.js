import React from 'react';
import {render} from 'react-dom';
import Svg_icon from './svg_icon';

import styled from 'styled-components';

function Drawer(){

    let Wrapper=styled.div`
            background-color:rgba(0, 69, 119, 0.87);
            box-sizing      :border-box;            
            position        :fixed;
            transition      :transform .3s;
            z-index         :990;

        `,
        onCloseCB=null;

    var div=document.createElement("div");

    div.addEventListener("webkitTransitionEnd", ()=>{  
        if(!div.classList.contains("show")){
            document.documentElement.removeChild(div);
        }
    })

    var _showContent=(children,from="top")=>{

        let PositionedWrapper=null;

            switch(from){
                case "top":
                    PositionedWrapper=styled(Wrapper)`
                        top       :0;
                        left      :0;
                        transform :translateY(-100%);
                        width     :100%;
                        box-shadow:0 1px 4px rgba(0,0,0,.5);

                        .show &{
                            transform:translateY(0);
                        }
                    `;
                break;

                case "bottom":
                    PositionedWrapper=styled(Wrapper)`
                        bottom   :0;
                        left     :0;
                        transform:translateY(100%);
                        width    :100%;
                        box-shadow:0 -1px 4px rgba(0,0,0,.5);

                        .show &{
                            transform:translateY(0);
                        }
                    `;

                break;

                case "left":
                    PositionedWrapper=styled(Wrapper)`
                        left     :0;
                        top      :0;
                        transform:translateX(-100%);
                        height   :100%;
                        box-shadow:1px 0 4px rgba(0,0,0,.5);

                        .show &{
                            transform:translateX(0);
                        }
                    `;
                break;

                case "right":
                    PositionedWrapper=styled(Wrapper)`
                        right    :0;
                        top      :0;
                        transform:translateX(100%);
                        height   :100%;
                        box-shadow:-1px 0 4px rgba(0,0,0,.5);

                        .show &{
                            transform:translateX(0);
                        }
                    `;
                break;
            }

        render(<PositionedWrapper className="drawer" >{children}</PositionedWrapper>,div)

        document.documentElement.appendChild(div);
        setTimeout(()=>{
            div.classList.add("show");
        },20);
    }


    var msgObj={
        show:(children,from="top",onClose=false)=>{
            _showContent(children,from)
            onClose && (onCloseCB=onClose);
        },
        close:()=>{
            onCloseCB && onCloseCB();
            div.classList.remove("show");
        }
    }

    return msgObj;
}

export default Drawer();