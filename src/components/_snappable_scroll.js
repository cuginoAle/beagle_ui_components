import React from 'react';
import _deb from "lodash.debounce";
import animate from './js/animate.js';
import Impetus from 'impetus';

//[.5,0,.5,1.5]


// this component renders a scrollable container
// the scroll will snap to the correct step on scrollEnd 
class _snappable_scroll extends React.Component {
    constructor(props) {
        
        super(props);
        this.displayName = '_snappable_scroll';

        var self=this,
            animation=props.animation||{};

        self.touchend=false;
        self.anim=null

        this.transformComponent=this.transformComponent.bind(this);


        this.snapToPosition=_deb((value)=>{
            if(self.touchend){

                var selectedIndex=Math.abs(Math.round(value/props.stepHeight)),
                    newTop=(-selectedIndex*props.stepHeight) - value;

                //executing the callback... or logging the selected index
                this.props.onChange?this.props.onChange(selectedIndex):console.log(selectedIndex);

                self.anim=animate({
                    curve:animation.curve,
                    duration:animation.duration,
                    callback:(perc)=>{

                        //updating Impetus
                        self.impetus.setValues(0,value + newTop*perc);

                        //translating the element
                        self.elem.style.transform="translateY(" + (value + newTop*perc) + "px)"
                    
                    }
                })                
            }
        },70)
    }

    transformComponent(x,y){
        
        if(this.elem){

            this.anim && this.anim.cancel();

            this.elem.style.transform="translateY(" + y + "px)" ;
            this.snapToPosition(y)        
        }
    }

    componentDidMount() {
        let self=this,
            elem=self.elem,
            maxX=0;

        //setting the initial position
        let initialPosition=-self.props.initialPosition * self.props.stepHeight;
        this.transformComponent(null,initialPosition);

        setTimeout(function(){
            //forcing the browser to get the calculation right!
            elem.scrollHeight && elem.parentNode.offsetHeight;

            maxX=elem.parentNode.scrollHeight - elem.parentNode.offsetHeight;

            //making sure we can scroll to the bottom of the component
            maxX=Math.ceil(maxX/self.props.stepHeight)*self.props.stepHeight;


            self.impetus=new Impetus({
                source:elem,
                friction:.94,
                boundY:[-maxX,0],
                bounce:true,
                initialValues:[0,initialPosition],
                update:self.transformComponent
            })

            
        },20)

        elem.parentNode.addEventListener('touchmove', function(e){            
            //preventing the page from scrolling
            e.preventDefault();            
        });

        elem.parentNode.addEventListener("touchstart",function(e){
            self.touchend=false;
        })
        elem.parentNode.addEventListener("mousedown",function(e){
            self.touchend=false;
        })


        elem.parentNode.addEventListener("touchend",function(e){
            self.touchend=true;
            var newTop=parseInt(self.elem.style.transform.replace("translateY(",""),10);
            self.snapToPosition(newTop);

        })

        elem.parentNode.addEventListener("mouseup",function(e){
            self.touchend=true;
            var newTop=parseInt(self.elem.style.transform.replace("translateY(",""),10);
            self.snapToPosition(newTop);

        })        

    }


    render() {
        let classes=["_snappable_scroll"];

        this.props.className && classes.push(this.props.className);

        return (
            <div ref={(elem)=>{this.wrapper=elem}} style={this.props.style} className={classes.join(" ")} >
                <div ref={(elem)=>{this.elem=elem}} className="draggable_content">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

_snappable_scroll.defaultProps={
    initialPosition:0
}

export default _snappable_scroll;


