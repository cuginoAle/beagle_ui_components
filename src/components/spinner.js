import React from 'react';
import styled, {keyframes} from 'styled-components';

function Spinner(props){
    let spinner=keyframes`
        to {transform: rotate(360deg);}
    `;
    
    let Wrapper=styled.div`
    
        .spinning_thing{
            position:relative;
            height:1em;
            font-size:2em;

            &:before {
                content: '';
                box-sizing: border-box;
                position: absolute;
                top: 50%;
                left: 50%;
                width: 1em;
                height: 1em;
                margin-left:-.5em;
                margin-top:-.5em;
                border-radius: 50%;
                border: 3px solid rgba(0,0,0,.06);
                border-top-color: rgba(0, 75, 144, 0.58);
                animation: ${spinner} .6s linear infinite;
            }                          
        }

        p {
            margin-top:10px;
            font-weight:200;
            font-size:.8em;
            text-align:center;
        }
    `;
    return (
        <Wrapper className="spinner">
            <div className="spinning_thing"/>
            {props.message && (<p>{props.message}</p>)}
        </Wrapper>
    )
}

export default Spinner