import React from 'react';
import styleVars from './js/styleVars.js';

function Badge(props){
    let classes=["crx_badge"];

    props.className && classes.push(props.className);

    let style={
            backgroundColor: styleVars.colors.yellow8,
            padding        : "5px 20px",
            color          : "white",
            fontWeight     : "400",
            borderRadius   : "100px",
            display        : "inline-block"
        };    

    let badgeStyle=Object.assign(style,props.style);

    return (
        <span id={props.id} className={classes.join(" ")} style={badgeStyle}>{props.caption}</span>
    );
}

export default Badge;
