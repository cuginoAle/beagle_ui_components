import React from 'react';
import styled from 'styled-components';
import styleVars from './js/styleVars';

import Button from './button';
import Svg_icon from './svg_icon';
import {getKey} from './js/utility';

import Input from "./input";

let Form=styled.form`

    display  :flex;
    font-size:10px;
    margin   :0;
    padding  :0;


    .input{
        flex-grow:1;        
    }

    .sb_input {
        -webkit-appearance: none;

        border            : 1px solid ${styleVars.colors.grey10};
        border-radius     : 3px 0 0 3px;
        box-shadow        : 0 0 1px 1px rgba(0,0,0,.1) inset;
        font-size         : 1.6em;
        margin            : 0;

        min-height        : 40px;
        outline           : none;
        padding           : 0 .3em;            
    }

    .sb_submit {
        border-radius: 0 3px 3px 0;

        .svg_icon{
            font-size: 32px;
            margin   : 0 .2em;
        }
    }
`;


class SearchBox extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'SearchBox';

        this.onSuggestionSelected=this.onSuggestionSelected.bind(this);

    }

    onSuggestionSelected(suggestion){
        if(this.props.form.autoSubmit){
            this.form.dispatchEvent(new Event("submit"));
        }
    }

    render() {
        let configObj={
            form:{
                onSubmit:(e)=>{
                    console.warn("OnSubmit callback not defined!")
                    e.preventDefault()
                },
                autoSubmit:false,
                ...this.props.form
            },
            datalist:{
                list:[],
                ...this.props.datalist,
                onSuggestionSelected:this.onSuggestionSelected
            },
            input:{
                autoCapitalize:"none",
                className     :"sb_input",
                name          :"search_keyword",
                type          :"search",
                ...this.props.input                
            }
        }

            
        return (
            <Form innerRef={(el)=>{this.form=el}} {...configObj.form}>
                <Input input={{...configObj.input}} datalist={{...configObj.datalist}} />
                <Button className="sb_submit"><Svg_icon icon="ddl_icon_search" /></Button>
            </Form>
        )
    }
}

export default SearchBox;

