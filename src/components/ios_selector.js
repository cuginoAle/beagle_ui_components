
import styled from 'styled-components';
import styleVars from './js/styleVars.js';

import _barrel_value_picker from './_barrel_value_picker.js';

let IOS_selector=styled(_barrel_value_picker)`
        border:1px solid rgba(0,0,0,.3);
        background-color:#fff;

        ._snappable_scroll{
            position:relative;
            overflow-y:hidden;
            &:before{
                content:"";
                position:absolute;
                left: 0;
                right:0;
                top:0;
                display: block;
                height:calc(50% - 18px);
                background: linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(255,255,255,0.5) 100%);
                border-bottom:1px solid rgba(0,0,0,.3);
                pointer-events: none;
                z-index: 2;
            }

            &:after{
                content:"";
                position:absolute;
                left: 0;
                right:0;
                bottom:0;
                display: block;
                height:calc(50% - 18px);
                background: linear-gradient(to bottom, rgba(255,255,255,0.5) 0%, rgba(255,255,255,1) 100%);
                border-top:1px solid rgba(0,0,0,.3);
                pointer-events: none;
                z-index: 2;
            }
            
        }

        .draggable_content {

            p{
                font-size  : 20px;
                line-height: 20px;
                margin     : 0;
                padding    : 8px 14px;
                text-align : center;
                white-space: nowrap;
            }        
        }     


        ._barrel_value_picker_header {
            display:flex;
            align-items:center;
            justify-content:space-between;
            background-color:#eee;
            border-bottom:1px solid rgba(0,0,0,.1);

            p{
                flex-grow  :1;
                font-size  :14px;
                margin     :0;
                text-align :center;
                white-space:nowrap;
            }

            button {
                background-color:transparent;
                border          :none;
                color           :${styleVars.colors.blue10};
                font-size       :14px;
                height          :40px;
                line-height     :38px;
                margin          :0;
                padding         :0 1em;
                width           :auto;
            }

            .doneBtn {
                font-weight:bold;
            }

        }
    `;

export default IOS_selector;