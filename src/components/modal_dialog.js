import React from 'react';
import {render} from 'react-dom';

import animate from './js/animate.js';
import styled from 'styled-components';




let Wrapper=styled.div`
    background-color  :rgba(0,0,0,0);
    display           :flex;
    flex-direction    :column;
    height            :100%;
    left              :0;
    margin            :0;
    overflow          :hidden;
    perspective       :600px;
    
    position          :fixed;
    right             :0;
    top               :0;
    transition        :background-color .3s;
    z-index           :9999;


    .crx_modal_dialog_content {                                        
        justify-content :center;
        display         :flex;
        opacity         :.2;
        transform-origin:bottom;
        transition      :all .4s cubic-bezier(0.56, 0.2, 0, 1) .1s;      

        >*{
            box-shadow      :rgba(0, 0, 0, 0.4) 0px 9px 12px -6px;                           
        }
    }

    &.bottom {       
        perspective-origin:50% 100%;
        justify-content   :flex-end;

        .crx_modal_dialog_content{
            transform :translateY(100%);     
        }                 
    }

    &.top {       
        perspective-origin:50% 100%;
        justify-content   :flex-start;

        .crx_modal_dialog_content{
            transform :translateY(-100%);     
        }                 
    }    

    &.center {
        perspective-origin:50%;
        justify-content   :center;
        
        .crx_modal_dialog_content{
            padding   :10px;              
            transform :scale(.8);
            transition:all .4s cubic-bezier(0.34, 0.38, 0.35, 1.6);     
        }
    }

    &.showMD{
        background-color  :rgba(0,0,0,.6);

        .crx_modal_dialog_content {
            transform:translateY(0) scale(1);
            opacity:1;
        }
    }
`;



let Modal_dialog=(()=>{
    var div=document.createElement("div"),
        modalStack=[];


    div.addEventListener("webkitTransitionEnd", ()=>{  

        if(!div.querySelector(".crx_modal_dialog").classList.contains("showMD")){
            document.documentElement.removeChild(div);
            
            let modObj=modalStack.shift();
            modObj.onCloseCB && modObj.onCloseCB();
        }
    })        

    let _show=()=>{

            let modalObj=modalStack[0],
                classes=["crx_modal_dialog", modalObj.align];

            render(
                <Wrapper className={classes.join(" ")}>
                    <div className="crx_modal_dialog_content">
                        {modalObj.children}
                    </div>
                </Wrapper>
            ,div);

            document.documentElement.appendChild(div);

            let content=div.querySelector(".crx_modal_dialog");

            setTimeout(()=>{
                content.classList.add("showMD");

                content
                    .addEventListener('touchmove', function(e){            
                        //preventing the page from scrolling

                        var sourceElement = e.target || e.srcElement;
                        if(sourceElement.classList.contains("crx_modal_dialog")){
                            e.preventDefault();
                        }

                    });

                content
                    .addEventListener('click', function(e){   

                        //if the user clicked on the BG element...
                        if(e.target.classList.contains("crx_modal_dialog_content") || e.target.classList.contains("crx_modal_dialog")){
                            modalObj.onBgClick && modalObj.onBgClick()                            
                        }         
                    });
                    
                modalObj.onReady && modalObj.onReady(content)                                                        
            },20);

        },
        _close=()=>{
            div.querySelector(".crx_modal_dialog").classList.remove("showMD");
        }


    return {
        show:(options)=>{
            // children,align="bottom",onCloseCB=()=>{console.log("Modal_dialog: onClose")},onBgClick=()=>{}            
            modalStack.push({...options})
            _show();

        },
        close:_close
    }


})()



export default Modal_dialog;
