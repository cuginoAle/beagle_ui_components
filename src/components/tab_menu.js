import React from 'react';
import styled from 'styled-components';
import styleVars from './js/styleVars';
import {getKey} from './js/utility';


function Tab_menu(props){
    let classes=["tab_menu"],
        Wrapper=styled.div`
                display:flex;
                font-size:14px;
                border-bottom: 1px solid rgba(0,0,0,.1);
                padding: 0 10px;

                .tab_menu__tab {
                    background-color:rgba(0,0,0,.03);
                    border          :1px solid transparent;
                    border-radius   :2px 2px 0 0;
                    color           :#666;
                    margin: 0 2px 0 0;
                    overflow:hidden;

                    button {
                        border          :none;
                        background-color:transparent;
                        curson          :pointer;
                        padding         : 5px 20px;
                    }

                    &.selected {
                        border-color: rgba(0,0,0,.1);
                        background-color   :#fff;
                        border-bottom-color:transparent;                        
                        color              :${styleVars.colors.green17};
                        font-weight        :bold;
                        margin-bottom:-1px;
                    }
                }
            `;

    props.className && classes.push(props.className);

    return (
        <Wrapper className={classes.join(" ")}>
            {props.tabs.map((tab)=>{
                let tabClass=["tab_menu__tab"];
                tab.selected && tabClass.push("selected");
                tab.className && tabClass.push(tab.className)
                return (
                    <div key={getKey()} className={tabClass.join(" ")}><button onClick={tab.onClick||function(tab){console.log(tab)}}>{tab.caption}</button></div>
                )
            })}
        </Wrapper>
    )
}

export default Tab_menu;