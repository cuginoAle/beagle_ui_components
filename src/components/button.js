import React from 'react';
import styled from 'styled-components';
import styleVars from './js/styleVars.js';
import Caption from './caption.js';

const MyButton=styled.button`

    
    align-items    : center;
    border         : none;
    border-radius  : 3px;
    cursor         : pointer;
    font-size      : 14px;
    font-weight    : 400;
    margin         : 0;
    padding        : 0;
    min-width      : 40px;
    outline        : none;
    text-align     : center;
    text-decoration: none;
    user-select    : none;
    vertical-align : text-bottom;
    width          : auto;

    .wrapper{
        padding        : 0 7px;        
        display        : flex;
        height         : 100%;
        justify-content: center;
        line-height    : 40px;
        min-height     : 40px;

    }

    .svg_icon {
        align-self:center;
        margin    :0 .5em;
    }

    .caption {
        margin:0 .5em;
    }

    .caption ~ .svg_icon {
        margin-left:0;
    }

    .svg_icon ~ .caption {
        margin-left:0;
    }

    &.primary {
        background-color: ${styleVars.colors.blue10};         
        color           : #fff;     

        &:hover {
            background-color:${styleVars.colors.blue5};
        }       
    }

    &.secondary {
        background-color: #fff; 
        border          : 2px solid ${styleVars.colors.blue10};
        color           : ${styleVars.colors.blue10};

        font-weight: bold;
        
        &:hover {
            background-color:${styleVars.colors.blue15};
        }             
    }

    &.warning {
        background-color: ${styleVars.colors.orange5};         
        color           : #fff;     

        &:hover {
            opacity:.9;
        }         
    }

    &.clear {
        color:${styleVars.colors.blue10};
        background-color:transparent;

        &:hover {
            background-color:rgba(170, 196, 220, 0.19);
        }
    }

    &:disabled{
        cursor          : default;
        color           : ${styleVars.colors.grey8};
        border          : none;
        background-color: ${styleVars.colors.grey20};

        &:hover {
            background-color: ${styleVars.colors.grey20};
        }
    }

    &.show_notification {

            position:relative;
            &:after {
                align-items     : center;
                background-color: rgba(255, 0, 0, 0.7);
                border-radius   : 50%;
                color           : white;
                content         : attr(data-notification);
                display         : flex;
                font-size       : 12px;
                font-weight     : 200;
                height          : 2em;
                justify-content : center;
                left            : 50%;
                padding         : 0px;
                position        : absolute;
                top             : 1px;
                width           : 2em;
            }            
    }


`;

function Button({id,className,icon,caption,onClick=()=>{console.log("Button - onClick")},disabled=false,type="primary",style={},children,dataNotification}){

    let classes=[type];

    className && classes.push(className);
    caption && classes.push("hasCaption");
    dataNotification>0 && classes.push("show_notification");

    return (
        <MyButton id={id} disabled={disabled} data-notification={dataNotification} className={classes.join(" ")} onClick={onClick} style={style}>
            <div className="wrapper">
                {caption==undefined?children:<Caption>{caption}</Caption>}          
            </div>
        </MyButton>
    )    
}
export default Button;
