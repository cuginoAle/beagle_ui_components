import React from 'react';
import styled from 'styled-components';
// import {svg} from "./assets/symbol-defs.svg";


// let tag=document.createElement("head");
// tag.innerHTML=svg;
// document.documentElement.insertBefore(tag,document.querySelector("head"))


const Wrapper=styled.span`
    display     : inline-block;
    svg{
        display     : block;
        fill        : currentColor;
        height      : 1em;
        stroke      : currentColor;
        stroke-width: 0;
        width       : 1em;

    }
    &.dll_icon {
        svg{
            stroke-width: 1.2;
            fill: none;                        
        }
    }
`;


function Svg_icon(props) {

    let iconClass = ["svg_icon ", props.icon];
    props.className && iconClass.push(props.className);

    if(props.icon && props.icon.indexOf("ddl_icon")>-1){
        iconClass.push("dll_icon");
    }

    //TODO: for production this should be: @tesco_ux/ui_components/assets/symbol-defs.svg
    let svgFile=props.svgFile || window.svgFilePath || "/assets/symbol-defs.svg",
        svgIcon=svgFile + "#" + props.icon;

    let html = { __html: `<svg xmlns:xlink="http://www.w3.org/1999/xlink"><use className="icon_element" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="${svgIcon}"></use></svg>` };
        
    return (<Wrapper className={iconClass.join(" ")} style={props.style} dangerouslySetInnerHTML = { html } />)
}


export default Svg_icon;