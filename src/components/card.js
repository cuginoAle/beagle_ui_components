import React from 'react';
import Svg_icon from './svg_icon.js';

import styled from 'styled-components';
import styleVars from './js/styleVars.js';



export default class Card extends React.Component {
  

    constructor(props) {
        super(props);

        this.state = {
            visible: true
        }

        this.onClick=this.onClick.bind(this);
    }

    onClick(e) {
        this.setState({visible:false});
        this.props.onClick && this.props.onClick(e);
    }

    getStyle(){
        let borderColor=styleVars.colors.grey8,
            bgColor="white";

        switch(this.props.theme){
            case "green":
                borderColor=styleVars.colors.green15
                bgColor=styleVars.colors.green20;
            break;

            case "yellow":
                bgColor=styleVars.colors.yellow10;
            break;
        }

        return styled.div`
            ${props=>this.props.noborders?"border:none" : "border:1px solid " + borderColor};
            background-color:${bgColor};
            border-radius   :${styleVars.border.borderRadius};
            padding:14px;
            position: relative;            

            .card_title {
                display    :flex;
                align-items:center;
                font-size  :16px;
                margin     :0 0 14px;
                height     :19px;

                .svg_icon {
                    font-size   :2em;
                    margin-right:.3em;
                }
            }
            

            &.removable {
                padding-right:60px;
            }
            .hideBtn{
                align-items     : center;
                background-color:transparent;
                border          :none;
                color           : ${styleVars.colors.blue10};
                display         : flex;
                font-size       : 16px;
                margin          :0;
                padding         :0 .5em;
                position        : absolute;
                right           :0px;
                top             :0px;
                width           :auto;

                .svg_icon {
                    font-size: 11px;
                    margin-left:.5em;
                }
            } 
        `;    
    }

  render() {
    
    let Wrapper=this.getStyle();
    let closeButton=(<button className="hideBtn" onClick={this.onClick}><span className="caption">Hide</span> <Svg_icon icon="close"/></button>);

    let classes=[this.props.className, "crx_card"];
    this.props.removable && classes.push("removable");

    let code=(<Wrapper className={classes.join(" ")}>
                {this.props.removable?closeButton:("")}
                {this.props.children}                
            </Wrapper>);

    return this.state.visible?code:null;
    
  }
}

