import React from 'react';
import {render} from 'react-dom';
import Svg_icon from './svg_icon';

import styled from 'styled-components';

function Message_box(){

    let Wrapper=styled.div`
            background-color:rgba(0, 83, 159, .95);
            bottom          :0;
            padding         :30px;
            position        :fixed;
            transform       :translateY(100%);
            transition      :transform .4s;
            width           :100%;
            box-sizing      :border-box;
            z-index         :999;

            .svg_icon.close {
                color      :white;
                font-size  :12px;
                position   :absolute;
                right      :10px;
                top        :10px;
            }

            .show &{
                transform:translateY(0);
            }


        `;

    var div=document.createElement("div"),
        onClose=[],
        isClosing=false,
        timeoutHnd=null;




    div.addEventListener("click",(e)=>{
        div.classList.remove("show");
    });

    div.addEventListener("webkitTransitionEnd", ()=>{  
        isClosing=false;

        if(!div.classList.contains("show")){
            document.documentElement.removeChild(div);
            callOnClose();
        }
    })

    var _showMessage=(children,timeout,closeCb)=>{
        clearTimeout(timeoutHnd);

        if(closeCb){
            setTimeout(()=>{
                onClose.push(closeCb);                
            },0)
        }

        render(<Wrapper className="message_box"><Svg_icon icon="close" />{children}</Wrapper>,div)

        document.documentElement.appendChild(div);
        setTimeout(()=>{
            div.classList.add("show");
            if(timeout){
                timeoutHnd=setTimeout(msgObj.close,timeout);
            }
        },20);
    }

    var callOnClose=()=>{
        var cb=onClose.shift();
        while(cb!=undefined){
            cb();
            cb=onClose.shift()
        }
        
    }


    var msgObj={
        show:(children,timeout,closeCb)=>{
            let alreadyDisplayed=div.classList.contains("show");

            if(isClosing){
                callOnClose();
            }

            if(alreadyDisplayed){
                
                onClose.push(()=>{    
                    console.log("showing earlier!")         
                    _showMessage(children,timeout,closeCb)
                })
                msgObj.close();
            }else{
                _showMessage(children,timeout,closeCb)
            }
        },
        close:()=>{
            isClosing=true;
            div.classList.remove("show");
            clearTimeout(timeoutHnd);
        }
    }

    return msgObj;
}

export default Message_box();