//data_sources
import Products from "../data_sources/products";
import TopKeywords from "../data_sources/top_keywords";

export default {
    Products,
    TopKeywords
};