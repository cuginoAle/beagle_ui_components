import React from 'react';
import styled from "styled-components";
import Datalist from "./dataList";

let Wrapper=styled.div`
    display:inline-block;

    input {
        box-sizing : border-box;        
        display    : block;
        font-weight:200;
        height     : 100%;
        width      : 100%;
    }

    ul {
        max-height: 35vh;
    }
`;

class Input extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Input';

        this.onSuggestionSelected=this.onSuggestionSelected.bind(this);
        this.blur=this.blur.bind(this);
        this.focus=this.focus.bind(this);
        this.keyDown=this.keyDown.bind(this);
        
        this.highlightSuggestion=this.highlightSuggestion.bind(this);


        this.state={
            showSuggestions:false,
            suggestions:props.datalist.list,
            suggestionIndex:-1
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            suggestions:nextProps.datalist.list,
            suggestionIndex:-1
        });
    }

    onSuggestionSelected(suggestion){
        this.input.value=suggestion;

        if(this.props.datalist.onSuggestionSelected){
            this.props.datalist.onSuggestionSelected(suggestion);
        }else{
            console.warn("Input: onSuggestionSelected function not defined!")
        }
    }

    blur(e){
        //allowing the user to click/tap on the suggestion before removing it
        setTimeout(()=>{
            this.setState({showSuggestions:false})            
        },20)
    }

    focus(e){
        this.setState({showSuggestions:true})
    }

    keyDown(e){
        
        
        const   enter=13,
                down=40,
                up=38;

        switch(e.keyCode){
            case down:
                this.highlightSuggestion(1);
                e.preventDefault();
            break;

            case up:
                this.highlightSuggestion(-1);
                e.preventDefault();
            break

            case enter:
                let suggestion=this.state.suggestions[this.state.suggestionIndex];
                if(suggestion){                
                    this.onSuggestionSelected(suggestion);
                    e.preventDefault();
                }
            break;
        }
    }

    highlightSuggestion(indexIncr){
        if(this.state.showSuggestions && this.state.suggestions.length>0){
            let index=this.state.suggestionIndex;

            index+=indexIncr;

            //checking that the index is within the boundaries
            index=Math.max(0,index);
            index=Math.min(this.state.suggestions.length-1,index);

            this.setState({
                suggestionIndex:index
            })

        }
    }

    render() {

        return (
            <Wrapper className="input">
                <input ref={(el)=>{this.input=el}} {...this.props.input} onBlur={this.blur} onFocus={this.focus} onKeyDown={this.keyDown}/>
                {this.state.showSuggestions && <Datalist highlightedIndex={this.state.suggestionIndex} onClick={this.onSuggestionSelected} list={this.state.suggestions} />}
            </Wrapper>
        )
    }
}

export default Input;
