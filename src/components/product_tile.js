import React from 'react';
import styled from 'styled-components';
import styleVars from './js/styleVars';
import Caption from './caption';
import Button from './button';
import Svg_icon from './svg_icon';

function Product_tile(props){
    let Wrapper=styled.div`
            
            background-color:#fff;
            border          :1px solid rgba(0,0,0,.1);

            display         : flex;
            flex-direction  : column;
            justify-content : space-between;
            min-width       : 140px;
            padding         : 10px;

            .img_wrapper {
                img {
                    display:block;
                    width:100%;
                    max-width:120px;
                    margin:auto;
                }
                margin-bottom:10px;
            }

            .product_name {
                margin-bottom:20px;
                flex-grow:1;
            }

            // .product_price {
            //     display:flex;
            //     justify-content:space-between;
            //     margin-bottom:10px;
            // }

            .secondary{
                .caption{
                    font-weight:bold;
                }
                .svg_icon {
                    font-size:18px;
                }
            }


        `;
    let classes=["product_tile"];
    props.className && classes.push(props.className);

    function onClick(e){
        props.onClick(e,props);
    }


    let buttonHtml=props.pinned?
            (            
                <Button onClick={onClick} type="secondary" className="pinBtn">
                    <Caption>Unpin</Caption>
                </Button>
            )
        :
            (
            <Button onClick={onClick} type="secondary" className="pinBtn">
                <Caption>Pin</Caption>
                <Svg_icon icon="pin"/>
            </Button>
            );

    return (
        <Wrapper key={props.ProductId} className={classes.join(" ")}>
            <div className="img_wrapper">
                <img src={props.ImagePath.replace("90x90","225x225")} />
            </div>
            <p className="product_name">{props.Name}</p>
            {
            //<p className="product_price"><span>{props.PriceDescription}</span> <b>£{props.Price}</b></p>
            }

            {buttonHtml}
        </Wrapper>
    )
}

export default Product_tile;