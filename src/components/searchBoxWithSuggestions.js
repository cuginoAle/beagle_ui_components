import React from 'react';
import SearchBox from './searchBox';
import Data from './data';
import {deb} from './js/utility';


class SearchBoxWithSuggestions extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'SearchBoxWithSuggestions';

        this.state={
            suggestionList:[]
        }

        this.getSuggestions=this.getSuggestions.bind(this);
        this.debGetSuggestions=deb(this.getSuggestions,100);
    }


    getSuggestions(userInput){
        let maxCount=8;

        userInput=userInput.toLowerCase();
        
        Data.TopKeywords.popularSearches([userInput,maxCount],(keywords)=>{

            let results=keywords
                        .sort((a,b)=>{
                            return a.position-b.position
                        })
                        .filter((item)=>{
                            return item.label.indexOf(userInput)>-1
                        })
                        .slice(0,maxCount)
                        .map((keyword)=>{
                            return keyword.label
                        });

            this.setState({
                suggestionList:results
            })
        })
    }


    render() {
        let self=this;
        let formOptions={
                form:{
                    ...self.props,
                    autoSubmit:true,
                    onSubmit:(e)=>{                    

                        self.setState({
                            suggestionList:[]
                        }) 

                        self.props.form && self.props.form.onSubmit && self.props.form.onSubmit(e);
                    }
                },

                datalist:{
                    list:this.state.suggestionList                  
                },
                input:{
                    ...self.props.input,
                    autoComplete:"off",
                    placeholder:self.props.input ? self.props.input.placeholder : "",
                    onInput:(e)=>{
                        let value=e.target.value;

                        if(value.length>1){                            
                            
                                this.debGetSuggestions(value)
                            
                        }else{
                            this.setState({
                                suggestionList:[]
                            })                            
                        }
                    }
                }
            };

        return (
            <div className='search_box_with_suggestions'>                
                <SearchBox {...formOptions} />
            </div>
        )
    }
}

export default SearchBoxWithSuggestions;
