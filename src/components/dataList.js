//this component renders a datalist element, which is only useful when used in tandem with the <input> element to provide auto-complete capability
import React from 'react';
import styled from "styled-components";
import {getKey} from './js/utility';
import Button from './button';


//checking 
// var nativedatalist = !!('list' in document.createElement('input')) && !!(document.createElement('datalist') && window.HTMLDataListElement);

let Wrapper=styled.div`
    position:relative;
    z-index:2;

    ul{
        -webkit-overflow-scrolling: touch;
        backgroundColor           :#fff;
        box-shadow                : 0 2px 4px -2px rgba(0,0,0,.4);
        left                      :0;
        list-style                : none;
        margin                    : 0;
        
        overflow                  :auto;
        padding                   : 0;
        position                  :absolute;
        right                     :0;
        top                       :0;

        .selected {
            background-color:rgba(59, 172, 255, 0.1);
        }
    }

    a {
        align-items    :center;
        color          :#00539F;
        cursor         :pointer;
        display        :flex;
        font-size      :1.7em;
        font-weight    :400;
        justify-content:flex-start;     
        min-height     :40px;
        padding        :0 .3em;
        width          :100%;
    }

`;

export default (props)=>{
    function onClick(e,item){

        if(props.onClick){
            props.onClick(item)
        }else{
            console.warn("Datalist: onClick function not defined!")            
        } 
    }

    return (
            <Wrapper>
                <ul>
                { 
                    props.list && props.list.map((item,i)=>{
                        let id="_" + getKey(),
                            isSelected=(i==props.highlightedIndex);

                        return (<li key={id} className={isSelected && "selected"}><a onClick={(e)=>{onClick(e, item)}} >{item}</a></li>)
                    })
                }
                </ul>
            </Wrapper>
    )
}
