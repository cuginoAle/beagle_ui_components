import React from 'react';

import styled from 'styled-components';
import styleVars from './js/styleVars.js';
import Svg_icon from './svg_icon.js';

function Checkbox(props){

    let Wrapper=styled.label`
        align-items: center;
        cursor     : pointer;
        display    : flex;
        flex-wrap  : nowrap;
        position   : relative;


        input {
            bottom  : 0;
            height  : auto;
            left    : 0;
            opacity : 0;
            position: absolute;
            right   : 0;
            top     : 0;
        }
        
        .chkBox{

                align-items     : center;
                background-color: #fff;
                border          : 1px solid ${styleVars.colors.grey5};
                border-radius   : 2px;
                color           : #fff;
                display         : flex;
                height          : 28px;
                justify-content : center;
                margin-right    : .5em;
                position        : relative;
                width           : 28px;
                z-index         : 2;
                
                .svg_icon {
                    display: none;
                }

        }
        :checked + .chkBox{
            background-color:${styleVars.colors.green5};
            border-color:${styleVars.colors.green5};

            .svg_icon {
                display: block;
            }
        }
    `;
    return (
        <Wrapper className="crx_checkBox">
            <input type="checkbox" onChange={props.onChange} checked={props.checked}/>
            <span className="chkBox">
                <Svg_icon icon="checkmark"/>
            </span>
            {
                props.label?(
                    <span className="caption">{props.label}</span>
                ):("")
            }
        </Wrapper>
    );
}

export default Checkbox;