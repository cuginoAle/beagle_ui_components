
import React from 'react';

import _snappable_scroll from './_snappable_scroll.js';

import styled from 'styled-components';
import styleVars from './js/styleVars.js';
import {getKey} from './js/utility.js';



class Combo_barrel_value_picker extends React.Component {


    constructor(props) {
        super(props);
        this.displayName = 'Combo_barrel_value_picker';

        this.state={
            padderStyle:{
                height:this.props.stepHeight
            }
        }
    }

    componentDidMount() {

        window.requestAnimationFrame(()=>{
            //this is needed only to wait for the browser to have rendered the html

            this.setState({
                padderStyle:{
                    height:this.snappableComponent.offsetHeight/2-this.props.stepHeight/2
                }
            })
        })
    }


    renderOptions(arr){

        return arr.map((optionSet)=>{
        

            return (<_snappable_scroll key={getKey()} initialPosition={optionSet.initialPosition} selectedIndex onChange={optionSet.onChange} style={{minHeight:this.props.stepHeight*3,height:this.props.stepHeight*this.props.height}} stepHeight={this.props.stepHeight} animation={{curve:[.3,0,.5,1.6],duration:200}}>
                
                <div style={this.state.padderStyle} />
                    {optionSet.options.map((opt)=>{
                        return <p key={opt.key}>{opt.label}</p>
                    })}
                <div style={this.state.padderStyle} />

            </_snappable_scroll>)
        })
    }


    render() {
        let classes=["combo_barrel_value_picker"];

        let StyledWrapper=styled.div`
                
                background-color:#fff;
                border          :1px solid rgba(0,0,0,.3);
                box-sizing      :border-box;
                width           :100%;

                .barrels_wrapper {
                    display        :flex;
                    justify-content:space-around;
                }

                ._snappable_scroll{
                    flex-basis:${(100 / this.props.options.length).toFixed(3)}%;
                    position  :relative;
                    overflow  :visible;
                    overflow-y:hidden;

                    &:before{
                        background    : linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(255,255,255,0.5) 100%);
                        border-bottom :1px solid rgba(0,0,0,.3);
                        content       :"";
                        display       : block;
                        height        :calc(50% - 18px);
                        left          : 0;
                        pointer-events: none;
                        position      :absolute;
                        right         :0;
                        top           :0;
                        z-index       : 2;
                    }

                    &:after{
                        background    : linear-gradient(to bottom, rgba(255,255,255,0.5) 0%, rgba(255,255,255,1) 100%);
                        border-top    :1px solid rgba(0,0,0,.3);
                        bottom        :0;
                        content       :"";
                        display       : block;
                        height        :calc(50% - 18px);
                        left          : 0;
                        pointer-events: none;
                        position      :absolute;
                        right         :0;
                        z-index       : 2;
                    }
                    
                }

                .draggable_content {

                    p{
                        font-size  : 20px;
                        padding    : 8px 14px;
                        text-align : center;
                        white-space: nowrap;
                        line-height: 20px;
                    }        
                }


                ._barrel_value_picker_header {
                    align-items     :center;
                    background-color:#eee;
                    border-bottom   :1px solid rgba(0,0,0,.1);
                    display         :flex;
                    justify-content :space-between;

                    p{
                        flex-grow  :1;
                        font-size  :14px;
                        text-align :center;
                        white-space:nowrap;
                    }

                    button {
                        background-color:transparent;
                        border          :none;
                        font-size       :14px;
                        margin          :0;
                        padding         :0 1em;
                        width           :auto;
                        color:${styleVars.colors.blue10};
                    }

                    .doneBtn {
                        font-weight:bold;
                    }
                }
            `;

        this.props.className && classes.push(this.props.className);

        let header=this.props.showHeader?(
                <div className="_barrel_value_picker_header">
                    <button className="button button-secondary cancelBtn" onClick={this.props.onCancel}>Cancel</button>
                    <p className="_barrel_value_picker_title">{this.props.title}</p>
                    <button className="button button-secondary doneBtn" onClick={this.props.onDone}>Done</button>
                </div>
            ):null;


        return (
            <StyledWrapper className={classes.join(" ")}>
                {header}
                <div className="barrels_wrapper" ref={(elem)=>{this.snappableComponent=elem}}>
                    {this.renderOptions(this.props.options)}
                </div>
            </StyledWrapper>
        )
    }
}

var selectionObj={};

Combo_barrel_value_picker.defaultProps={
    title:"Choose an option",
    showHeader:true,
    stepHeight:36,
    height:5,
    options:[
        {
            onChange:function(value){
                console.log(value)
                // selectionObj.qty=this.options[value].value
            },
            options:(()=>{
                let arr=[];
                for (var x=1;x<21;x++){
                    arr.push({key:"dummy_" + x,label:"value "+x,value:x})
                }

                return arr
            })()
        }
    ]
}

export default Combo_barrel_value_picker;

