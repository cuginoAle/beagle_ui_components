import React from 'react';
import styled from 'styled-components';

import Button from './button';
import Svg_icon from './svg_icon';


let Wrapper=styled(Button)`
        .wrapper {
            padding:0;
        }

        .svg_icon{
            margin:0;
        }
    `;

export default (props)=>{
    let classes=["icon_button"];

    props.button.className && classes.push(props.button.className);

    return (
        <Wrapper className={classes.join(" ")} {...props.button}><Svg_icon icon={props.icon} /></Wrapper>
    )
}