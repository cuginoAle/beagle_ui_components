import styled from 'styled-components';
import React from 'react';

import Svg_icon from './svg_icon';

let Wrapper=styled.a`
    display:inline-flex;
    height:.364em;

    svg,
    .svg_icon{
        height:100%;
    }
`;

function Tesco_monochrome_logo(props){
    let homeLink=props.href||"/",
        classes=["tesco_monochrome_logo"];
        

    props.className && classes.push(props.className);

    return (
        <Wrapper className={classes.join(" ")} href={homeLink}>
            <Svg_icon icon="monochrome_logo"/>
        </Wrapper>
    );

}

export default Tesco_monochrome_logo;