import React from 'react';
import styled from 'styled-components';
import styleVars from './js/styleVars.js';

import Svg_icon from './svg_icon.js';


function Link(props){
    let Wrapper=styled.a`
            display        : inline-block;
            text-decoration: none;

            .content{
                align-items    : center;
                color          : ${styleVars.colors.blue10};
                display        : flex;            
                font-size      : 14px;
                font-weight    : 400;
                line-height    : 1.2;
                justify-content: center;
            }

            .svg_icon {
                margin-left: .3em;
            }
            &:hover{
                .caption{
                    text-decoration: underline;
                }
                
            }

        `;
    return (
        <Wrapper id={props.id} style={props.style} href={props.href}>
            <span className="content">
                <span className="caption">{props.caption}</span>
                {props.icon?(                
                    <Svg_icon icon={props.icon}/>
                ):("")}                
            </span>
        </Wrapper>
    );
}

export default Link;