import React from 'react';
import _snappable_scroll from './_snappable_scroll.js';
import {getKey} from './js/utility';


class _barrel_value_picker extends React.Component {

    constructor(props) {
        super(props);
        this.displayName = '_barrel_value_picker';

        this.state={
            padderStyle:{
                height:this.props.stepHeight
            }
        }
    }

    componentDidMount() {

        window.requestAnimationFrame(()=>{
            //this is needed only to wait for the browser to have rendered the html

            this.setState({
                padderStyle:{
                    height:this.snappableComponent.wrapper.offsetHeight/2-this.props.stepHeight/2
                }
            })
        })
    }


    render() {
        let classes=["_barrel_value_picker"];

        let style={
            minHeight:this.props.stepHeight*3,
            height   :this.props.stepHeight*this.props.height
        };


        this.props.className && classes.push(this.props.className);
        return (
            <div className={classes.join(" ")}>
                <div className="_barrel_value_picker_header">
                    <button className="cancelBtn" onClick={this.props.onCancel}>Cancel</button>
                    <p className="_barrel_value_picker_title">{this.props.title}</p>
                    <button className="doneBtn" onClick={this.props.onDone}>Done</button>
                </div>
                <_snappable_scroll onChange={this.props.onChange} initialPosition={this.props.initialPosition} ref={(elem)=>{this.snappableComponent=elem}} style={style} stepHeight={this.props.stepHeight} animation={{curve:[.3,0,.5,1.6],duration:500}}>
                    
                    <div style={this.state.padderStyle} />
                        {this.props.entries.map((entry)=>{
                            return (
                                <p key={getKey()}>{entry}</p>
                            )
                        })}
                    <div style={this.state.padderStyle} />

                </_snappable_scroll>
            </div>
        )
    }
}

_barrel_value_picker.defaultProps={
    title:"Barrel value picker!",
    stepHeight:36,
    initialPosition:0,
    height:3,
    children:[(<span>empty</span>)]
}

export default _barrel_value_picker;

