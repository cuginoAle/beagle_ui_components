import styled from 'styled-components';
import React from 'react';

import Svg_icon from './svg_icon';


function Tesco_logo(props){
    let homeLink=props.href||"/",
        classes=["tesco_logo"],
        Wrapper=styled.a`
            display:inline-flex;
            height:.364em;

            svg,
            .svg_icon{
                height:100%;
            }
        `;

    props.className && classes.push(props.className);

    return (
        <Wrapper style={props.style} className={classes.join(" ")} href={homeLink}>
            <Svg_icon svgFile={props.svgFile} icon="logo"/>
        </Wrapper>
    );

}

export default Tesco_logo;