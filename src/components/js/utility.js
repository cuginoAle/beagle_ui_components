import React, {Component} from 'react';
import Svg_icon from '../svg_icon';
import styled from 'styled-components';

export const $=(selector,parent=document)=>{
    return parent.querySelector(selector)
};

export const $$=(selector,parent=document)=>{
    return parent.querySelectorAll(selector)
};

let key=0;
export const getKey=()=>{
    return key++; 
};

var NoTextSeletion=styled.div`
    -webkit-user-select: none;  /* Chrome all / Safari all */
    -moz-user-select: none;     /* Firefox all */
    -ms-user-select: none;      /* IE 10+ */
    user-select: none;          /* Likely future */    

    -webkit-touch-callout: none; /* disable the IOS popup when long-press on a link */
`;

let SelectableWrapper=styled.div`

    position:relative;

    .wrappable_content{
        transition: transform 0.4s cubic-bezier(0.16, 0.76, 0.43, 1.3);
        transform:scale3d(1,1,1);
        
    }

    &>.svg_icon{
        position:absolute;
        top:5px;
        left:5px;
        fontSize:2.2em;
        padding:.2em;
        borderRadius:50%;
        border:1px solid rgba(0,0,0,.2);
        backgroundColor:transparent;
        color:transparent;
        transition: all 0.4s cubic-bezier(0.16, 0.76, 0.43, 1.3);
    },

    &.selected{

        .wrappable_content{
            transform:scale3d(.85,.85,1);                
        }
        
        &>.svg_icon{
            border:1px solid rgba(0,0,0,0);
            backgroundColor:green;
            color:white;     
        }
    }

`;

export const MakeSelectable=(WrappedComponent)=>{

    return class extends Component {
        constructor(props) {
            super(props);
            this.onClick=this.onClick.bind(this)
        }

        onClick(e){
            if(this.props.selectableOptions.isSelectable){
                this.props.selectableOptions.onSelect(this.props.selectableOptions);
                e.stopPropagation()
            }
        }

        render(){
            let classes=["make_selectable"],
                props={...this.props};

            delete props.selectableOptions;

            this.props.selectableOptions.selected && classes.push("selected");
            this.props.selectableOptions.className && classes.push(this.props.selectableOptions.className);


            return (
                <SelectableWrapper className={classes.join(" ")}  onClickCapture={this.onClick}>
                    <div className="wrappable_content">
                        <WrappedComponent {...props}/>
                    </div>   
                    {this.props.selectableOptions.isSelectable && <Svg_icon icon="ddl_icon_benefits" />}                                     
                </SelectableWrapper>
            )
        }
    }   
}

export const WithLongTap=(WrappedComponent)=>{
    let longTapFired=false;

    return class extends Component {
        constructor(props) {
            super(props);
            this.timerHnd=null

            this.touchStart=this.touchStart.bind(this);
            this.touchMove=this.touchMove.bind(this);
            this.touchEnd=this.touchEnd.bind(this);
            this.onClick=this.onClick.bind(this);
        }


        touchStart(e){
            // e.preventDefault();
            longTapFired=false;

            let cb=this.props.longTapOptions.onLongTap || function(e){
                    console.warn("please define onLongTap callback!")
                };

            let longTapThreshold=this.props.longTapOptions.longTapThreshold || 400;


            
            this.timerHnd=setTimeout(()=>{          
                    longTapFired=true;
                    cb(e,this.props.longTapOptions);
                },longTapThreshold);

            if(this.props.longTapOptions.onTouchStart){                
                this.props.longTapOptions.onTouchStart(e)
            }

        }
        
        touchMove(e){
            clearTimeout(this.timerHnd);
            longTapFired=false;
            this.timerHnd=null;
            if(this.props.longTapOptions.onTouchMove){
                this.props.longTapOptions.onTouchMove(e)
            }            
        }

        touchEnd(e){
            clearTimeout(this.timerHnd);
            this.timerHnd=null;

            if(this.props.longTapOptions.onTouchEnd){
                e.preventDefault();
                this.props.longTapOptions.onTouchEnd(e)
            }

            //preventing the click event to fire
            if(longTapFired){
                e.stopPropagation();
                e.preventDefault();
            }
        }

        onClick(e){
            if(!longTapFired){
                this.props.longTapOptions.onClick && this.props.longTapOptions.onClick(e,this.props.longTapOptions)
            }
        }



        render(){
            let props={...this.props};
            delete props.longTapOptions;

            return (
                <NoTextSeletion className={this.props.longTapOptions.className} onClick={this.onClick} onTouchStart={this.touchStart} onTouchMove={this.touchMove} onTouchEnd={this.touchEnd} >
                    <WrappedComponent {...props}/>
                </NoTextSeletion>
            )
        }
    }
}

export const ready = function ( fn ) {

  if (document.readyState != 'loading'){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }

};
// Example
// ready(function() {
    // Do stuff...
// });





//element.closest polifill (https://developer.mozilla.org/en-US/docs/Web/API/Element/closest)

if (window.Element && !Element.prototype.closest) {
    Element.prototype.closest = 
    function(s) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
            i,
            el = this;
        do {
            i = matches.length;
            while (--i >= 0 && matches.item(i) !== el) {};
        } while ((i < 0) && (el = el.parentElement)); 
        return el;
    };
}





// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
export const deb = function(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};