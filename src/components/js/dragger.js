import TWEEN from 'tween.js';


class Decelerator{
    constructor(V=300,a=-200,currentValue,snapValue,limit,){
        //object to get duration and position of the element whose initial speed 
        //is V and friction is a
        //optionally it will snap the object to descrete position
        //optionally it will make sure the returned value is never greater than limit

        var S=-V*V/(a*2);
            T=-V/a;

        if(limit!=undefined && Math.abs(S)>Math.abs(limit)){
            S=limit;
            a=-(V*V)/(2*S);
            T=-V/a;
        }

        if(snapValue){

            S=Math.round((S+currentValue)/snapValue) * snapValue - currentValue;
            
            if(S==0){
                V=a=T=0;
            }else{
                a=-(V*V)/(2*S);
                T=-V/a;            
            }

            if(a==0) {T=0};

            T=Math.abs(T);
        }

        this.V=V;
        this.a=a;
        this.S=S;
        this.T=T;
          
    }

    get Duration(){
        return this.T;
    }

    Pos(t){
        return this.V*t + this.a/2*(t*t)
    }

    get FinalPos(){
        return this.Pos(this.T);        
    }
}



export default class dragger{
    constructor({
            sourceEl=document,
            friction=2000,
            boundX=[],
            boundY=[],
            snapWidth=60,
            snapHeight=60,
            initialValues={x:0,y:0},
            callback
        }){

        let body=document.querySelector("body"),
            lastValues={x:initialValues.x,y:initialValues.y},
            tween=null,
            startingPosition={x:initialValues.x,y:initialValues.y},
            pointerId,
            startX,
            startY,
            trackingPoints=[];
        
        sourceEl = (typeof sourceEl === 'string') ? document.querySelector(sourceEl) : sourceEl;

        if (!sourceEl) {
            throw new Error('Dragger: source not found.');
        }

        sourceEl.addEventListener('touchstart', onDown);
        sourceEl.addEventListener('mousedown', onDown);


        /**
         * Initializes movement tracking
         * @param  {Object} ev Normalized event
         */
        function onDown(ev) {
            var event = normalizeEvent(ev);                
            tween && tween.end();

            pointerId = event.id;

            trackingPoints=[];
            startX=event.x;
            startY=event.y;     
            startingPosition.x=lastValues.x;
            startingPosition.y=lastValues.y;           
            
            body.addEventListener('touchmove', onMove,{passive:false});
            body.addEventListener('touchend', onUp);
            body.addEventListener('touchcancel', stopTracking);
            body.addEventListener('mousemove', onMove);
            body.addEventListener('mouseup', onUp);
        
        }

        /**
         * Handles move events
         * @param  {Object} ev Normalized event
         */
        function onMove(ev) {
            ev.preventDefault();
            var event = normalizeEvent(ev);


            if (event.id === pointerId) {
                lastValues.x=startingPosition.x+(event.x-startX);
                lastValues.y=startingPosition.y+(event.y-startY);
                lastValues.timestamp=(new Date()).getTime();

                lastValues=checkBounds(lastValues,{x:boundX,y:boundY})
                
                addTrackingPoint(lastValues);

                callback(lastValues);
            }
        }


        /**
         * Handles up/end events
         * @param {Object} ev Normalized event
         */
        function onUp(ev) {
            var event = normalizeEvent(ev),
                animationComplete=false;

            
            if (event.id === pointerId) {
                stopTracking();

                let currentValue={
                        x:startingPosition.x+(event.x-startX),
                        y:startingPosition.y+(event.y-startY),
                        timestamp:(new Date()).getTime()
                    };

                    addTrackingPoint(currentValue);

                let newX=lastValues.x<boundX[0]?boundX[0]:lastValues.x>boundX[1]?boundX[1]:lastValues.x,
                    newY=lastValues.y<boundY[0]?boundY[0]:lastValues.y>boundY[1]?boundY[1]:lastValues.y;

                if(newX==lastValues.x && newY==lastValues.y){
                    let speedX=getXSpeed(),
                        speedY=getYSpeed();

                    if(speedX || speedY){
                        let movObj={
                                speed:{x:speedX,y:speedY},
                                currentPos:{x:newX,y:newY},
                                boundX,
                                boundY,
                                snapOn:{width:snapWidth,height:snapHeight},
                                friction
                            }
                        let landingPoint=getLandingPoint(movObj);

                        tween=new TWEEN.Tween(lastValues)
                            .to(landingPoint,landingPoint.t*1000)     
                            .easing(TWEEN.Easing.Elastic.Out)                                       
                            .onComplete(()=>{
                                animationComplete=true;
                                lastValues.x=landingPoint.x;
                                lastValues.y=landingPoint.y;
                            })
                            .start()
                        render();

                    }else{
                        if(newX%snapWidth>0 || newY%snapHeight>0){
                            newX=Math.round(lastValues.x/snapWidth)*snapWidth;
                            newY=Math.round(lastValues.y/snapHeight)*snapHeight;

                            tween=new TWEEN.Tween(lastValues)
                                .to({x:newX,y:newY},600)     
                                .easing(TWEEN.Easing.Elastic.Out)                                       
                                .onComplete(()=>{
                                    animationComplete=true;
                                    lastValues.x=newX;
                                    lastValues.y=newY;
                                })
                                .start()
                            render();                            
                        }
                    }                    
                }else{
                    tween=new TWEEN.Tween(lastValues)
                        .to({x:newX,y:newY},600)
                        .easing(TWEEN.Easing.Elastic.Out)
                        .onComplete(()=>{
                            animationComplete=true;
                            lastValues.x=newX;
                            lastValues.y=newY;
                        })                                    
                        .start()

                    render();                
                }
            }


            function render(time){
                if(!animationComplete){
                    requestAnimationFrame(render);                    
                    TWEEN.update();
                
                    callback(lastValues);
                }
            }
        }
        
        /**
         * Stops movement tracking, starts animation
         */
        function stopTracking() {
            
            body.removeEventListener('touchmove', onMove);
            body.removeEventListener('touchend', onUp);
            body.removeEventListener('touchcancel', stopTracking);
            body.removeEventListener('mouseup', onUp);
            body.removeEventListener('mousemove', onMove);
        }

        /**
         * Creates a custom normalized event object from touch and mouse events
         * @param  {Event} ev
         * @returns {Object} with x, y, and id properties
         */
        function normalizeEvent(ev) {
            if (ev.type === 'touchmove' || ev.type === 'touchstart' || ev.type === 'touchend') {
                var touch = ev.targetTouches[0] || ev.changedTouches[0];
                return {
                    x: touch.clientX,
                    y: touch.clientY,
                    id: touch.identifier
                };
            } else { // mouse events
                return {
                    x: ev.clientX,
                    y: ev.clientY,
                    id: null
                };
            }
        }

        function addTrackingPoint(point){
            trackingPoints.push(point);
            if(trackingPoints.length>6){
                trackingPoints.shift();
            }
        }

        function getXSpeed(){
            let p1=trackingPoints[0],
                p2=trackingPoints[trackingPoints.length-1],
                speed=(p2.x-p1.x)/(p2.timestamp-p1.timestamp)*1000;

            speed/=4;
            return Math.abs(speed)>10?speed:0;
        }

        function getYSpeed(){
            let p1=trackingPoints[0],
                p2=trackingPoints[trackingPoints.length-1],
                speed=(p2.y-p1.y)/(p2.timestamp-p1.timestamp)*1000;
            speed/=4;
            return Math.abs(speed)>10?speed:0;
        }

        function checkBounds(values,bounds){
            let newValues={x:values.x,y:values.y,timestamp:values.timestamp};

            if (bounds.x[0] !== undefined && values.x < bounds.x[0]) {
                newValues.x=bounds.x[0] - (bounds.x[0] - newValues.x)/4
            }

            if (bounds.x[1] !== undefined && values.x > bounds.x[1]) {
                newValues.x=bounds.x[1] - (bounds.x[1] - newValues.x)/4
            }

            if (bounds.y[0] !== undefined && values.y < bounds.y[0]) {
                newValues.y=bounds.y[0] - (bounds.y[0] - newValues.y)/4
            }

            if (bounds.y[1] !== undefined && values.y > bounds.y[1]) {
                newValues.y=bounds.y[1] - (bounds.y[1] - newValues.y)/4
            }

            return newValues;
        }

        function getLandingPoint({speed,currentPos,boundX,boundY,snapOn,friction}){

            let limitX=speed.x>0?boundX[1]-lastValues.x:boundX[0]-lastValues.x,
                limitY=speed.y>0?boundY[1]-lastValues.y:boundY[0]-lastValues.y;


            let decX=new Decelerator(speed.x,speed.x>0?-friction:friction,currentPos.x,snapOn.width,limitX),
                decY=new Decelerator(speed.y,speed.y>0?-friction:friction,currentPos.y,snapOn.height,limitY);
            
            // console.info("limitX",limitX);
            // console.info("limitY",limitY);

            // console.log("decX",decX)
            // console.log("decY",decY)

            let point={
                x:currentPos.x+decX.FinalPos,
                y:currentPos.y+decY.FinalPos,
                t:Math.max(decX.Duration , decY.Duration)
            }
            
            // console.log(point)
            return point;
        };
    }
}







export const Momentum = function(configObj){
    var options=Object.assign({V:300,a:-200,callback:console.log,onEnd:console.log},configObj);

    var easing=new Decelerator(options.V,options.a,options.snapX,options.limit), 
        duration=easing.getDuration(),
        start=null,
        now=null,
        animFramHnd=null;

    // this function calculates the inertia an element has when dragged and released at a given speed
    function init(){
        easing=new Decelerator(options.V,options.a,options.snapX,options.limit);
        duration=easing.getDuration();
        start=null;
        now=null;
        animFramHnd=null;
    }
    

    function step(timestamp){
        start=start||timestamp;

        now=timestamp;

        var t=(timestamp-start)/1000;

        if(t<duration){
            options.callback(easing.getPos(t));
            animFramHnd=window.requestAnimationFrame(step);
        }else{
            options.callback(easing.getPos(duration));
            options.onEnd(easing.getPos(duration));
            animFramHnd=null;
        }
    }


    var obj={
        start:()=>{
            animFramHnd=window.requestAnimationFrame(step);
            return obj;
        },
        config:(configObj)=>{

            options=Object.assign(options,configObj);

            init();
            return obj;
        },
        cancel:()=>{
            if(animFramHnd){
                window.cancelAnimationFrame(animFramHnd); 
                animFramHnd=null;     
                var t=(now-start)/1000;

                options.onEnd(easing.getPos(t));          
            }
            return obj;
        }
    }

    return obj
}
