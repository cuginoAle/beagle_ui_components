// see this for possible easing curves: http://tweenjs.github.io/tween.js/examples/03_graphs.html 
import TWEEN from 'tween.js';

var valueObj={x:0},
    animationComplete,
    tween=new TWEEN
                .Tween(valueObj)
                .onComplete(()=>{
                    animationComplete=true;
                }),
    animFramHnd=null;


function animate({
                    easing=null,
                    fade=null,
                    duration=1000,
                    callback=console.log,
                    onEnd=console.log
                }){
    
    valueObj.x=0;
    
    reset();

    animationComplete=false;

    tween
        .easing(easing?TWEEN.Easing[easing][fade]:TWEEN.Easing.Elastic.Out)
        .to({x:1},duration)
        .start();


    animFramHnd=window.requestAnimationFrame(step);


    function step(){
        if(!animationComplete){
            TWEEN.update();            
            animFramHnd=window.requestAnimationFrame(step);
            callback(valueObj.x);
        }else{
            callback(valueObj.x);
            onEnd();
        }
    }

    function reset(){
        window.cancelAnimationFrame(animFramHnd);  
        tween.stop();    
        animationComplete=true;        
    }


    return{
        cancel:reset
    }

}
export default animate;
