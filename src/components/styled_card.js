import styled from 'styled-components';
import styleVars from 'js/styleVars.js';

import Card from 'components/card.js';

let Styled_card=styled(Card)`
    border:1px solid ${styleVars.colors.green15};
    background-color:${styleVars.colors.green20};
    border-radius: 5px;
    padding:14px;
    position: relative;            
    

    &.removable {
        padding-right:70px;
    }
    .hideBtn{
        font-size: 16px;
        position: absolute;
        top:0px;
        right:0px;
        width:auto;
        margin:0;
        padding:0 .5em;
        background-color:transparent;
        display: flex;
        align-items: center;
        color: ${styleVars.colors.blue10};

        .svg_icon {
            font-size: 12px;
            margin-left:.5em;
        }
    } 

`;

export default Styled_card;