import React from 'react';
import Svg_icon from './svg_icon.js';
import animate from './js/animate.js';
import styled from 'styled-components';
import styleVars from './js/styleVars.js';

class Accordion extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Accordion';

        this.state={
            showContent:props.showContent
        }

        this.handleClick=this.handleClick.bind(this);
    }

    componentDidMount() {
        this.checkContentHeight()
        this.contentElem.style.overflow="hidden"
    }
    componentDidUpdate(prevProps, prevState) {
        this.checkContentHeight()
    }

    checkContentHeight(){
        let elem=this.contentElem,
            oldHeight=elem.offsetHeight;

        if(oldHeight==0){
            elem.firstChild.style.display="block";
        }
            
        let contentHeight=elem.scrollHeight,
            newHeight=this.state.showContent?contentHeight:0;

        animate({
            duration:800,
            easing:"Exponential",
            fade:"Out",
            callback:(progress)=>{
                let sanitisedHeight=oldHeight - (oldHeight-newHeight)*progress;            
                let y=newHeight==0?progress:1-progress;

                elem.firstChild.style.transform="translateY(-" + (y*100) + "%)";

                elem.style.height=Math.max(0,sanitisedHeight) + "px";

            },
            onEnd:()=>{
                if(newHeight==0){
                    elem.firstChild.style.display="none";
                }else{
                    elem.style.height="auto";
                }
            }
        })      
    }

    handleClick(){
        let show=!this.state.showContent;

        this.setState({
            showContent:show
        })
    }

    render() {
        let classes=['crx_accordion'];
        this.props.className && classes.push(this.props.className);
        this.state.showContent && classes.push("expanded");

        return (
            <S_Accordion className={classes.join(" ")}>
                <button className="crx_accordion_switch_btn" onClick={this.handleClick}>
                    <span className="switch_btn_title">{this.props.caption}</span>
                    <Svg_icon icon="down"/>
                </button>
                <div ref={(el)=>{this.contentElem=el}} className="crx_accordion_content">
                    {this.props.children}
                </div>
            </S_Accordion>
        )
    }
}

export default Accordion;

Accordion.defaultProps={
    showContent:false
}

const S_Accordion=styled.div`
    border-bottom:1px solid ${styleVars.colors.grey10};
    border-top   :1px solid ${styleVars.colors.grey10};


    .crx_accordion_content {
        >*{            
            display:none;
            box-sizing:border-box;
            margin:14px 0;
        }
    }
    .crx_accordion_switch_btn {
        align-items     : center;
        background-color: transparent;
        border          : none;
        color           : ${styleVars.colors.blue10};
        display         : flex;
        font-size       : 16px;
        font-weight     : 400;
        justify-content : space-between;
        line-height     : normal;
        margin          : 0;
        outline         : none;
        padding         : 8px 12px;
        width           : 100%;

        &:active{

        }

        .svg_icon {
            align-self      :center;
            font-size       :14px;
            transform-origin:50%;
            transition      :transform .6s;
        }
        
    }

    &.expanded {
        .crx_accordion_switch_btn {
            .svg_icon {
                transform:rotateZ(180deg);
            }            
        }
    }
`;