import React from 'react';

import styled from 'styled-components';
import styleVars from 'js/styleVars.js';
import {getKey} from 'js/utility.js';


function Drop_down({options=[],onChange=()=>{},caption,name,value}){
    let Wrapper=styled.label`
        
        span {
            font-weight: bold;
            font-size: 14px;
            display: block;
            line-height: 1;
            margin-bottom:5px;
        }

        select {
            font-size: 14px;
            margin:0;
        }
    `;


    return (
        <Wrapper>
            {caption?(<span>{caption}</span>):("")}
            <select name="{name}" onChange={onChange} value={value}>
                {options.map((opt)=>{
                    return (<option key={getKey()} value={opt.value}>{opt.label}</option>)
                })}
            </select>
        </Wrapper>
    )
}

export default Drop_down;