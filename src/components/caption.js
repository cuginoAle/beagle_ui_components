import React from 'react';

function Caption(props){
    let classes=["caption"];

    props.className && classes.push(props.className);
    
    return (
        <span style={props.style} className={classes.join(" ")}>{props.children}</span>
    )
}

export default Caption;
