import React from 'react';
import {render} from 'react-dom';

import SyntaxHighlighter from 'react-syntax-highlighter';

import styleVars from './js/styleVars.js';
import styled from 'styled-components';



class Code_highlighter extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Code_highlighter';
    }
    render() {
        let html=this.props.keepIndentation ? this.props.children : normaliseIndentation(this.props.children),
            classes=["code_highlighter"],
            Wrapper=styled.div`
                background-color: #e4e9ea;
                color           : #8296a9;
                padding         : 0 10px;
                
                .hljs {
                    overflow:auto;
                    margin:0;
                }
                .hljs-tag {
                    // background-color: #f3f7f9;
                    // padding         : 1px 5px;
                    // border-radius   : 20px;
                    // margin          : 1px;
                    // display         : inline-block;
                }

                .hljs-name {
                    // font-weight: bold;
                    color: ${styleVars.colors.blue10};
                }

                .hljs-attr{
                    color:#94b33c;
                }

                .hljs-string {
                    color:#deb887;
                }
            `;

        this.props.className && classes.push(this.props.className)

        return (
            <Wrapper style={this.props.style} className={classes.join(" ")}>
                <SyntaxHighlighter language='xml' useInlineStyles={false}>{html}</SyntaxHighlighter>                        
            </Wrapper>
        )
    }
}


function normaliseIndentation(html){
    let leadingSpaces=html.split("<")[0].replace("\n","");

    return html.split(leadingSpaces+"<").join("<")
}
export default Code_highlighter;
