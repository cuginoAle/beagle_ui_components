//components
import Accordion from "./components/accordion";
import animate from "./components/js/animate";
import Badge from "./components/badge";
import Button from "./components/button";
import Icon_button from "./components/icon_button";
import Caption from "./components/caption";
import Checkbox from "./components/checkbox";
import Link from "./components/link";
import Card from "./components/card";
import Svg_icon from "./components/svg_icon";

import Tesco_logo from "./components/tesco_logo";
import Tesco_monochrome_logo from "./components/tesco_monochrome_logo";
import Code_highlighter from "./components/code_highlighter";

import IOS_selector from "./components/ios_selector";
import Tab_menu from "./components/tab_menu";
import Product_tile from "./components/product_tile";
import Spinner from "./components/spinner";
import Message_box from "./components/message_box";
import Modal_dialog from "./components/modal_dialog";
import Drawer from "./components/drawer";
import SearchBox from './components/searchBox';
import SearchBoxWithSuggestions from './components/searchBoxWithSuggestions';
import Datalist from './components/dataList';



//utility
import {$,$$,deb,getKey,ready,WithLongTap,MakeSelectable} from "./components/js/utility";
import styleVars from "./components/js/styleVars";

export {
    $$,
    $,
    ready,
    WithLongTap,
    MakeSelectable,
    deb,
    styleVars,
    Accordion,
    Drawer,
    Code_highlighter,
    animate,
    Badge,
    Button,
    Icon_button,
    Caption,
    Checkbox,
    getKey,
    Tesco_logo,
    Tesco_monochrome_logo,
    Card,
    Link,
    IOS_selector,
    Tab_menu,
    Svg_icon,
    
    
    Spinner,
    Message_box,
    Modal_dialog,
    Product_tile,
    SearchBox,
    SearchBoxWithSuggestions,
    Datalist
};

// Products.all([],(products)=>{console.log(products)})

