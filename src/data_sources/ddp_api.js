import DDP from "ddp.js";


const ddpConn=new DDP({
    // endpoint:"wss://nowledge.xyz:9000/websocket",
    endpoint:"ws://ddpapi.nowledge.xyz/websocket",
    // endpoint:"ws://localhost:3400/websocket",
    // endpoint:"ws://52.51.184.63:2800/websocket",
    SocketConstructor: WebSocket
})


ddpConn.on("connected", () => {
    console.log("Connected");
});

var callbacksHash={};
        
ddpConn.on("result",message=>{
    console.log("result" , message)
    callbacksHash["_index_" + message.id](message);

    delete callbacksHash[message.id];
})

ddpConn.exec=function(method,params,cb){
    var mId="_index_" + ddpConn.method(method,params);
    callbacksHash[mId]=cb;
}


export default ddpConn