import collection_handler from './collection_handler'

let Keywords=collection_handler.addCollection("Top_keywords")

//subscriptions
export default {
    popularSearches:function(paramsArr=[],cb){
        Keywords.addSubscription("popular_searches",paramsArr,cb);
    }
}
