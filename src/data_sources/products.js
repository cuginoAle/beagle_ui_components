import collection_handler from './collection_handler'

let Products=collection_handler.addCollection("Products")

//subscriptions
export default {
    all:function(paramsArr=[],cb){
        Products.addSubscription("all_products",paramsArr,cb);    
    }, 
    byKeyword:function(paramsArr=[],cb){
        Products.addSubscription("products_by_keyword",paramsArr,cb);
    },
    byIsleId:function(paramsArr=[],cb){
        Products.addSubscription("products_by_aisleId",paramsArr,cb);        
    },
    byShelfId:function(paramsArr=[],cb){
        Products.addSubscription("products_by_shelfId",paramsArr,cb);        
    },
    by_EANBarcode:function(paramsArr=[],cb){
        Products.addSubscription("products_by_EANBarcode",paramsArr,cb);        
    }
}

