import ddpConn from "./ddp_api";

const collection_handler={
    addCollection:function(collectionName){

        var subscriptions={},
        isReady=false,
        recordsHash={};

        ddpConn.on("ready",(message)=>{

            message.subs.forEach((subId)=>{
                let subscr=subscriptions[subId];

                if(subscr){
                    isReady=true
                    subscr.cb(Object.values(recordsHash));
                } 
            })
        })

        ddpConn.on("added",(message)=>{
            if(message.collection==collectionName){        
                recordsHash[message.id]=message.fields;
                if(isReady){
                    for(let subscr in subscriptions){
                        subscr.cb(Object.values(recordsHash));
                    }
                }
            }
        })

        ddpConn.on("changed",(message)=>{
            console.warn("Changed")
            console.warn(message)
        })  

        return {
            addSubscription:function(subscriptionName,paramsArr,cb){
                let id=ddpConn.sub(subscriptionName,paramsArr);

                console.warn(paramsArr)
                
                subscriptions[id]={
                    id,
                    name:subscriptionName,
                    cb:cb
                }

                isReady=false;
            }
        } 
    }
}




export default collection_handler;