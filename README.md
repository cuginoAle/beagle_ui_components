# ui_components
A set of React components to build prototypes

Demo:
http://ui_components.nowledge.xyz/

## To use the components in your project

    1 - Create, in the root of the project, a .npmrc file.
    2 - Copy/paste the following line: @tesco_ux:registry=http://npm.user3xperience.com 
    This file tells NPM that all the packages scoped `tesco_ux` should be resolved to the registry `http://npm.user3xperience.com`.
    
    3 - run "npm i --save @tesco_ux/ui_components"


## If you're a dev and want to contribute
    1 - Clone the repo
    2 - run "npm install"
    3 - To build just the components run "npm run prepare"
    4 - To build just the Demo page then run "npm run gh_pages"
    5 - To build both then "npm run build"
    6 - To have both re-built automatically whenever a file changes: "npm run build-watch"
