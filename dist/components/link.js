(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'styled-components', './js/styleVars.js', './svg_icon.js'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('styled-components'), require('./js/styleVars.js'), require('./svg_icon.js'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.styledComponents, global.styleVars, global.svg_icon);
        global.link = mod.exports;
    }
})(this, function (exports, _react, _styledComponents, _styleVars, _svg_icon) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _styleVars2 = _interopRequireDefault(_styleVars);

    var _svg_icon2 = _interopRequireDefault(_svg_icon);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n            display        : inline-block;\n            text-decoration: none;\n\n            .content{\n                align-items    : center;\n                color          : ', ';\n                display        : flex;            \n                font-size      : 14px;\n                font-weight    : 400;\n                line-height    : 1.2;\n                justify-content: center;\n            }\n\n            .svg_icon {\n                margin-left: .3em;\n            }\n            &:hover{\n                .caption{\n                    text-decoration: underline;\n                }\n                \n            }\n\n        '], ['\n            display        : inline-block;\n            text-decoration: none;\n\n            .content{\n                align-items    : center;\n                color          : ', ';\n                display        : flex;            \n                font-size      : 14px;\n                font-weight    : 400;\n                line-height    : 1.2;\n                justify-content: center;\n            }\n\n            .svg_icon {\n                margin-left: .3em;\n            }\n            &:hover{\n                .caption{\n                    text-decoration: underline;\n                }\n                \n            }\n\n        ']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    function Link(props) {
        var Wrapper = _styledComponents2.default.a(_templateObject, _styleVars2.default.colors.blue10);
        return _react2.default.createElement(
            Wrapper,
            { id: props.id, style: props.style, href: props.href },
            _react2.default.createElement(
                'span',
                { className: 'content' },
                _react2.default.createElement(
                    'span',
                    { className: 'caption' },
                    props.caption
                ),
                props.icon ? _react2.default.createElement(_svg_icon2.default, { icon: props.icon }) : ""
            )
        );
    }

    exports.default = Link;
});