(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'react-dom', './svg_icon', 'styled-components'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('react-dom'), require('./svg_icon'), require('styled-components'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.reactDom, global.svg_icon, global.styledComponents);
        global.drawer = mod.exports;
    }
})(this, function (exports, _react, _reactDom, _svg_icon, _styledComponents) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _svg_icon2 = _interopRequireDefault(_svg_icon);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n            background-color:rgba(0, 69, 119, 0.87);\n            box-sizing      :border-box;            \n            position        :fixed;\n            transition      :transform .3s;\n            z-index         :990;\n\n        '], ['\n            background-color:rgba(0, 69, 119, 0.87);\n            box-sizing      :border-box;            \n            position        :fixed;\n            transition      :transform .3s;\n            z-index         :990;\n\n        ']),
        _templateObject2 = _taggedTemplateLiteral(['\n                        top       :0;\n                        left      :0;\n                        transform :translateY(-100%);\n                        width     :100%;\n                        box-shadow:0 1px 4px rgba(0,0,0,.5);\n\n                        .show &{\n                            transform:translateY(0);\n                        }\n                    '], ['\n                        top       :0;\n                        left      :0;\n                        transform :translateY(-100%);\n                        width     :100%;\n                        box-shadow:0 1px 4px rgba(0,0,0,.5);\n\n                        .show &{\n                            transform:translateY(0);\n                        }\n                    ']),
        _templateObject3 = _taggedTemplateLiteral(['\n                        bottom   :0;\n                        left     :0;\n                        transform:translateY(100%);\n                        width    :100%;\n                        box-shadow:0 -1px 4px rgba(0,0,0,.5);\n\n                        .show &{\n                            transform:translateY(0);\n                        }\n                    '], ['\n                        bottom   :0;\n                        left     :0;\n                        transform:translateY(100%);\n                        width    :100%;\n                        box-shadow:0 -1px 4px rgba(0,0,0,.5);\n\n                        .show &{\n                            transform:translateY(0);\n                        }\n                    ']),
        _templateObject4 = _taggedTemplateLiteral(['\n                        left     :0;\n                        top      :0;\n                        transform:translateX(-100%);\n                        height   :100%;\n                        box-shadow:1px 0 4px rgba(0,0,0,.5);\n\n                        .show &{\n                            transform:translateX(0);\n                        }\n                    '], ['\n                        left     :0;\n                        top      :0;\n                        transform:translateX(-100%);\n                        height   :100%;\n                        box-shadow:1px 0 4px rgba(0,0,0,.5);\n\n                        .show &{\n                            transform:translateX(0);\n                        }\n                    ']),
        _templateObject5 = _taggedTemplateLiteral(['\n                        right    :0;\n                        top      :0;\n                        transform:translateX(100%);\n                        height   :100%;\n                        box-shadow:-1px 0 4px rgba(0,0,0,.5);\n\n                        .show &{\n                            transform:translateX(0);\n                        }\n                    '], ['\n                        right    :0;\n                        top      :0;\n                        transform:translateX(100%);\n                        height   :100%;\n                        box-shadow:-1px 0 4px rgba(0,0,0,.5);\n\n                        .show &{\n                            transform:translateX(0);\n                        }\n                    ']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    function Drawer() {

        var Wrapper = _styledComponents2.default.div(_templateObject),
            onCloseCB = null;

        var div = document.createElement("div");

        div.addEventListener("webkitTransitionEnd", function () {
            if (!div.classList.contains("show")) {
                document.documentElement.removeChild(div);
            }
        });

        var _showContent = function _showContent(children) {
            var from = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "top";


            var PositionedWrapper = null;

            switch (from) {
                case "top":
                    PositionedWrapper = (0, _styledComponents2.default)(Wrapper)(_templateObject2);
                    break;

                case "bottom":
                    PositionedWrapper = (0, _styledComponents2.default)(Wrapper)(_templateObject3);

                    break;

                case "left":
                    PositionedWrapper = (0, _styledComponents2.default)(Wrapper)(_templateObject4);
                    break;

                case "right":
                    PositionedWrapper = (0, _styledComponents2.default)(Wrapper)(_templateObject5);
                    break;
            }

            (0, _reactDom.render)(_react2.default.createElement(
                PositionedWrapper,
                { className: 'drawer' },
                children
            ), div);

            document.documentElement.appendChild(div);
            setTimeout(function () {
                div.classList.add("show");
            }, 20);
        };

        var msgObj = {
            show: function show(children) {
                var from = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "top";
                var onClose = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

                _showContent(children, from);
                onClose && (onCloseCB = onClose);
            },
            close: function close() {
                onCloseCB && onCloseCB();
                div.classList.remove("show");
            }
        };

        return msgObj;
    }

    exports.default = Drawer();
});