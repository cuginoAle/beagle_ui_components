(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', './js/styleVars.js'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('./js/styleVars.js'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.styleVars);
        global.badge = mod.exports;
    }
})(this, function (exports, _react, _styleVars) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _styleVars2 = _interopRequireDefault(_styleVars);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    function Badge(props) {
        var classes = ["crx_badge"];

        props.className && classes.push(props.className);

        var style = {
            backgroundColor: _styleVars2.default.colors.yellow8,
            padding: "5px 20px",
            color: "white",
            fontWeight: "400",
            borderRadius: "100px",
            display: "inline-block"
        };

        var badgeStyle = Object.assign(style, props.style);

        return _react2.default.createElement(
            'span',
            { id: props.id, className: classes.join(" "), style: badgeStyle },
            props.caption
        );
    }

    exports.default = Badge;
});