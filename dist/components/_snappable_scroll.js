(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'lodash.debounce', './js/animate.js', 'impetus'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('lodash.debounce'), require('./js/animate.js'), require('impetus'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.lodash, global.animate, global.impetus);
        global._snappable_scroll = mod.exports;
    }
})(this, function (exports, _react, _lodash, _animate, _impetus) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _lodash2 = _interopRequireDefault(_lodash);

    var _animate2 = _interopRequireDefault(_animate);

    var _impetus2 = _interopRequireDefault(_impetus);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var _snappable_scroll = function (_React$Component) {
        _inherits(_snappable_scroll, _React$Component);

        function _snappable_scroll(props) {
            _classCallCheck(this, _snappable_scroll);

            var _this = _possibleConstructorReturn(this, (_snappable_scroll.__proto__ || Object.getPrototypeOf(_snappable_scroll)).call(this, props));

            _this.displayName = '_snappable_scroll';

            var self = _this,
                animation = props.animation || {};

            self.touchend = false;
            self.anim = null;

            _this.transformComponent = _this.transformComponent.bind(_this);

            _this.snapToPosition = (0, _lodash2.default)(function (value) {
                if (self.touchend) {

                    var selectedIndex = Math.abs(Math.round(value / props.stepHeight)),
                        newTop = -selectedIndex * props.stepHeight - value;

                    //executing the callback... or logging the selected index
                    _this.props.onChange ? _this.props.onChange(selectedIndex) : console.log(selectedIndex);

                    self.anim = (0, _animate2.default)({
                        curve: animation.curve,
                        duration: animation.duration,
                        callback: function callback(perc) {

                            //updating Impetus
                            self.impetus.setValues(0, value + newTop * perc);

                            //translating the element
                            self.elem.style.transform = "translateY(" + (value + newTop * perc) + "px)";
                        }
                    });
                }
            }, 70);
            return _this;
        }

        _createClass(_snappable_scroll, [{
            key: 'transformComponent',
            value: function transformComponent(x, y) {

                if (this.elem) {

                    this.anim && this.anim.cancel();

                    this.elem.style.transform = "translateY(" + y + "px)";
                    this.snapToPosition(y);
                }
            }
        }, {
            key: 'componentDidMount',
            value: function componentDidMount() {
                var self = this,
                    elem = self.elem,
                    maxX = 0;

                //setting the initial position
                var initialPosition = -self.props.initialPosition * self.props.stepHeight;
                this.transformComponent(null, initialPosition);

                setTimeout(function () {
                    //forcing the browser to get the calculation right!
                    elem.scrollHeight && elem.parentNode.offsetHeight;

                    maxX = elem.parentNode.scrollHeight - elem.parentNode.offsetHeight;

                    //making sure we can scroll to the bottom of the component
                    maxX = Math.ceil(maxX / self.props.stepHeight) * self.props.stepHeight;

                    self.impetus = new _impetus2.default({
                        source: elem,
                        friction: .94,
                        boundY: [-maxX, 0],
                        bounce: true,
                        initialValues: [0, initialPosition],
                        update: self.transformComponent
                    });
                }, 20);

                elem.parentNode.addEventListener('touchmove', function (e) {
                    //preventing the page from scrolling
                    e.preventDefault();
                });

                elem.parentNode.addEventListener("touchstart", function (e) {
                    self.touchend = false;
                });
                elem.parentNode.addEventListener("mousedown", function (e) {
                    self.touchend = false;
                });

                elem.parentNode.addEventListener("touchend", function (e) {
                    self.touchend = true;
                    var newTop = parseInt(self.elem.style.transform.replace("translateY(", ""), 10);
                    self.snapToPosition(newTop);
                });

                elem.parentNode.addEventListener("mouseup", function (e) {
                    self.touchend = true;
                    var newTop = parseInt(self.elem.style.transform.replace("translateY(", ""), 10);
                    self.snapToPosition(newTop);
                });
            }
        }, {
            key: 'render',
            value: function render() {
                var _this2 = this;

                var classes = ["_snappable_scroll"];

                this.props.className && classes.push(this.props.className);

                return _react2.default.createElement(
                    'div',
                    { ref: function ref(elem) {
                            _this2.wrapper = elem;
                        }, style: this.props.style, className: classes.join(" ") },
                    _react2.default.createElement(
                        'div',
                        { ref: function ref(elem) {
                                _this2.elem = elem;
                            }, className: 'draggable_content' },
                        this.props.children
                    )
                );
            }
        }]);

        return _snappable_scroll;
    }(_react2.default.Component);

    _snappable_scroll.defaultProps = {
        initialPosition: 0
    };

    exports.default = _snappable_scroll;
});