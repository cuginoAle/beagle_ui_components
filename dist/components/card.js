(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', './svg_icon.js', 'styled-components', './js/styleVars.js'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('./svg_icon.js'), require('styled-components'), require('./js/styleVars.js'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.svg_icon, global.styledComponents, global.styleVars);
        global.card = mod.exports;
    }
})(this, function (exports, _react, _svg_icon, _styledComponents, _styleVars) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _svg_icon2 = _interopRequireDefault(_svg_icon);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _styleVars2 = _interopRequireDefault(_styleVars);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n            ', ';\n            background-color:', ';\n            border-radius   :', ';\n            padding:14px;\n            position: relative;            \n\n            .card_title {\n                display    :flex;\n                align-items:center;\n                font-size  :16px;\n                margin     :0 0 14px;\n                height     :19px;\n\n                .svg_icon {\n                    font-size   :2em;\n                    margin-right:.3em;\n                }\n            }\n            \n\n            &.removable {\n                padding-right:60px;\n            }\n            .hideBtn{\n                align-items     : center;\n                background-color:transparent;\n                border          :none;\n                color           : ', ';\n                display         : flex;\n                font-size       : 16px;\n                margin          :0;\n                padding         :0 .5em;\n                position        : absolute;\n                right           :0px;\n                top             :0px;\n                width           :auto;\n\n                .svg_icon {\n                    font-size: 11px;\n                    margin-left:.5em;\n                }\n            } \n        '], ['\n            ', ';\n            background-color:', ';\n            border-radius   :', ';\n            padding:14px;\n            position: relative;            \n\n            .card_title {\n                display    :flex;\n                align-items:center;\n                font-size  :16px;\n                margin     :0 0 14px;\n                height     :19px;\n\n                .svg_icon {\n                    font-size   :2em;\n                    margin-right:.3em;\n                }\n            }\n            \n\n            &.removable {\n                padding-right:60px;\n            }\n            .hideBtn{\n                align-items     : center;\n                background-color:transparent;\n                border          :none;\n                color           : ', ';\n                display         : flex;\n                font-size       : 16px;\n                margin          :0;\n                padding         :0 .5em;\n                position        : absolute;\n                right           :0px;\n                top             :0px;\n                width           :auto;\n\n                .svg_icon {\n                    font-size: 11px;\n                    margin-left:.5em;\n                }\n            } \n        ']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var Card = function (_React$Component) {
        _inherits(Card, _React$Component);

        function Card(props) {
            _classCallCheck(this, Card);

            var _this = _possibleConstructorReturn(this, (Card.__proto__ || Object.getPrototypeOf(Card)).call(this, props));

            _this.state = {
                visible: true
            };

            _this.onClick = _this.onClick.bind(_this);
            return _this;
        }

        _createClass(Card, [{
            key: 'onClick',
            value: function onClick(e) {
                this.setState({ visible: false });
                this.props.onClick && this.props.onClick(e);
            }
        }, {
            key: 'getStyle',
            value: function getStyle() {
                var _this2 = this;

                var borderColor = _styleVars2.default.colors.grey8,
                    bgColor = "white";

                switch (this.props.theme) {
                    case "green":
                        borderColor = _styleVars2.default.colors.green15;
                        bgColor = _styleVars2.default.colors.green20;
                        break;

                    case "yellow":
                        bgColor = _styleVars2.default.colors.yellow10;
                        break;
                }

                return _styledComponents2.default.div(_templateObject, function (props) {
                    return _this2.props.noborders ? "border:none" : "border:1px solid " + borderColor;
                }, bgColor, _styleVars2.default.border.borderRadius, _styleVars2.default.colors.blue10);
            }
        }, {
            key: 'render',
            value: function render() {

                var Wrapper = this.getStyle();
                var closeButton = _react2.default.createElement(
                    'button',
                    { className: 'hideBtn', onClick: this.onClick },
                    _react2.default.createElement(
                        'span',
                        { className: 'caption' },
                        'Hide'
                    ),
                    ' ',
                    _react2.default.createElement(_svg_icon2.default, { icon: 'close' })
                );

                var classes = [this.props.className, "crx_card"];
                this.props.removable && classes.push("removable");

                var code = _react2.default.createElement(
                    Wrapper,
                    { className: classes.join(" ") },
                    this.props.removable ? closeButton : "",
                    this.props.children
                );

                return this.state.visible ? code : null;
            }
        }]);

        return Card;
    }(_react2.default.Component);

    exports.default = Card;
});