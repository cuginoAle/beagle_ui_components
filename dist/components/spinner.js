(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'styled-components'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('styled-components'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.styledComponents);
        global.spinner = mod.exports;
    }
})(this, function (exports, _react, _styledComponents) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n        to {transform: rotate(360deg);}\n    '], ['\n        to {transform: rotate(360deg);}\n    ']),
        _templateObject2 = _taggedTemplateLiteral(['\n    \n        .spinning_thing{\n            position:relative;\n            height:1em;\n            font-size:2em;\n\n            &:before {\n                content: \'\';\n                box-sizing: border-box;\n                position: absolute;\n                top: 50%;\n                left: 50%;\n                width: 1em;\n                height: 1em;\n                margin-left:-.5em;\n                margin-top:-.5em;\n                border-radius: 50%;\n                border: 3px solid rgba(0,0,0,.06);\n                border-top-color: rgba(0, 75, 144, 0.58);\n                animation: ', ' .6s linear infinite;\n            }                          \n        }\n\n        p {\n            margin-top:10px;\n            font-weight:200;\n            font-size:.8em;\n            text-align:center;\n        }\n    '], ['\n    \n        .spinning_thing{\n            position:relative;\n            height:1em;\n            font-size:2em;\n\n            &:before {\n                content: \'\';\n                box-sizing: border-box;\n                position: absolute;\n                top: 50%;\n                left: 50%;\n                width: 1em;\n                height: 1em;\n                margin-left:-.5em;\n                margin-top:-.5em;\n                border-radius: 50%;\n                border: 3px solid rgba(0,0,0,.06);\n                border-top-color: rgba(0, 75, 144, 0.58);\n                animation: ', ' .6s linear infinite;\n            }                          \n        }\n\n        p {\n            margin-top:10px;\n            font-weight:200;\n            font-size:.8em;\n            text-align:center;\n        }\n    ']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    function Spinner(props) {
        var spinner = (0, _styledComponents.keyframes)(_templateObject);

        var Wrapper = _styledComponents2.default.div(_templateObject2, spinner);
        return _react2.default.createElement(
            Wrapper,
            { className: 'spinner' },
            _react2.default.createElement('div', { className: 'spinning_thing' }),
            props.message && _react2.default.createElement(
                'p',
                null,
                props.message
            )
        );
    }

    exports.default = Spinner;
});