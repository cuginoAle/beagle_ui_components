(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'react-dom', './js/animate.js', 'styled-components'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('react-dom'), require('./js/animate.js'), require('styled-components'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.reactDom, global.animate, global.styledComponents);
        global.modal_dialog = mod.exports;
    }
})(this, function (exports, _react, _reactDom, _animate, _styledComponents) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _animate2 = _interopRequireDefault(_animate);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _extends = Object.assign || function (target) {
        for (var i = 1; i < arguments.length; i++) {
            var source = arguments[i];

            for (var key in source) {
                if (Object.prototype.hasOwnProperty.call(source, key)) {
                    target[key] = source[key];
                }
            }
        }

        return target;
    };

    var _templateObject = _taggedTemplateLiteral(['\n    background-color  :rgba(0,0,0,0);\n    display           :flex;\n    flex-direction    :column;\n    height            :100%;\n    left              :0;\n    margin            :0;\n    overflow          :hidden;\n    perspective       :600px;\n    \n    position          :fixed;\n    right             :0;\n    top               :0;\n    transition        :background-color .3s;\n    z-index           :9999;\n\n\n    .crx_modal_dialog_content {                                        \n        justify-content :center;\n        display         :flex;\n        opacity         :.2;\n        transform-origin:bottom;\n        transition      :all .4s cubic-bezier(0.56, 0.2, 0, 1) .1s;      \n\n        >*{\n            box-shadow      :rgba(0, 0, 0, 0.4) 0px 9px 12px -6px;                           \n        }\n    }\n\n    &.bottom {       \n        perspective-origin:50% 100%;\n        justify-content   :flex-end;\n\n        .crx_modal_dialog_content{\n            transform :translateY(100%);     \n        }                 \n    }\n\n    &.top {       \n        perspective-origin:50% 100%;\n        justify-content   :flex-start;\n\n        .crx_modal_dialog_content{\n            transform :translateY(-100%);     \n        }                 \n    }    \n\n    &.center {\n        perspective-origin:50%;\n        justify-content   :center;\n        \n        .crx_modal_dialog_content{\n            padding   :10px;              \n            transform :scale(.8);\n            transition:all .4s cubic-bezier(0.34, 0.38, 0.35, 1.6);     \n        }\n    }\n\n    &.showMD{\n        background-color  :rgba(0,0,0,.6);\n\n        .crx_modal_dialog_content {\n            transform:translateY(0) scale(1);\n            opacity:1;\n        }\n    }\n'], ['\n    background-color  :rgba(0,0,0,0);\n    display           :flex;\n    flex-direction    :column;\n    height            :100%;\n    left              :0;\n    margin            :0;\n    overflow          :hidden;\n    perspective       :600px;\n    \n    position          :fixed;\n    right             :0;\n    top               :0;\n    transition        :background-color .3s;\n    z-index           :9999;\n\n\n    .crx_modal_dialog_content {                                        \n        justify-content :center;\n        display         :flex;\n        opacity         :.2;\n        transform-origin:bottom;\n        transition      :all .4s cubic-bezier(0.56, 0.2, 0, 1) .1s;      \n\n        >*{\n            box-shadow      :rgba(0, 0, 0, 0.4) 0px 9px 12px -6px;                           \n        }\n    }\n\n    &.bottom {       \n        perspective-origin:50% 100%;\n        justify-content   :flex-end;\n\n        .crx_modal_dialog_content{\n            transform :translateY(100%);     \n        }                 \n    }\n\n    &.top {       \n        perspective-origin:50% 100%;\n        justify-content   :flex-start;\n\n        .crx_modal_dialog_content{\n            transform :translateY(-100%);     \n        }                 \n    }    \n\n    &.center {\n        perspective-origin:50%;\n        justify-content   :center;\n        \n        .crx_modal_dialog_content{\n            padding   :10px;              \n            transform :scale(.8);\n            transition:all .4s cubic-bezier(0.34, 0.38, 0.35, 1.6);     \n        }\n    }\n\n    &.showMD{\n        background-color  :rgba(0,0,0,.6);\n\n        .crx_modal_dialog_content {\n            transform:translateY(0) scale(1);\n            opacity:1;\n        }\n    }\n']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    var Wrapper = _styledComponents2.default.div(_templateObject);

    var Modal_dialog = function () {
        var div = document.createElement("div"),
            modalStack = [];

        div.addEventListener("webkitTransitionEnd", function () {

            if (!div.querySelector(".crx_modal_dialog").classList.contains("showMD")) {
                document.documentElement.removeChild(div);

                var modObj = modalStack.shift();
                modObj.onCloseCB && modObj.onCloseCB();
            }
        });

        var _show = function _show() {

            var modalObj = modalStack[0],
                classes = ["crx_modal_dialog", modalObj.align];

            (0, _reactDom.render)(_react2.default.createElement(
                Wrapper,
                { className: classes.join(" ") },
                _react2.default.createElement(
                    'div',
                    { className: 'crx_modal_dialog_content' },
                    modalObj.children
                )
            ), div);

            document.documentElement.appendChild(div);

            var content = div.querySelector(".crx_modal_dialog");

            setTimeout(function () {
                content.classList.add("showMD");

                content.addEventListener('touchmove', function (e) {
                    //preventing the page from scrolling

                    var sourceElement = e.target || e.srcElement;
                    if (sourceElement.classList.contains("crx_modal_dialog")) {
                        e.preventDefault();
                    }
                });

                content.addEventListener('click', function (e) {

                    //if the user clicked on the BG element...
                    if (e.target.classList.contains("crx_modal_dialog_content") || e.target.classList.contains("crx_modal_dialog")) {
                        modalObj.onBgClick && modalObj.onBgClick();
                    }
                });

                modalObj.onReady && modalObj.onReady(content);
            }, 20);
        },
            _close = function _close() {
            div.querySelector(".crx_modal_dialog").classList.remove("showMD");
        };

        return {
            show: function show(options) {
                // children,align="bottom",onCloseCB=()=>{console.log("Modal_dialog: onClose")},onBgClick=()=>{}            
                modalStack.push(_extends({}, options));
                _show();
            },
            close: _close
        };
    }();

    exports.default = Modal_dialog;
});