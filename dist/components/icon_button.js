(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'styled-components', './button', './svg_icon'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('styled-components'), require('./button'), require('./svg_icon'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.styledComponents, global.button, global.svg_icon);
        global.icon_button = mod.exports;
    }
})(this, function (exports, _react, _styledComponents, _button, _svg_icon) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _button2 = _interopRequireDefault(_button);

    var _svg_icon2 = _interopRequireDefault(_svg_icon);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _extends = Object.assign || function (target) {
        for (var i = 1; i < arguments.length; i++) {
            var source = arguments[i];

            for (var key in source) {
                if (Object.prototype.hasOwnProperty.call(source, key)) {
                    target[key] = source[key];
                }
            }
        }

        return target;
    };

    var _templateObject = _taggedTemplateLiteral(['\n        .wrapper {\n            padding:0;\n        }\n\n        .svg_icon{\n            margin:0;\n        }\n    '], ['\n        .wrapper {\n            padding:0;\n        }\n\n        .svg_icon{\n            margin:0;\n        }\n    ']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    var Wrapper = (0, _styledComponents2.default)(_button2.default)(_templateObject);

    exports.default = function (props) {
        var classes = ["icon_button"];

        props.button.className && classes.push(props.button.className);

        return _react2.default.createElement(
            Wrapper,
            _extends({ className: classes.join(" ") }, props.button),
            _react2.default.createElement(_svg_icon2.default, { icon: props.icon })
        );
    };
});