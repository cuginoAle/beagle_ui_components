(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(["exports"], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports);
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports);
        global.styleVars = mod.exports;
    }
})(this, function (exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var style = {
        colors: {
            lightGrey: "#f6f6f6",
            grey20: "#eee",
            grey10: "#ccc",
            grey8: "#999",
            grey5: "#666",
            grey3: "#333",
            green5: "#008209",
            green3: "#339933",
            green10: "#417505",
            green15: "#509500",
            green17: "#94b33c",
            green20: "#ECF3E4",

            yellow10: "#fffde5",
            yellow8: "#ffc200",
            yellow5: "#D5B647",

            blue15: "#dfe9f6",
            blue10: "#00539F",
            blue5: "#004380",

            orange5: "#ef9201"

        },

        border: {
            borderRadius: "3px"
        }
    };

    exports.default = style;
});