(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'tween.js'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('tween.js'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.tween);
        global.animate = mod.exports;
    }
})(this, function (exports, _tween) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _tween2 = _interopRequireDefault(_tween);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var valueObj = { x: 0 },
        animationComplete,
        tween = new _tween2.default.Tween(valueObj).onComplete(function () {
        animationComplete = true;
    }),
        animFramHnd = null; // see this for possible easing curves: http://tweenjs.github.io/tween.js/examples/03_graphs.html 


    function animate(_ref) {
        var _ref$easing = _ref.easing,
            easing = _ref$easing === undefined ? null : _ref$easing,
            _ref$fade = _ref.fade,
            fade = _ref$fade === undefined ? null : _ref$fade,
            _ref$duration = _ref.duration,
            duration = _ref$duration === undefined ? 1000 : _ref$duration,
            _ref$callback = _ref.callback,
            callback = _ref$callback === undefined ? console.log : _ref$callback,
            _ref$onEnd = _ref.onEnd,
            onEnd = _ref$onEnd === undefined ? console.log : _ref$onEnd;


        valueObj.x = 0;

        reset();

        animationComplete = false;

        tween.easing(easing ? _tween2.default.Easing[easing][fade] : _tween2.default.Easing.Elastic.Out).to({ x: 1 }, duration).start();

        animFramHnd = window.requestAnimationFrame(step);

        function step() {
            if (!animationComplete) {
                _tween2.default.update();
                animFramHnd = window.requestAnimationFrame(step);
                callback(valueObj.x);
            } else {
                callback(valueObj.x);
                onEnd();
            }
        }

        function reset() {
            window.cancelAnimationFrame(animFramHnd);
            tween.stop();
            animationComplete = true;
        }

        return {
            cancel: reset
        };
    }
    exports.default = animate;
});