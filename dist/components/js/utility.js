(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', '../svg_icon', 'styled-components'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('../svg_icon'), require('styled-components'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.svg_icon, global.styledComponents);
        global.utility = mod.exports;
    }
})(this, function (exports, _react, _svg_icon, _styledComponents) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.deb = exports.ready = exports.WithLongTap = exports.MakeSelectable = exports.getKey = exports.$$ = exports.$ = undefined;

    var _react2 = _interopRequireDefault(_react);

    var _svg_icon2 = _interopRequireDefault(_svg_icon);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _extends = Object.assign || function (target) {
        for (var i = 1; i < arguments.length; i++) {
            var source = arguments[i];

            for (var key in source) {
                if (Object.prototype.hasOwnProperty.call(source, key)) {
                    target[key] = source[key];
                }
            }
        }

        return target;
    };

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var _templateObject = _taggedTemplateLiteral(['\n    -webkit-user-select: none;  /* Chrome all / Safari all */\n    -moz-user-select: none;     /* Firefox all */\n    -ms-user-select: none;      /* IE 10+ */\n    user-select: none;          /* Likely future */    \n\n    -webkit-touch-callout: none; /* disable the IOS popup when long-press on a link */\n'], ['\n    -webkit-user-select: none;  /* Chrome all / Safari all */\n    -moz-user-select: none;     /* Firefox all */\n    -ms-user-select: none;      /* IE 10+ */\n    user-select: none;          /* Likely future */    \n\n    -webkit-touch-callout: none; /* disable the IOS popup when long-press on a link */\n']),
        _templateObject2 = _taggedTemplateLiteral(['\n\n    position:relative;\n\n    .wrappable_content{\n        transition: transform 0.4s cubic-bezier(0.16, 0.76, 0.43, 1.3);\n        transform:scale3d(1,1,1);\n        \n    }\n\n    &>.svg_icon{\n        position:absolute;\n        top:5px;\n        left:5px;\n        fontSize:2.2em;\n        padding:.2em;\n        borderRadius:50%;\n        border:1px solid rgba(0,0,0,.2);\n        backgroundColor:transparent;\n        color:transparent;\n        transition: all 0.4s cubic-bezier(0.16, 0.76, 0.43, 1.3);\n    },\n\n    &.selected{\n\n        .wrappable_content{\n            transform:scale3d(.85,.85,1);                \n        }\n        \n        &>.svg_icon{\n            border:1px solid rgba(0,0,0,0);\n            backgroundColor:green;\n            color:white;     \n        }\n    }\n\n'], ['\n\n    position:relative;\n\n    .wrappable_content{\n        transition: transform 0.4s cubic-bezier(0.16, 0.76, 0.43, 1.3);\n        transform:scale3d(1,1,1);\n        \n    }\n\n    &>.svg_icon{\n        position:absolute;\n        top:5px;\n        left:5px;\n        fontSize:2.2em;\n        padding:.2em;\n        borderRadius:50%;\n        border:1px solid rgba(0,0,0,.2);\n        backgroundColor:transparent;\n        color:transparent;\n        transition: all 0.4s cubic-bezier(0.16, 0.76, 0.43, 1.3);\n    },\n\n    &.selected{\n\n        .wrappable_content{\n            transform:scale3d(.85,.85,1);                \n        }\n        \n        &>.svg_icon{\n            border:1px solid rgba(0,0,0,0);\n            backgroundColor:green;\n            color:white;     \n        }\n    }\n\n']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    var $ = exports.$ = function $(selector) {
        var parent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

        return parent.querySelector(selector);
    };

    var $$ = exports.$$ = function $$(selector) {
        var parent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

        return parent.querySelectorAll(selector);
    };

    var key = 0;
    var getKey = exports.getKey = function getKey() {
        return key++;
    };

    var NoTextSeletion = _styledComponents2.default.div(_templateObject);

    var SelectableWrapper = _styledComponents2.default.div(_templateObject2);

    var MakeSelectable = exports.MakeSelectable = function MakeSelectable(WrappedComponent) {

        return function (_Component) {
            _inherits(_class, _Component);

            function _class(props) {
                _classCallCheck(this, _class);

                var _this = _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this, props));

                _this.onClick = _this.onClick.bind(_this);
                return _this;
            }

            _createClass(_class, [{
                key: 'onClick',
                value: function onClick(e) {
                    if (this.props.selectableOptions.isSelectable) {
                        this.props.selectableOptions.onSelect(this.props.selectableOptions);
                        e.stopPropagation();
                    }
                }
            }, {
                key: 'render',
                value: function render() {
                    var classes = ["make_selectable"],
                        props = _extends({}, this.props);

                    delete props.selectableOptions;

                    this.props.selectableOptions.selected && classes.push("selected");
                    this.props.selectableOptions.className && classes.push(this.props.selectableOptions.className);

                    return _react2.default.createElement(
                        SelectableWrapper,
                        { className: classes.join(" "), onClickCapture: this.onClick },
                        _react2.default.createElement(
                            'div',
                            { className: 'wrappable_content' },
                            _react2.default.createElement(WrappedComponent, props)
                        ),
                        this.props.selectableOptions.isSelectable && _react2.default.createElement(_svg_icon2.default, { icon: 'ddl_icon_benefits' })
                    );
                }
            }]);

            return _class;
        }(_react.Component);
    };

    var WithLongTap = exports.WithLongTap = function WithLongTap(WrappedComponent) {
        var longTapFired = false;

        return function (_Component2) {
            _inherits(_class2, _Component2);

            function _class2(props) {
                _classCallCheck(this, _class2);

                var _this2 = _possibleConstructorReturn(this, (_class2.__proto__ || Object.getPrototypeOf(_class2)).call(this, props));

                _this2.timerHnd = null;

                _this2.touchStart = _this2.touchStart.bind(_this2);
                _this2.touchMove = _this2.touchMove.bind(_this2);
                _this2.touchEnd = _this2.touchEnd.bind(_this2);
                _this2.onClick = _this2.onClick.bind(_this2);
                return _this2;
            }

            _createClass(_class2, [{
                key: 'touchStart',
                value: function touchStart(e) {
                    var _this3 = this;

                    // e.preventDefault();
                    longTapFired = false;

                    var cb = this.props.longTapOptions.onLongTap || function (e) {
                        console.warn("please define onLongTap callback!");
                    };

                    var longTapThreshold = this.props.longTapOptions.longTapThreshold || 400;

                    this.timerHnd = setTimeout(function () {
                        longTapFired = true;
                        cb(e, _this3.props.longTapOptions);
                    }, longTapThreshold);

                    if (this.props.longTapOptions.onTouchStart) {
                        this.props.longTapOptions.onTouchStart(e);
                    }
                }
            }, {
                key: 'touchMove',
                value: function touchMove(e) {
                    clearTimeout(this.timerHnd);
                    longTapFired = false;
                    this.timerHnd = null;
                    if (this.props.longTapOptions.onTouchMove) {
                        this.props.longTapOptions.onTouchMove(e);
                    }
                }
            }, {
                key: 'touchEnd',
                value: function touchEnd(e) {
                    clearTimeout(this.timerHnd);
                    this.timerHnd = null;

                    if (this.props.longTapOptions.onTouchEnd) {
                        e.preventDefault();
                        this.props.longTapOptions.onTouchEnd(e);
                    }

                    //preventing the click event to fire
                    if (longTapFired) {
                        e.stopPropagation();
                        e.preventDefault();
                    }
                }
            }, {
                key: 'onClick',
                value: function onClick(e) {
                    if (!longTapFired) {
                        this.props.longTapOptions.onClick && this.props.longTapOptions.onClick(e, this.props.longTapOptions);
                    }
                }
            }, {
                key: 'render',
                value: function render() {
                    var props = _extends({}, this.props);
                    delete props.longTapOptions;

                    return _react2.default.createElement(
                        NoTextSeletion,
                        { className: this.props.longTapOptions.className, onClick: this.onClick, onTouchStart: this.touchStart, onTouchMove: this.touchMove, onTouchEnd: this.touchEnd },
                        _react2.default.createElement(WrappedComponent, props)
                    );
                }
            }]);

            return _class2;
        }(_react.Component);
    };

    var ready = exports.ready = function ready(fn) {

        if (document.readyState != 'loading') {
            fn();
        } else {
            document.addEventListener('DOMContentLoaded', fn);
        }
    };
    // Example
    // ready(function() {
    // Do stuff...
    // });


    //element.closest polifill (https://developer.mozilla.org/en-US/docs/Web/API/Element/closest)

    if (window.Element && !Element.prototype.closest) {
        Element.prototype.closest = function (s) {
            var matches = (this.document || this.ownerDocument).querySelectorAll(s),
                i,
                el = this;
            do {
                i = matches.length;
                while (--i >= 0 && matches.item(i) !== el) {};
            } while (i < 0 && (el = el.parentElement));
            return el;
        };
    }

    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    var deb = exports.deb = function deb(func, wait, immediate) {
        var timeout;
        return function () {
            var context = this,
                args = arguments;
            var later = function later() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };
});