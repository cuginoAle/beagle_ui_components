(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'styled-components', './js/styleVars.js', './svg_icon.js'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('styled-components'), require('./js/styleVars.js'), require('./svg_icon.js'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.styledComponents, global.styleVars, global.svg_icon);
        global.checkbox = mod.exports;
    }
})(this, function (exports, _react, _styledComponents, _styleVars, _svg_icon) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _styleVars2 = _interopRequireDefault(_styleVars);

    var _svg_icon2 = _interopRequireDefault(_svg_icon);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n        align-items: center;\n        cursor     : pointer;\n        display    : flex;\n        flex-wrap  : nowrap;\n        position   : relative;\n\n\n        input {\n            bottom  : 0;\n            height  : auto;\n            left    : 0;\n            opacity : 0;\n            position: absolute;\n            right   : 0;\n            top     : 0;\n        }\n        \n        .chkBox{\n\n                align-items     : center;\n                background-color: #fff;\n                border          : 1px solid ', ';\n                border-radius   : 2px;\n                color           : #fff;\n                display         : flex;\n                height          : 28px;\n                justify-content : center;\n                margin-right    : .5em;\n                position        : relative;\n                width           : 28px;\n                z-index         : 2;\n                \n                .svg_icon {\n                    display: none;\n                }\n\n        }\n        :checked + .chkBox{\n            background-color:', ';\n            border-color:', ';\n\n            .svg_icon {\n                display: block;\n            }\n        }\n    '], ['\n        align-items: center;\n        cursor     : pointer;\n        display    : flex;\n        flex-wrap  : nowrap;\n        position   : relative;\n\n\n        input {\n            bottom  : 0;\n            height  : auto;\n            left    : 0;\n            opacity : 0;\n            position: absolute;\n            right   : 0;\n            top     : 0;\n        }\n        \n        .chkBox{\n\n                align-items     : center;\n                background-color: #fff;\n                border          : 1px solid ', ';\n                border-radius   : 2px;\n                color           : #fff;\n                display         : flex;\n                height          : 28px;\n                justify-content : center;\n                margin-right    : .5em;\n                position        : relative;\n                width           : 28px;\n                z-index         : 2;\n                \n                .svg_icon {\n                    display: none;\n                }\n\n        }\n        :checked + .chkBox{\n            background-color:', ';\n            border-color:', ';\n\n            .svg_icon {\n                display: block;\n            }\n        }\n    ']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    function Checkbox(props) {

        var Wrapper = _styledComponents2.default.label(_templateObject, _styleVars2.default.colors.grey5, _styleVars2.default.colors.green5, _styleVars2.default.colors.green5);
        return _react2.default.createElement(
            Wrapper,
            { className: 'crx_checkBox' },
            _react2.default.createElement('input', { type: 'checkbox', onChange: props.onChange, checked: props.checked }),
            _react2.default.createElement(
                'span',
                { className: 'chkBox' },
                _react2.default.createElement(_svg_icon2.default, { icon: 'checkmark' })
            ),
            props.label ? _react2.default.createElement(
                'span',
                { className: 'caption' },
                props.label
            ) : ""
        );
    }

    exports.default = Checkbox;
});