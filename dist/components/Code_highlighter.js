(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'react-dom', 'react-syntax-highlighter', './js/styleVars.js', 'styled-components'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('react-dom'), require('react-syntax-highlighter'), require('./js/styleVars.js'), require('styled-components'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.reactDom, global.reactSyntaxHighlighter, global.styleVars, global.styledComponents);
        global.code_highlighter = mod.exports;
    }
})(this, function (exports, _react, _reactDom, _reactSyntaxHighlighter, _styleVars, _styledComponents) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _reactSyntaxHighlighter2 = _interopRequireDefault(_reactSyntaxHighlighter);

    var _styleVars2 = _interopRequireDefault(_styleVars);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n                background-color: #e4e9ea;\n                color           : #8296a9;\n                padding         : 0 10px;\n                \n                .hljs {\n                    overflow:auto;\n                    margin:0;\n                }\n                .hljs-tag {\n                    // background-color: #f3f7f9;\n                    // padding         : 1px 5px;\n                    // border-radius   : 20px;\n                    // margin          : 1px;\n                    // display         : inline-block;\n                }\n\n                .hljs-name {\n                    // font-weight: bold;\n                    color: ', ';\n                }\n\n                .hljs-attr{\n                    color:#94b33c;\n                }\n\n                .hljs-string {\n                    color:#deb887;\n                }\n            '], ['\n                background-color: #e4e9ea;\n                color           : #8296a9;\n                padding         : 0 10px;\n                \n                .hljs {\n                    overflow:auto;\n                    margin:0;\n                }\n                .hljs-tag {\n                    // background-color: #f3f7f9;\n                    // padding         : 1px 5px;\n                    // border-radius   : 20px;\n                    // margin          : 1px;\n                    // display         : inline-block;\n                }\n\n                .hljs-name {\n                    // font-weight: bold;\n                    color: ', ';\n                }\n\n                .hljs-attr{\n                    color:#94b33c;\n                }\n\n                .hljs-string {\n                    color:#deb887;\n                }\n            ']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var Code_highlighter = function (_React$Component) {
        _inherits(Code_highlighter, _React$Component);

        function Code_highlighter(props) {
            _classCallCheck(this, Code_highlighter);

            var _this = _possibleConstructorReturn(this, (Code_highlighter.__proto__ || Object.getPrototypeOf(Code_highlighter)).call(this, props));

            _this.displayName = 'Code_highlighter';
            return _this;
        }

        _createClass(Code_highlighter, [{
            key: 'render',
            value: function render() {
                var html = this.props.keepIndentation ? this.props.children : normaliseIndentation(this.props.children),
                    classes = ["code_highlighter"],
                    Wrapper = _styledComponents2.default.div(_templateObject, _styleVars2.default.colors.blue10);

                this.props.className && classes.push(this.props.className);

                return _react2.default.createElement(
                    Wrapper,
                    { style: this.props.style, className: classes.join(" ") },
                    _react2.default.createElement(
                        _reactSyntaxHighlighter2.default,
                        { language: 'xml', useInlineStyles: false },
                        html
                    )
                );
            }
        }]);

        return Code_highlighter;
    }(_react2.default.Component);

    function normaliseIndentation(html) {
        var leadingSpaces = html.split("<")[0].replace("\n", "");

        return html.split(leadingSpaces + "<").join("<");
    }
    exports.default = Code_highlighter;
});