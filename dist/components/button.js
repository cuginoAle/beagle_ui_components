(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'styled-components', './js/styleVars.js', './caption.js'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('styled-components'), require('./js/styleVars.js'), require('./caption.js'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.styledComponents, global.styleVars, global.caption);
        global.button = mod.exports;
    }
})(this, function (exports, _react, _styledComponents, _styleVars, _caption) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _styleVars2 = _interopRequireDefault(_styleVars);

    var _caption2 = _interopRequireDefault(_caption);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n\n    \n    align-items    : center;\n    border         : none;\n    border-radius  : 3px;\n    cursor         : pointer;\n    font-size      : 14px;\n    font-weight    : 400;\n    margin         : 0;\n    padding        : 0;\n    min-width      : 40px;\n    outline        : none;\n    text-align     : center;\n    text-decoration: none;\n    user-select    : none;\n    vertical-align : text-bottom;\n    width          : auto;\n\n    .wrapper{\n        padding        : 0 7px;        \n        display        : flex;\n        height         : 100%;\n        justify-content: center;\n        line-height    : 40px;\n        min-height     : 40px;\n\n    }\n\n    .svg_icon {\n        align-self:center;\n        margin    :0 .5em;\n    }\n\n    .caption {\n        margin:0 .5em;\n    }\n\n    .caption ~ .svg_icon {\n        margin-left:0;\n    }\n\n    .svg_icon ~ .caption {\n        margin-left:0;\n    }\n\n    &.primary {\n        background-color: ', ';         \n        color           : #fff;     \n\n        &:hover {\n            background-color:', ';\n        }       \n    }\n\n    &.secondary {\n        background-color: #fff; \n        border          : 2px solid ', ';\n        color           : ', ';\n\n        font-weight: bold;\n        \n        &:hover {\n            background-color:', ';\n        }             \n    }\n\n    &.warning {\n        background-color: ', ';         \n        color           : #fff;     \n\n        &:hover {\n            opacity:.9;\n        }         \n    }\n\n    &.clear {\n        color:', ';\n        background-color:transparent;\n\n        &:hover {\n            background-color:rgba(170, 196, 220, 0.19);\n        }\n    }\n\n    &:disabled{\n        cursor          : default;\n        color           : ', ';\n        border          : none;\n        background-color: ', ';\n\n        &:hover {\n            background-color: ', ';\n        }\n    }\n\n    &.show_notification {\n\n            position:relative;\n            &:after {\n                align-items     : center;\n                background-color: rgba(255, 0, 0, 0.7);\n                border-radius   : 50%;\n                color           : white;\n                content         : attr(data-notification);\n                display         : flex;\n                font-size       : 12px;\n                font-weight     : 200;\n                height          : 2em;\n                justify-content : center;\n                left            : 50%;\n                padding         : 0px;\n                position        : absolute;\n                top             : 1px;\n                width           : 2em;\n            }            \n    }\n\n\n'], ['\n\n    \n    align-items    : center;\n    border         : none;\n    border-radius  : 3px;\n    cursor         : pointer;\n    font-size      : 14px;\n    font-weight    : 400;\n    margin         : 0;\n    padding        : 0;\n    min-width      : 40px;\n    outline        : none;\n    text-align     : center;\n    text-decoration: none;\n    user-select    : none;\n    vertical-align : text-bottom;\n    width          : auto;\n\n    .wrapper{\n        padding        : 0 7px;        \n        display        : flex;\n        height         : 100%;\n        justify-content: center;\n        line-height    : 40px;\n        min-height     : 40px;\n\n    }\n\n    .svg_icon {\n        align-self:center;\n        margin    :0 .5em;\n    }\n\n    .caption {\n        margin:0 .5em;\n    }\n\n    .caption ~ .svg_icon {\n        margin-left:0;\n    }\n\n    .svg_icon ~ .caption {\n        margin-left:0;\n    }\n\n    &.primary {\n        background-color: ', ';         \n        color           : #fff;     \n\n        &:hover {\n            background-color:', ';\n        }       \n    }\n\n    &.secondary {\n        background-color: #fff; \n        border          : 2px solid ', ';\n        color           : ', ';\n\n        font-weight: bold;\n        \n        &:hover {\n            background-color:', ';\n        }             \n    }\n\n    &.warning {\n        background-color: ', ';         \n        color           : #fff;     \n\n        &:hover {\n            opacity:.9;\n        }         \n    }\n\n    &.clear {\n        color:', ';\n        background-color:transparent;\n\n        &:hover {\n            background-color:rgba(170, 196, 220, 0.19);\n        }\n    }\n\n    &:disabled{\n        cursor          : default;\n        color           : ', ';\n        border          : none;\n        background-color: ', ';\n\n        &:hover {\n            background-color: ', ';\n        }\n    }\n\n    &.show_notification {\n\n            position:relative;\n            &:after {\n                align-items     : center;\n                background-color: rgba(255, 0, 0, 0.7);\n                border-radius   : 50%;\n                color           : white;\n                content         : attr(data-notification);\n                display         : flex;\n                font-size       : 12px;\n                font-weight     : 200;\n                height          : 2em;\n                justify-content : center;\n                left            : 50%;\n                padding         : 0px;\n                position        : absolute;\n                top             : 1px;\n                width           : 2em;\n            }            \n    }\n\n\n']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    var MyButton = _styledComponents2.default.button(_templateObject, _styleVars2.default.colors.blue10, _styleVars2.default.colors.blue5, _styleVars2.default.colors.blue10, _styleVars2.default.colors.blue10, _styleVars2.default.colors.blue15, _styleVars2.default.colors.orange5, _styleVars2.default.colors.blue10, _styleVars2.default.colors.grey8, _styleVars2.default.colors.grey20, _styleVars2.default.colors.grey20);

    function Button(_ref) {
        var id = _ref.id,
            className = _ref.className,
            icon = _ref.icon,
            caption = _ref.caption,
            _ref$onClick = _ref.onClick,
            onClick = _ref$onClick === undefined ? function () {
            console.log("Button - onClick");
        } : _ref$onClick,
            _ref$disabled = _ref.disabled,
            disabled = _ref$disabled === undefined ? false : _ref$disabled,
            _ref$type = _ref.type,
            type = _ref$type === undefined ? "primary" : _ref$type,
            _ref$style = _ref.style,
            style = _ref$style === undefined ? {} : _ref$style,
            children = _ref.children,
            dataNotification = _ref.dataNotification;


        var classes = [type];

        className && classes.push(className);
        caption && classes.push("hasCaption");
        dataNotification > 0 && classes.push("show_notification");

        return _react2.default.createElement(
            MyButton,
            { id: id, disabled: disabled, 'data-notification': dataNotification, className: classes.join(" "), onClick: onClick, style: style },
            _react2.default.createElement(
                'div',
                { className: 'wrapper' },
                caption == undefined ? children : _react2.default.createElement(
                    _caption2.default,
                    null,
                    caption
                )
            )
        );
    }
    exports.default = Button;
});