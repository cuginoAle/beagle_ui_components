(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', './svg_icon.js', './js/animate.js', 'styled-components', './js/styleVars.js'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('./svg_icon.js'), require('./js/animate.js'), require('styled-components'), require('./js/styleVars.js'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.svg_icon, global.animate, global.styledComponents, global.styleVars);
        global.accordion = mod.exports;
    }
})(this, function (exports, _react, _svg_icon, _animate, _styledComponents, _styleVars) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _svg_icon2 = _interopRequireDefault(_svg_icon);

    var _animate2 = _interopRequireDefault(_animate);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _styleVars2 = _interopRequireDefault(_styleVars);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n    border-bottom:1px solid ', ';\n    border-top   :1px solid ', ';\n\n\n    .crx_accordion_content {\n        >*{            \n            display:none;\n            box-sizing:border-box;\n            margin:14px 0;\n        }\n    }\n    .crx_accordion_switch_btn {\n        align-items     : center;\n        background-color: transparent;\n        border          : none;\n        color           : ', ';\n        display         : flex;\n        font-size       : 16px;\n        font-weight     : 400;\n        justify-content : space-between;\n        line-height     : normal;\n        margin          : 0;\n        outline         : none;\n        padding         : 8px 12px;\n        width           : 100%;\n\n        &:active{\n\n        }\n\n        .svg_icon {\n            align-self      :center;\n            font-size       :14px;\n            transform-origin:50%;\n            transition      :transform .6s;\n        }\n        \n    }\n\n    &.expanded {\n        .crx_accordion_switch_btn {\n            .svg_icon {\n                transform:rotateZ(180deg);\n            }            \n        }\n    }\n'], ['\n    border-bottom:1px solid ', ';\n    border-top   :1px solid ', ';\n\n\n    .crx_accordion_content {\n        >*{            \n            display:none;\n            box-sizing:border-box;\n            margin:14px 0;\n        }\n    }\n    .crx_accordion_switch_btn {\n        align-items     : center;\n        background-color: transparent;\n        border          : none;\n        color           : ', ';\n        display         : flex;\n        font-size       : 16px;\n        font-weight     : 400;\n        justify-content : space-between;\n        line-height     : normal;\n        margin          : 0;\n        outline         : none;\n        padding         : 8px 12px;\n        width           : 100%;\n\n        &:active{\n\n        }\n\n        .svg_icon {\n            align-self      :center;\n            font-size       :14px;\n            transform-origin:50%;\n            transition      :transform .6s;\n        }\n        \n    }\n\n    &.expanded {\n        .crx_accordion_switch_btn {\n            .svg_icon {\n                transform:rotateZ(180deg);\n            }            \n        }\n    }\n']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var Accordion = function (_React$Component) {
        _inherits(Accordion, _React$Component);

        function Accordion(props) {
            _classCallCheck(this, Accordion);

            var _this = _possibleConstructorReturn(this, (Accordion.__proto__ || Object.getPrototypeOf(Accordion)).call(this, props));

            _this.displayName = 'Accordion';

            _this.state = {
                showContent: props.showContent
            };

            _this.handleClick = _this.handleClick.bind(_this);
            return _this;
        }

        _createClass(Accordion, [{
            key: 'componentDidMount',
            value: function componentDidMount() {
                this.checkContentHeight();
                this.contentElem.style.overflow = "hidden";
            }
        }, {
            key: 'componentDidUpdate',
            value: function componentDidUpdate(prevProps, prevState) {
                this.checkContentHeight();
            }
        }, {
            key: 'checkContentHeight',
            value: function checkContentHeight() {
                var elem = this.contentElem,
                    oldHeight = elem.offsetHeight;

                if (oldHeight == 0) {
                    elem.firstChild.style.display = "block";
                }

                var contentHeight = elem.scrollHeight,
                    newHeight = this.state.showContent ? contentHeight : 0;

                (0, _animate2.default)({
                    duration: 800,
                    easing: "Exponential",
                    fade: "Out",
                    callback: function callback(progress) {
                        var sanitisedHeight = oldHeight - (oldHeight - newHeight) * progress;
                        var y = newHeight == 0 ? progress : 1 - progress;

                        elem.firstChild.style.transform = "translateY(-" + y * 100 + "%)";

                        elem.style.height = Math.max(0, sanitisedHeight) + "px";
                    },
                    onEnd: function onEnd() {
                        if (newHeight == 0) {
                            elem.firstChild.style.display = "none";
                        } else {
                            elem.style.height = "auto";
                        }
                    }
                });
            }
        }, {
            key: 'handleClick',
            value: function handleClick() {
                var show = !this.state.showContent;

                this.setState({
                    showContent: show
                });
            }
        }, {
            key: 'render',
            value: function render() {
                var _this2 = this;

                var classes = ['crx_accordion'];
                this.props.className && classes.push(this.props.className);
                this.state.showContent && classes.push("expanded");

                return _react2.default.createElement(
                    S_Accordion,
                    { className: classes.join(" ") },
                    _react2.default.createElement(
                        'button',
                        { className: 'crx_accordion_switch_btn', onClick: this.handleClick },
                        _react2.default.createElement(
                            'span',
                            { className: 'switch_btn_title' },
                            this.props.caption
                        ),
                        _react2.default.createElement(_svg_icon2.default, { icon: 'down' })
                    ),
                    _react2.default.createElement(
                        'div',
                        { ref: function ref(el) {
                                _this2.contentElem = el;
                            }, className: 'crx_accordion_content' },
                        this.props.children
                    )
                );
            }
        }]);

        return Accordion;
    }(_react2.default.Component);

    exports.default = Accordion;


    Accordion.defaultProps = {
        showContent: false
    };

    var S_Accordion = _styledComponents2.default.div(_templateObject, _styleVars2.default.colors.grey10, _styleVars2.default.colors.grey10, _styleVars2.default.colors.blue10);
});