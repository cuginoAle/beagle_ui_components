(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(["exports", "react", "styled-components", "./dataList"], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require("react"), require("styled-components"), require("./dataList"));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.styledComponents, global.dataList);
        global.input = mod.exports;
    }
})(this, function (exports, _react, _styledComponents, _dataList) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _dataList2 = _interopRequireDefault(_dataList);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _extends = Object.assign || function (target) {
        for (var i = 1; i < arguments.length; i++) {
            var source = arguments[i];

            for (var key in source) {
                if (Object.prototype.hasOwnProperty.call(source, key)) {
                    target[key] = source[key];
                }
            }
        }

        return target;
    };

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var _templateObject = _taggedTemplateLiteral(["\n    display:inline-block;\n\n    input {\n        box-sizing : border-box;        \n        display    : block;\n        font-weight:200;\n        height     : 100%;\n        width      : 100%;\n    }\n\n    ul {\n        max-height: 35vh;\n    }\n"], ["\n    display:inline-block;\n\n    input {\n        box-sizing : border-box;        \n        display    : block;\n        font-weight:200;\n        height     : 100%;\n        width      : 100%;\n    }\n\n    ul {\n        max-height: 35vh;\n    }\n"]);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    var Wrapper = _styledComponents2.default.div(_templateObject);

    var Input = function (_React$Component) {
        _inherits(Input, _React$Component);

        function Input(props) {
            _classCallCheck(this, Input);

            var _this = _possibleConstructorReturn(this, (Input.__proto__ || Object.getPrototypeOf(Input)).call(this, props));

            _this.displayName = 'Input';

            _this.onSuggestionSelected = _this.onSuggestionSelected.bind(_this);
            _this.blur = _this.blur.bind(_this);
            _this.focus = _this.focus.bind(_this);
            _this.keyDown = _this.keyDown.bind(_this);

            _this.highlightSuggestion = _this.highlightSuggestion.bind(_this);

            _this.state = {
                showSuggestions: false,
                suggestions: props.datalist.list,
                suggestionIndex: -1
            };
            return _this;
        }

        _createClass(Input, [{
            key: "componentWillReceiveProps",
            value: function componentWillReceiveProps(nextProps) {
                this.setState({
                    suggestions: nextProps.datalist.list,
                    suggestionIndex: -1
                });
            }
        }, {
            key: "onSuggestionSelected",
            value: function onSuggestionSelected(suggestion) {
                this.input.value = suggestion;

                if (this.props.datalist.onSuggestionSelected) {
                    this.props.datalist.onSuggestionSelected(suggestion);
                } else {
                    console.warn("Input: onSuggestionSelected function not defined!");
                }
            }
        }, {
            key: "blur",
            value: function blur(e) {
                var _this2 = this;

                //allowing the user to click/tap on the suggestion before removing it
                setTimeout(function () {
                    _this2.setState({ showSuggestions: false });
                }, 20);
            }
        }, {
            key: "focus",
            value: function focus(e) {
                this.setState({ showSuggestions: true });
            }
        }, {
            key: "keyDown",
            value: function keyDown(e) {

                var enter = 13,
                    down = 40,
                    up = 38;

                switch (e.keyCode) {
                    case down:
                        this.highlightSuggestion(1);
                        e.preventDefault();
                        break;

                    case up:
                        this.highlightSuggestion(-1);
                        e.preventDefault();
                        break;

                    case enter:
                        var suggestion = this.state.suggestions[this.state.suggestionIndex];
                        if (suggestion) {
                            this.onSuggestionSelected(suggestion);
                            e.preventDefault();
                        }
                        break;
                }
            }
        }, {
            key: "highlightSuggestion",
            value: function highlightSuggestion(indexIncr) {
                if (this.state.showSuggestions && this.state.suggestions.length > 0) {
                    var index = this.state.suggestionIndex;

                    index += indexIncr;

                    //checking that the index is within the boundaries
                    index = Math.max(0, index);
                    index = Math.min(this.state.suggestions.length - 1, index);

                    this.setState({
                        suggestionIndex: index
                    });
                }
            }
        }, {
            key: "render",
            value: function render() {
                var _this3 = this;

                return _react2.default.createElement(
                    Wrapper,
                    { className: "input" },
                    _react2.default.createElement("input", _extends({ ref: function ref(el) {
                            _this3.input = el;
                        } }, this.props.input, { onBlur: this.blur, onFocus: this.focus, onKeyDown: this.keyDown })),
                    this.state.showSuggestions && _react2.default.createElement(_dataList2.default, { highlightedIndex: this.state.suggestionIndex, onClick: this.onSuggestionSelected, list: this.state.suggestions })
                );
            }
        }]);

        return Input;
    }(_react2.default.Component);

    exports.default = Input;
});