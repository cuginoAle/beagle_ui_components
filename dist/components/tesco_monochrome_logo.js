(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'styled-components', 'react', './svg_icon'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('styled-components'), require('react'), require('./svg_icon'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.styledComponents, global.react, global.svg_icon);
        global.tesco_monochrome_logo = mod.exports;
    }
})(this, function (exports, _styledComponents, _react, _svg_icon) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _react2 = _interopRequireDefault(_react);

    var _svg_icon2 = _interopRequireDefault(_svg_icon);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n    display:inline-flex;\n    height:.364em;\n\n    svg,\n    .svg_icon{\n        height:100%;\n    }\n'], ['\n    display:inline-flex;\n    height:.364em;\n\n    svg,\n    .svg_icon{\n        height:100%;\n    }\n']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    var Wrapper = _styledComponents2.default.a(_templateObject);

    function Tesco_monochrome_logo(props) {
        var homeLink = props.href || "/",
            classes = ["tesco_monochrome_logo"];

        props.className && classes.push(props.className);

        return _react2.default.createElement(
            Wrapper,
            { className: classes.join(" "), href: homeLink },
            _react2.default.createElement(_svg_icon2.default, { icon: 'monochrome_logo' })
        );
    }

    exports.default = Tesco_monochrome_logo;
});