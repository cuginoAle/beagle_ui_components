(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'styled-components', 'js/styleVars.js', 'components/card.js'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('styled-components'), require('js/styleVars.js'), require('components/card.js'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.styledComponents, global.styleVars, global.card);
        global.styled_card = mod.exports;
    }
})(this, function (exports, _styledComponents, _styleVars, _card) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _styleVars2 = _interopRequireDefault(_styleVars);

    var _card2 = _interopRequireDefault(_card);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n    border:1px solid ', ';\n    background-color:', ';\n    border-radius: 5px;\n    padding:14px;\n    position: relative;            \n    \n\n    &.removable {\n        padding-right:70px;\n    }\n    .hideBtn{\n        font-size: 16px;\n        position: absolute;\n        top:0px;\n        right:0px;\n        width:auto;\n        margin:0;\n        padding:0 .5em;\n        background-color:transparent;\n        display: flex;\n        align-items: center;\n        color: ', ';\n\n        .svg_icon {\n            font-size: 12px;\n            margin-left:.5em;\n        }\n    } \n\n'], ['\n    border:1px solid ', ';\n    background-color:', ';\n    border-radius: 5px;\n    padding:14px;\n    position: relative;            \n    \n\n    &.removable {\n        padding-right:70px;\n    }\n    .hideBtn{\n        font-size: 16px;\n        position: absolute;\n        top:0px;\n        right:0px;\n        width:auto;\n        margin:0;\n        padding:0 .5em;\n        background-color:transparent;\n        display: flex;\n        align-items: center;\n        color: ', ';\n\n        .svg_icon {\n            font-size: 12px;\n            margin-left:.5em;\n        }\n    } \n\n']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    var Styled_card = (0, _styledComponents2.default)(_card2.default)(_templateObject, _styleVars2.default.colors.green15, _styleVars2.default.colors.green20, _styleVars2.default.colors.blue10);

    exports.default = Styled_card;
});