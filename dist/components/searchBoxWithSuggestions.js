(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', './searchBox', './data', './js/utility'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('./searchBox'), require('./data'), require('./js/utility'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.searchBox, global.data, global.utility);
        global.searchBoxWithSuggestions = mod.exports;
    }
})(this, function (exports, _react, _searchBox, _data, _utility) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _searchBox2 = _interopRequireDefault(_searchBox);

    var _data2 = _interopRequireDefault(_data);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _extends = Object.assign || function (target) {
        for (var i = 1; i < arguments.length; i++) {
            var source = arguments[i];

            for (var key in source) {
                if (Object.prototype.hasOwnProperty.call(source, key)) {
                    target[key] = source[key];
                }
            }
        }

        return target;
    };

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var SearchBoxWithSuggestions = function (_React$Component) {
        _inherits(SearchBoxWithSuggestions, _React$Component);

        function SearchBoxWithSuggestions(props) {
            _classCallCheck(this, SearchBoxWithSuggestions);

            var _this = _possibleConstructorReturn(this, (SearchBoxWithSuggestions.__proto__ || Object.getPrototypeOf(SearchBoxWithSuggestions)).call(this, props));

            _this.displayName = 'SearchBoxWithSuggestions';

            _this.state = {
                suggestionList: []
            };

            _this.getSuggestions = _this.getSuggestions.bind(_this);
            _this.debGetSuggestions = (0, _utility.deb)(_this.getSuggestions, 100);
            return _this;
        }

        _createClass(SearchBoxWithSuggestions, [{
            key: 'getSuggestions',
            value: function getSuggestions(userInput) {
                var _this2 = this;

                var maxCount = 8;

                userInput = userInput.toLowerCase();

                _data2.default.TopKeywords.popularSearches([userInput, maxCount], function (keywords) {

                    var results = keywords.sort(function (a, b) {
                        return a.position - b.position;
                    }).filter(function (item) {
                        return item.label.indexOf(userInput) > -1;
                    }).slice(0, maxCount).map(function (keyword) {
                        return keyword.label;
                    });

                    _this2.setState({
                        suggestionList: results
                    });
                });
            }
        }, {
            key: 'render',
            value: function render() {
                var _this3 = this;

                var self = this;
                var formOptions = {
                    form: _extends({}, self.props, {
                        autoSubmit: true,
                        onSubmit: function onSubmit(e) {

                            self.setState({
                                suggestionList: []
                            });

                            self.props.form && self.props.form.onSubmit && self.props.form.onSubmit(e);
                        }
                    }),

                    datalist: {
                        list: this.state.suggestionList
                    },
                    input: _extends({}, self.props.input, {
                        autoComplete: "off",
                        placeholder: self.props.input ? self.props.input.placeholder : "",
                        onInput: function onInput(e) {
                            var value = e.target.value;

                            if (value.length > 1) {

                                _this3.debGetSuggestions(value);
                            } else {
                                _this3.setState({
                                    suggestionList: []
                                });
                            }
                        }
                    })
                };

                return _react2.default.createElement(
                    'div',
                    { className: 'search_box_with_suggestions' },
                    _react2.default.createElement(_searchBox2.default, formOptions)
                );
            }
        }]);

        return SearchBoxWithSuggestions;
    }(_react2.default.Component);

    exports.default = SearchBoxWithSuggestions;
});