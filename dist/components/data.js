(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(["exports", "../data_sources/products", "../data_sources/top_keywords"], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require("../data_sources/products"), require("../data_sources/top_keywords"));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.products, global.top_keywords);
        global.data = mod.exports;
    }
})(this, function (exports, _products, _top_keywords) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _products2 = _interopRequireDefault(_products);

    var _top_keywords2 = _interopRequireDefault(_top_keywords);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    exports.default = {
        Products: _products2.default,
        TopKeywords: _top_keywords2.default
    };
});