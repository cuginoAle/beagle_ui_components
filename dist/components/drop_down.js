(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'styled-components', 'js/styleVars.js', 'js/utility.js'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('styled-components'), require('js/styleVars.js'), require('js/utility.js'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.styledComponents, global.styleVars, global.utility);
        global.drop_down = mod.exports;
    }
})(this, function (exports, _react, _styledComponents, _styleVars, _utility) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _styleVars2 = _interopRequireDefault(_styleVars);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n        \n        span {\n            font-weight: bold;\n            font-size: 14px;\n            display: block;\n            line-height: 1;\n            margin-bottom:5px;\n        }\n\n        select {\n            font-size: 14px;\n            margin:0;\n        }\n    '], ['\n        \n        span {\n            font-weight: bold;\n            font-size: 14px;\n            display: block;\n            line-height: 1;\n            margin-bottom:5px;\n        }\n\n        select {\n            font-size: 14px;\n            margin:0;\n        }\n    ']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    function Drop_down(_ref) {
        var _ref$options = _ref.options,
            options = _ref$options === undefined ? [] : _ref$options,
            _ref$onChange = _ref.onChange,
            onChange = _ref$onChange === undefined ? function () {} : _ref$onChange,
            caption = _ref.caption,
            name = _ref.name,
            value = _ref.value;

        var Wrapper = _styledComponents2.default.label(_templateObject);

        return _react2.default.createElement(
            Wrapper,
            null,
            caption ? _react2.default.createElement(
                'span',
                null,
                caption
            ) : "",
            _react2.default.createElement(
                'select',
                { name: '{name}', onChange: onChange, value: value },
                options.map(function (opt) {
                    return _react2.default.createElement(
                        'option',
                        { key: (0, _utility.getKey)(), value: opt.value },
                        opt.label
                    );
                })
            )
        );
    }

    exports.default = Drop_down;
});