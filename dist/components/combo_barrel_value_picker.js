(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', './_snappable_scroll.js', 'styled-components', './js/styleVars.js', './js/utility.js'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('./_snappable_scroll.js'), require('styled-components'), require('./js/styleVars.js'), require('./js/utility.js'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global._snappable_scroll, global.styledComponents, global.styleVars, global.utility);
        global.combo_barrel_value_picker = mod.exports;
    }
})(this, function (exports, _react, _snappable_scroll2, _styledComponents, _styleVars, _utility) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _snappable_scroll3 = _interopRequireDefault(_snappable_scroll2);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _styleVars2 = _interopRequireDefault(_styleVars);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n                \n                background-color:#fff;\n                border          :1px solid rgba(0,0,0,.3);\n                box-sizing      :border-box;\n                width           :100%;\n\n                .barrels_wrapper {\n                    display        :flex;\n                    justify-content:space-around;\n                }\n\n                ._snappable_scroll{\n                    flex-basis:', '%;\n                    position  :relative;\n                    overflow  :visible;\n                    overflow-y:hidden;\n\n                    &:before{\n                        background    : linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(255,255,255,0.5) 100%);\n                        border-bottom :1px solid rgba(0,0,0,.3);\n                        content       :"";\n                        display       : block;\n                        height        :calc(50% - 18px);\n                        left          : 0;\n                        pointer-events: none;\n                        position      :absolute;\n                        right         :0;\n                        top           :0;\n                        z-index       : 2;\n                    }\n\n                    &:after{\n                        background    : linear-gradient(to bottom, rgba(255,255,255,0.5) 0%, rgba(255,255,255,1) 100%);\n                        border-top    :1px solid rgba(0,0,0,.3);\n                        bottom        :0;\n                        content       :"";\n                        display       : block;\n                        height        :calc(50% - 18px);\n                        left          : 0;\n                        pointer-events: none;\n                        position      :absolute;\n                        right         :0;\n                        z-index       : 2;\n                    }\n                    \n                }\n\n                .draggable_content {\n\n                    p{\n                        font-size  : 20px;\n                        padding    : 8px 14px;\n                        text-align : center;\n                        white-space: nowrap;\n                        line-height: 20px;\n                    }        \n                }\n\n\n                ._barrel_value_picker_header {\n                    align-items     :center;\n                    background-color:#eee;\n                    border-bottom   :1px solid rgba(0,0,0,.1);\n                    display         :flex;\n                    justify-content :space-between;\n\n                    p{\n                        flex-grow  :1;\n                        font-size  :14px;\n                        text-align :center;\n                        white-space:nowrap;\n                    }\n\n                    button {\n                        background-color:transparent;\n                        border          :none;\n                        font-size       :14px;\n                        margin          :0;\n                        padding         :0 1em;\n                        width           :auto;\n                        color:', ';\n                    }\n\n                    .doneBtn {\n                        font-weight:bold;\n                    }\n                }\n            '], ['\n                \n                background-color:#fff;\n                border          :1px solid rgba(0,0,0,.3);\n                box-sizing      :border-box;\n                width           :100%;\n\n                .barrels_wrapper {\n                    display        :flex;\n                    justify-content:space-around;\n                }\n\n                ._snappable_scroll{\n                    flex-basis:', '%;\n                    position  :relative;\n                    overflow  :visible;\n                    overflow-y:hidden;\n\n                    &:before{\n                        background    : linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(255,255,255,0.5) 100%);\n                        border-bottom :1px solid rgba(0,0,0,.3);\n                        content       :"";\n                        display       : block;\n                        height        :calc(50% - 18px);\n                        left          : 0;\n                        pointer-events: none;\n                        position      :absolute;\n                        right         :0;\n                        top           :0;\n                        z-index       : 2;\n                    }\n\n                    &:after{\n                        background    : linear-gradient(to bottom, rgba(255,255,255,0.5) 0%, rgba(255,255,255,1) 100%);\n                        border-top    :1px solid rgba(0,0,0,.3);\n                        bottom        :0;\n                        content       :"";\n                        display       : block;\n                        height        :calc(50% - 18px);\n                        left          : 0;\n                        pointer-events: none;\n                        position      :absolute;\n                        right         :0;\n                        z-index       : 2;\n                    }\n                    \n                }\n\n                .draggable_content {\n\n                    p{\n                        font-size  : 20px;\n                        padding    : 8px 14px;\n                        text-align : center;\n                        white-space: nowrap;\n                        line-height: 20px;\n                    }        \n                }\n\n\n                ._barrel_value_picker_header {\n                    align-items     :center;\n                    background-color:#eee;\n                    border-bottom   :1px solid rgba(0,0,0,.1);\n                    display         :flex;\n                    justify-content :space-between;\n\n                    p{\n                        flex-grow  :1;\n                        font-size  :14px;\n                        text-align :center;\n                        white-space:nowrap;\n                    }\n\n                    button {\n                        background-color:transparent;\n                        border          :none;\n                        font-size       :14px;\n                        margin          :0;\n                        padding         :0 1em;\n                        width           :auto;\n                        color:', ';\n                    }\n\n                    .doneBtn {\n                        font-weight:bold;\n                    }\n                }\n            ']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var Combo_barrel_value_picker = function (_React$Component) {
        _inherits(Combo_barrel_value_picker, _React$Component);

        function Combo_barrel_value_picker(props) {
            _classCallCheck(this, Combo_barrel_value_picker);

            var _this = _possibleConstructorReturn(this, (Combo_barrel_value_picker.__proto__ || Object.getPrototypeOf(Combo_barrel_value_picker)).call(this, props));

            _this.displayName = 'Combo_barrel_value_picker';

            _this.state = {
                padderStyle: {
                    height: _this.props.stepHeight
                }
            };
            return _this;
        }

        _createClass(Combo_barrel_value_picker, [{
            key: 'componentDidMount',
            value: function componentDidMount() {
                var _this2 = this;

                window.requestAnimationFrame(function () {
                    //this is needed only to wait for the browser to have rendered the html

                    _this2.setState({
                        padderStyle: {
                            height: _this2.snappableComponent.offsetHeight / 2 - _this2.props.stepHeight / 2
                        }
                    });
                });
            }
        }, {
            key: 'renderOptions',
            value: function renderOptions(arr) {
                var _this3 = this;

                return arr.map(function (optionSet) {

                    return _react2.default.createElement(
                        _snappable_scroll3.default,
                        { key: (0, _utility.getKey)(), initialPosition: optionSet.initialPosition, selectedIndex: true, onChange: optionSet.onChange, style: { minHeight: _this3.props.stepHeight * 3, height: _this3.props.stepHeight * _this3.props.height }, stepHeight: _this3.props.stepHeight, animation: { curve: [.3, 0, .5, 1.6], duration: 200 } },
                        _react2.default.createElement('div', { style: _this3.state.padderStyle }),
                        optionSet.options.map(function (opt) {
                            return _react2.default.createElement(
                                'p',
                                { key: opt.key },
                                opt.label
                            );
                        }),
                        _react2.default.createElement('div', { style: _this3.state.padderStyle })
                    );
                });
            }
        }, {
            key: 'render',
            value: function render() {
                var _this4 = this;

                var classes = ["combo_barrel_value_picker"];

                var StyledWrapper = _styledComponents2.default.div(_templateObject, (100 / this.props.options.length).toFixed(3), _styleVars2.default.colors.blue10);

                this.props.className && classes.push(this.props.className);

                var header = this.props.showHeader ? _react2.default.createElement(
                    'div',
                    { className: '_barrel_value_picker_header' },
                    _react2.default.createElement(
                        'button',
                        { className: 'button button-secondary cancelBtn', onClick: this.props.onCancel },
                        'Cancel'
                    ),
                    _react2.default.createElement(
                        'p',
                        { className: '_barrel_value_picker_title' },
                        this.props.title
                    ),
                    _react2.default.createElement(
                        'button',
                        { className: 'button button-secondary doneBtn', onClick: this.props.onDone },
                        'Done'
                    )
                ) : null;

                return _react2.default.createElement(
                    StyledWrapper,
                    { className: classes.join(" ") },
                    header,
                    _react2.default.createElement(
                        'div',
                        { className: 'barrels_wrapper', ref: function ref(elem) {
                                _this4.snappableComponent = elem;
                            } },
                        this.renderOptions(this.props.options)
                    )
                );
            }
        }]);

        return Combo_barrel_value_picker;
    }(_react2.default.Component);

    var selectionObj = {};

    Combo_barrel_value_picker.defaultProps = {
        title: "Choose an option",
        showHeader: true,
        stepHeight: 36,
        height: 5,
        options: [{
            onChange: function onChange(value) {
                console.log(value
                // selectionObj.qty=this.options[value].value
                );
            },
            options: function () {
                var arr = [];
                for (var x = 1; x < 21; x++) {
                    arr.push({ key: "dummy_" + x, label: "value " + x, value: x });
                }

                return arr;
            }()
        }]
    };

    exports.default = Combo_barrel_value_picker;
});