(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'react-dom', './svg_icon', 'styled-components'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('react-dom'), require('./svg_icon'), require('styled-components'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.reactDom, global.svg_icon, global.styledComponents);
        global.message_box = mod.exports;
    }
})(this, function (exports, _react, _reactDom, _svg_icon, _styledComponents) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _svg_icon2 = _interopRequireDefault(_svg_icon);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n            background-color:rgba(0, 83, 159, .95);\n            bottom          :0;\n            padding         :30px;\n            position        :fixed;\n            transform       :translateY(100%);\n            transition      :transform .4s;\n            width           :100%;\n            box-sizing      :border-box;\n            z-index         :999;\n\n            .svg_icon.close {\n                color      :white;\n                font-size  :12px;\n                position   :absolute;\n                right      :10px;\n                top        :10px;\n            }\n\n            .show &{\n                transform:translateY(0);\n            }\n\n\n        '], ['\n            background-color:rgba(0, 83, 159, .95);\n            bottom          :0;\n            padding         :30px;\n            position        :fixed;\n            transform       :translateY(100%);\n            transition      :transform .4s;\n            width           :100%;\n            box-sizing      :border-box;\n            z-index         :999;\n\n            .svg_icon.close {\n                color      :white;\n                font-size  :12px;\n                position   :absolute;\n                right      :10px;\n                top        :10px;\n            }\n\n            .show &{\n                transform:translateY(0);\n            }\n\n\n        ']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    function Message_box() {

        var Wrapper = _styledComponents2.default.div(_templateObject);

        var div = document.createElement("div"),
            onClose = [],
            isClosing = false,
            timeoutHnd = null;

        div.addEventListener("click", function (e) {
            div.classList.remove("show");
        });

        div.addEventListener("webkitTransitionEnd", function () {
            isClosing = false;

            if (!div.classList.contains("show")) {
                document.documentElement.removeChild(div);
                callOnClose();
            }
        });

        var _showMessage = function _showMessage(children, timeout, closeCb) {
            clearTimeout(timeoutHnd);

            if (closeCb) {
                setTimeout(function () {
                    onClose.push(closeCb);
                }, 0);
            }

            (0, _reactDom.render)(_react2.default.createElement(
                Wrapper,
                { className: 'message_box' },
                _react2.default.createElement(_svg_icon2.default, { icon: 'close' }),
                children
            ), div);

            document.documentElement.appendChild(div);
            setTimeout(function () {
                div.classList.add("show");
                if (timeout) {
                    timeoutHnd = setTimeout(msgObj.close, timeout);
                }
            }, 20);
        };

        var callOnClose = function callOnClose() {
            var cb = onClose.shift();
            while (cb != undefined) {
                cb();
                cb = onClose.shift();
            }
        };

        var msgObj = {
            show: function show(children, timeout, closeCb) {
                var alreadyDisplayed = div.classList.contains("show");

                if (isClosing) {
                    callOnClose();
                }

                if (alreadyDisplayed) {

                    onClose.push(function () {
                        console.log("showing earlier!");
                        _showMessage(children, timeout, closeCb);
                    });
                    msgObj.close();
                } else {
                    _showMessage(children, timeout, closeCb);
                }
            },
            close: function close() {
                isClosing = true;
                div.classList.remove("show");
                clearTimeout(timeoutHnd);
            }
        };

        return msgObj;
    }

    exports.default = Message_box();
});