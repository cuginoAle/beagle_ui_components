(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', './_snappable_scroll.js', './js/utility'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('./_snappable_scroll.js'), require('./js/utility'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global._snappable_scroll, global.utility);
        global._barrel_value_picker = mod.exports;
    }
})(this, function (exports, _react, _snappable_scroll2, _utility) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _snappable_scroll3 = _interopRequireDefault(_snappable_scroll2);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var _barrel_value_picker = function (_React$Component) {
        _inherits(_barrel_value_picker, _React$Component);

        function _barrel_value_picker(props) {
            _classCallCheck(this, _barrel_value_picker);

            var _this = _possibleConstructorReturn(this, (_barrel_value_picker.__proto__ || Object.getPrototypeOf(_barrel_value_picker)).call(this, props));

            _this.displayName = '_barrel_value_picker';

            _this.state = {
                padderStyle: {
                    height: _this.props.stepHeight
                }
            };
            return _this;
        }

        _createClass(_barrel_value_picker, [{
            key: 'componentDidMount',
            value: function componentDidMount() {
                var _this2 = this;

                window.requestAnimationFrame(function () {
                    //this is needed only to wait for the browser to have rendered the html

                    _this2.setState({
                        padderStyle: {
                            height: _this2.snappableComponent.wrapper.offsetHeight / 2 - _this2.props.stepHeight / 2
                        }
                    });
                });
            }
        }, {
            key: 'render',
            value: function render() {
                var _this3 = this;

                var classes = ["_barrel_value_picker"];

                var style = {
                    minHeight: this.props.stepHeight * 3,
                    height: this.props.stepHeight * this.props.height
                };

                this.props.className && classes.push(this.props.className);
                return _react2.default.createElement(
                    'div',
                    { className: classes.join(" ") },
                    _react2.default.createElement(
                        'div',
                        { className: '_barrel_value_picker_header' },
                        _react2.default.createElement(
                            'button',
                            { className: 'cancelBtn', onClick: this.props.onCancel },
                            'Cancel'
                        ),
                        _react2.default.createElement(
                            'p',
                            { className: '_barrel_value_picker_title' },
                            this.props.title
                        ),
                        _react2.default.createElement(
                            'button',
                            { className: 'doneBtn', onClick: this.props.onDone },
                            'Done'
                        )
                    ),
                    _react2.default.createElement(
                        _snappable_scroll3.default,
                        { onChange: this.props.onChange, initialPosition: this.props.initialPosition, ref: function ref(elem) {
                                _this3.snappableComponent = elem;
                            }, style: style, stepHeight: this.props.stepHeight, animation: { curve: [.3, 0, .5, 1.6], duration: 500 } },
                        _react2.default.createElement('div', { style: this.state.padderStyle }),
                        this.props.entries.map(function (entry) {
                            return _react2.default.createElement(
                                'p',
                                { key: (0, _utility.getKey)() },
                                entry
                            );
                        }),
                        _react2.default.createElement('div', { style: this.state.padderStyle })
                    )
                );
            }
        }]);

        return _barrel_value_picker;
    }(_react2.default.Component);

    _barrel_value_picker.defaultProps = {
        title: "Barrel value picker!",
        stepHeight: 36,
        initialPosition: 0,
        height: 3,
        children: [_react2.default.createElement(
            'span',
            null,
            'empty'
        )]
    };

    exports.default = _barrel_value_picker;
});