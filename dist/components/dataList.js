(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'styled-components', './js/utility', './button'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('styled-components'), require('./js/utility'), require('./button'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.styledComponents, global.utility, global.button);
        global.dataList = mod.exports;
    }
})(this, function (exports, _react, _styledComponents, _utility, _button) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _button2 = _interopRequireDefault(_button);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n    position:relative;\n    z-index:2;\n\n    ul{\n        -webkit-overflow-scrolling: touch;\n        backgroundColor           :#fff;\n        box-shadow                : 0 2px 4px -2px rgba(0,0,0,.4);\n        left                      :0;\n        list-style                : none;\n        margin                    : 0;\n        \n        overflow                  :auto;\n        padding                   : 0;\n        position                  :absolute;\n        right                     :0;\n        top                       :0;\n\n        .selected {\n            background-color:rgba(59, 172, 255, 0.1);\n        }\n    }\n\n    a {\n        align-items    :center;\n        color          :#00539F;\n        cursor         :pointer;\n        display        :flex;\n        font-size      :1.7em;\n        font-weight    :400;\n        justify-content:flex-start;     \n        min-height     :40px;\n        padding        :0 .3em;\n        width          :100%;\n    }\n\n'], ['\n    position:relative;\n    z-index:2;\n\n    ul{\n        -webkit-overflow-scrolling: touch;\n        backgroundColor           :#fff;\n        box-shadow                : 0 2px 4px -2px rgba(0,0,0,.4);\n        left                      :0;\n        list-style                : none;\n        margin                    : 0;\n        \n        overflow                  :auto;\n        padding                   : 0;\n        position                  :absolute;\n        right                     :0;\n        top                       :0;\n\n        .selected {\n            background-color:rgba(59, 172, 255, 0.1);\n        }\n    }\n\n    a {\n        align-items    :center;\n        color          :#00539F;\n        cursor         :pointer;\n        display        :flex;\n        font-size      :1.7em;\n        font-weight    :400;\n        justify-content:flex-start;     \n        min-height     :40px;\n        padding        :0 .3em;\n        width          :100%;\n    }\n\n']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    //checking 
    // var nativedatalist = !!('list' in document.createElement('input')) && !!(document.createElement('datalist') && window.HTMLDataListElement);

    var Wrapper = _styledComponents2.default.div(_templateObject);

    exports.default = function (props) {
        function _onClick(e, item) {

            if (props.onClick) {
                props.onClick(item);
            } else {
                console.warn("Datalist: onClick function not defined!");
            }
        }

        return _react2.default.createElement(
            Wrapper,
            null,
            _react2.default.createElement(
                'ul',
                null,
                props.list && props.list.map(function (item, i) {
                    var id = "_" + (0, _utility.getKey)(),
                        isSelected = i == props.highlightedIndex;

                    return _react2.default.createElement(
                        'li',
                        { key: id, className: isSelected && "selected" },
                        _react2.default.createElement(
                            'a',
                            { onClick: function onClick(e) {
                                    _onClick(e, item);
                                } },
                            item
                        )
                    );
                })
            )
        );
    };
});