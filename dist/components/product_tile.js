(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'styled-components', './js/styleVars', './caption', './button', './svg_icon'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('styled-components'), require('./js/styleVars'), require('./caption'), require('./button'), require('./svg_icon'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.styledComponents, global.styleVars, global.caption, global.button, global.svg_icon);
        global.product_tile = mod.exports;
    }
})(this, function (exports, _react, _styledComponents, _styleVars, _caption, _button, _svg_icon) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _styleVars2 = _interopRequireDefault(_styleVars);

    var _caption2 = _interopRequireDefault(_caption);

    var _button2 = _interopRequireDefault(_button);

    var _svg_icon2 = _interopRequireDefault(_svg_icon);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n            \n            background-color:#fff;\n            border          :1px solid rgba(0,0,0,.1);\n\n            display         : flex;\n            flex-direction  : column;\n            justify-content : space-between;\n            min-width       : 140px;\n            padding         : 10px;\n\n            .img_wrapper {\n                img {\n                    display:block;\n                    width:100%;\n                    max-width:120px;\n                    margin:auto;\n                }\n                margin-bottom:10px;\n            }\n\n            .product_name {\n                margin-bottom:20px;\n                flex-grow:1;\n            }\n\n            // .product_price {\n            //     display:flex;\n            //     justify-content:space-between;\n            //     margin-bottom:10px;\n            // }\n\n            .secondary{\n                .caption{\n                    font-weight:bold;\n                }\n                .svg_icon {\n                    font-size:18px;\n                }\n            }\n\n\n        '], ['\n            \n            background-color:#fff;\n            border          :1px solid rgba(0,0,0,.1);\n\n            display         : flex;\n            flex-direction  : column;\n            justify-content : space-between;\n            min-width       : 140px;\n            padding         : 10px;\n\n            .img_wrapper {\n                img {\n                    display:block;\n                    width:100%;\n                    max-width:120px;\n                    margin:auto;\n                }\n                margin-bottom:10px;\n            }\n\n            .product_name {\n                margin-bottom:20px;\n                flex-grow:1;\n            }\n\n            // .product_price {\n            //     display:flex;\n            //     justify-content:space-between;\n            //     margin-bottom:10px;\n            // }\n\n            .secondary{\n                .caption{\n                    font-weight:bold;\n                }\n                .svg_icon {\n                    font-size:18px;\n                }\n            }\n\n\n        ']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    function Product_tile(props) {
        var Wrapper = _styledComponents2.default.div(_templateObject);
        var classes = ["product_tile"];
        props.className && classes.push(props.className);

        function onClick(e) {
            props.onClick(e, props);
        }

        var buttonHtml = props.pinned ? _react2.default.createElement(
            _button2.default,
            { onClick: onClick, type: 'secondary', className: 'pinBtn' },
            _react2.default.createElement(
                _caption2.default,
                null,
                'Unpin'
            )
        ) : _react2.default.createElement(
            _button2.default,
            { onClick: onClick, type: 'secondary', className: 'pinBtn' },
            _react2.default.createElement(
                _caption2.default,
                null,
                'Pin'
            ),
            _react2.default.createElement(_svg_icon2.default, { icon: 'pin' })
        );

        return _react2.default.createElement(
            Wrapper,
            { key: props.ProductId, className: classes.join(" ") },
            _react2.default.createElement(
                'div',
                { className: 'img_wrapper' },
                _react2.default.createElement('img', { src: props.ImagePath.replace("90x90", "225x225") })
            ),
            _react2.default.createElement(
                'p',
                { className: 'product_name' },
                props.Name
            ),
            buttonHtml
        );
    }

    exports.default = Product_tile;
});