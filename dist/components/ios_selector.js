(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'styled-components', './js/styleVars.js', './_barrel_value_picker.js'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('styled-components'), require('./js/styleVars.js'), require('./_barrel_value_picker.js'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.styledComponents, global.styleVars, global._barrel_value_picker);
        global.ios_selector = mod.exports;
    }
})(this, function (exports, _styledComponents, _styleVars, _barrel_value_picker2) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _styleVars2 = _interopRequireDefault(_styleVars);

    var _barrel_value_picker3 = _interopRequireDefault(_barrel_value_picker2);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n        border:1px solid rgba(0,0,0,.3);\n        background-color:#fff;\n\n        ._snappable_scroll{\n            position:relative;\n            overflow-y:hidden;\n            &:before{\n                content:"";\n                position:absolute;\n                left: 0;\n                right:0;\n                top:0;\n                display: block;\n                height:calc(50% - 18px);\n                background: linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(255,255,255,0.5) 100%);\n                border-bottom:1px solid rgba(0,0,0,.3);\n                pointer-events: none;\n                z-index: 2;\n            }\n\n            &:after{\n                content:"";\n                position:absolute;\n                left: 0;\n                right:0;\n                bottom:0;\n                display: block;\n                height:calc(50% - 18px);\n                background: linear-gradient(to bottom, rgba(255,255,255,0.5) 0%, rgba(255,255,255,1) 100%);\n                border-top:1px solid rgba(0,0,0,.3);\n                pointer-events: none;\n                z-index: 2;\n            }\n            \n        }\n\n        .draggable_content {\n\n            p{\n                font-size  : 20px;\n                line-height: 20px;\n                margin     : 0;\n                padding    : 8px 14px;\n                text-align : center;\n                white-space: nowrap;\n            }        \n        }     \n\n\n        ._barrel_value_picker_header {\n            display:flex;\n            align-items:center;\n            justify-content:space-between;\n            background-color:#eee;\n            border-bottom:1px solid rgba(0,0,0,.1);\n\n            p{\n                flex-grow  :1;\n                font-size  :14px;\n                margin     :0;\n                text-align :center;\n                white-space:nowrap;\n            }\n\n            button {\n                background-color:transparent;\n                border          :none;\n                color           :', ';\n                font-size       :14px;\n                height          :40px;\n                line-height     :38px;\n                margin          :0;\n                padding         :0 1em;\n                width           :auto;\n            }\n\n            .doneBtn {\n                font-weight:bold;\n            }\n\n        }\n    '], ['\n        border:1px solid rgba(0,0,0,.3);\n        background-color:#fff;\n\n        ._snappable_scroll{\n            position:relative;\n            overflow-y:hidden;\n            &:before{\n                content:"";\n                position:absolute;\n                left: 0;\n                right:0;\n                top:0;\n                display: block;\n                height:calc(50% - 18px);\n                background: linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(255,255,255,0.5) 100%);\n                border-bottom:1px solid rgba(0,0,0,.3);\n                pointer-events: none;\n                z-index: 2;\n            }\n\n            &:after{\n                content:"";\n                position:absolute;\n                left: 0;\n                right:0;\n                bottom:0;\n                display: block;\n                height:calc(50% - 18px);\n                background: linear-gradient(to bottom, rgba(255,255,255,0.5) 0%, rgba(255,255,255,1) 100%);\n                border-top:1px solid rgba(0,0,0,.3);\n                pointer-events: none;\n                z-index: 2;\n            }\n            \n        }\n\n        .draggable_content {\n\n            p{\n                font-size  : 20px;\n                line-height: 20px;\n                margin     : 0;\n                padding    : 8px 14px;\n                text-align : center;\n                white-space: nowrap;\n            }        \n        }     \n\n\n        ._barrel_value_picker_header {\n            display:flex;\n            align-items:center;\n            justify-content:space-between;\n            background-color:#eee;\n            border-bottom:1px solid rgba(0,0,0,.1);\n\n            p{\n                flex-grow  :1;\n                font-size  :14px;\n                margin     :0;\n                text-align :center;\n                white-space:nowrap;\n            }\n\n            button {\n                background-color:transparent;\n                border          :none;\n                color           :', ';\n                font-size       :14px;\n                height          :40px;\n                line-height     :38px;\n                margin          :0;\n                padding         :0 1em;\n                width           :auto;\n            }\n\n            .doneBtn {\n                font-weight:bold;\n            }\n\n        }\n    ']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    var IOS_selector = (0, _styledComponents2.default)(_barrel_value_picker3.default)(_templateObject, _styleVars2.default.colors.blue10);

    exports.default = IOS_selector;
});