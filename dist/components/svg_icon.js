(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'styled-components'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('styled-components'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.styledComponents);
        global.svg_icon = mod.exports;
    }
})(this, function (exports, _react, _styledComponents) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n    display     : inline-block;\n    svg{\n        display     : block;\n        fill        : currentColor;\n        height      : 1em;\n        stroke      : currentColor;\n        stroke-width: 0;\n        width       : 1em;\n\n    }\n    &.dll_icon {\n        svg{\n            stroke-width: 1.2;\n            fill: none;                        \n        }\n    }\n'], ['\n    display     : inline-block;\n    svg{\n        display     : block;\n        fill        : currentColor;\n        height      : 1em;\n        stroke      : currentColor;\n        stroke-width: 0;\n        width       : 1em;\n\n    }\n    &.dll_icon {\n        svg{\n            stroke-width: 1.2;\n            fill: none;                        \n        }\n    }\n']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    // import {svg} from "./assets/symbol-defs.svg";


    // let tag=document.createElement("head");
    // tag.innerHTML=svg;
    // document.documentElement.insertBefore(tag,document.querySelector("head"))


    var Wrapper = _styledComponents2.default.span(_templateObject);

    function Svg_icon(props) {

        var iconClass = ["svg_icon ", props.icon];
        props.className && iconClass.push(props.className);

        if (props.icon && props.icon.indexOf("ddl_icon") > -1) {
            iconClass.push("dll_icon");
        }

        //TODO: for production this should be: @tesco_ux/ui_components/assets/symbol-defs.svg
        var svgFile = props.svgFile || window.svgFilePath || "/assets/symbol-defs.svg",
            svgIcon = svgFile + "#" + props.icon;

        var html = { __html: '<svg xmlns:xlink="http://www.w3.org/1999/xlink"><use className="icon_element" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + svgIcon + '"></use></svg>' };

        return _react2.default.createElement(Wrapper, { className: iconClass.join(" "), style: props.style, dangerouslySetInnerHTML: html });
    }

    exports.default = Svg_icon;
});