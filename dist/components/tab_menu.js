(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'styled-components', './js/styleVars', './js/utility'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('styled-components'), require('./js/styleVars'), require('./js/utility'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.styledComponents, global.styleVars, global.utility);
        global.tab_menu = mod.exports;
    }
})(this, function (exports, _react, _styledComponents, _styleVars, _utility) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _styleVars2 = _interopRequireDefault(_styleVars);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _templateObject = _taggedTemplateLiteral(['\n                display:flex;\n                font-size:14px;\n                border-bottom: 1px solid rgba(0,0,0,.1);\n                padding: 0 10px;\n\n                .tab_menu__tab {\n                    background-color:rgba(0,0,0,.03);\n                    border          :1px solid transparent;\n                    border-radius   :2px 2px 0 0;\n                    color           :#666;\n                    margin: 0 2px 0 0;\n                    overflow:hidden;\n\n                    button {\n                        border          :none;\n                        background-color:transparent;\n                        curson          :pointer;\n                        padding         : 5px 20px;\n                    }\n\n                    &.selected {\n                        border-color: rgba(0,0,0,.1);\n                        background-color   :#fff;\n                        border-bottom-color:transparent;                        \n                        color              :', ';\n                        font-weight        :bold;\n                        margin-bottom:-1px;\n                    }\n                }\n            '], ['\n                display:flex;\n                font-size:14px;\n                border-bottom: 1px solid rgba(0,0,0,.1);\n                padding: 0 10px;\n\n                .tab_menu__tab {\n                    background-color:rgba(0,0,0,.03);\n                    border          :1px solid transparent;\n                    border-radius   :2px 2px 0 0;\n                    color           :#666;\n                    margin: 0 2px 0 0;\n                    overflow:hidden;\n\n                    button {\n                        border          :none;\n                        background-color:transparent;\n                        curson          :pointer;\n                        padding         : 5px 20px;\n                    }\n\n                    &.selected {\n                        border-color: rgba(0,0,0,.1);\n                        background-color   :#fff;\n                        border-bottom-color:transparent;                        \n                        color              :', ';\n                        font-weight        :bold;\n                        margin-bottom:-1px;\n                    }\n                }\n            ']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    function Tab_menu(props) {
        var classes = ["tab_menu"],
            Wrapper = _styledComponents2.default.div(_templateObject, _styleVars2.default.colors.green17);

        props.className && classes.push(props.className);

        return _react2.default.createElement(
            Wrapper,
            { className: classes.join(" ") },
            props.tabs.map(function (tab) {
                var tabClass = ["tab_menu__tab"];
                tab.selected && tabClass.push("selected");
                tab.className && tabClass.push(tab.className);
                return _react2.default.createElement(
                    'div',
                    { key: (0, _utility.getKey)(), className: tabClass.join(" ") },
                    _react2.default.createElement(
                        'button',
                        { onClick: tab.onClick || function (tab) {
                                console.log(tab);
                            } },
                        tab.caption
                    )
                );
            })
        );
    }

    exports.default = Tab_menu;
});