(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(['exports', 'react', 'styled-components', './js/styleVars', './button', './svg_icon', './js/utility', './input'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('react'), require('styled-components'), require('./js/styleVars'), require('./button'), require('./svg_icon'), require('./js/utility'), require('./input'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.react, global.styledComponents, global.styleVars, global.button, global.svg_icon, global.utility, global.input);
        global.searchBox = mod.exports;
    }
})(this, function (exports, _react, _styledComponents, _styleVars, _button, _svg_icon, _utility, _input) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _react2 = _interopRequireDefault(_react);

    var _styledComponents2 = _interopRequireDefault(_styledComponents);

    var _styleVars2 = _interopRequireDefault(_styleVars);

    var _button2 = _interopRequireDefault(_button);

    var _svg_icon2 = _interopRequireDefault(_svg_icon);

    var _input2 = _interopRequireDefault(_input);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _extends = Object.assign || function (target) {
        for (var i = 1; i < arguments.length; i++) {
            var source = arguments[i];

            for (var key in source) {
                if (Object.prototype.hasOwnProperty.call(source, key)) {
                    target[key] = source[key];
                }
            }
        }

        return target;
    };

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var _templateObject = _taggedTemplateLiteral(['\n\n    display  :flex;\n    font-size:10px;\n    margin   :0;\n    padding  :0;\n\n\n    .input{\n        flex-grow:1;        \n    }\n\n    .sb_input {\n        -webkit-appearance: none;\n\n        border            : 1px solid ', ';\n        border-radius     : 3px 0 0 3px;\n        box-shadow        : 0 0 1px 1px rgba(0,0,0,.1) inset;\n        font-size         : 1.6em;\n        margin            : 0;\n\n        min-height        : 40px;\n        outline           : none;\n        padding           : 0 .3em;            \n    }\n\n    .sb_submit {\n        border-radius: 0 3px 3px 0;\n\n        .svg_icon{\n            font-size: 32px;\n            margin   : 0 .2em;\n        }\n    }\n'], ['\n\n    display  :flex;\n    font-size:10px;\n    margin   :0;\n    padding  :0;\n\n\n    .input{\n        flex-grow:1;        \n    }\n\n    .sb_input {\n        -webkit-appearance: none;\n\n        border            : 1px solid ', ';\n        border-radius     : 3px 0 0 3px;\n        box-shadow        : 0 0 1px 1px rgba(0,0,0,.1) inset;\n        font-size         : 1.6em;\n        margin            : 0;\n\n        min-height        : 40px;\n        outline           : none;\n        padding           : 0 .3em;            \n    }\n\n    .sb_submit {\n        border-radius: 0 3px 3px 0;\n\n        .svg_icon{\n            font-size: 32px;\n            margin   : 0 .2em;\n        }\n    }\n']);

    function _taggedTemplateLiteral(strings, raw) {
        return Object.freeze(Object.defineProperties(strings, {
            raw: {
                value: Object.freeze(raw)
            }
        }));
    }

    var Form = _styledComponents2.default.form(_templateObject, _styleVars2.default.colors.grey10);

    var SearchBox = function (_React$Component) {
        _inherits(SearchBox, _React$Component);

        function SearchBox(props) {
            _classCallCheck(this, SearchBox);

            var _this = _possibleConstructorReturn(this, (SearchBox.__proto__ || Object.getPrototypeOf(SearchBox)).call(this, props));

            _this.displayName = 'SearchBox';

            _this.onSuggestionSelected = _this.onSuggestionSelected.bind(_this);

            return _this;
        }

        _createClass(SearchBox, [{
            key: 'onSuggestionSelected',
            value: function onSuggestionSelected(suggestion) {
                if (this.props.form.autoSubmit) {
                    this.form.dispatchEvent(new Event("submit"));
                }
            }
        }, {
            key: 'render',
            value: function render() {
                var _this2 = this;

                var configObj = {
                    form: _extends({
                        onSubmit: function onSubmit(e) {
                            console.warn("OnSubmit callback not defined!");
                            e.preventDefault();
                        },
                        autoSubmit: false
                    }, this.props.form),
                    datalist: _extends({
                        list: []
                    }, this.props.datalist, {
                        onSuggestionSelected: this.onSuggestionSelected
                    }),
                    input: _extends({
                        autoCapitalize: "none",
                        className: "sb_input",
                        name: "search_keyword",
                        type: "search"
                    }, this.props.input)
                };

                return _react2.default.createElement(
                    Form,
                    _extends({ innerRef: function innerRef(el) {
                            _this2.form = el;
                        } }, configObj.form),
                    _react2.default.createElement(_input2.default, { input: _extends({}, configObj.input), datalist: _extends({}, configObj.datalist) }),
                    _react2.default.createElement(
                        _button2.default,
                        { className: 'sb_submit' },
                        _react2.default.createElement(_svg_icon2.default, { icon: 'ddl_icon_search' })
                    )
                );
            }
        }]);

        return SearchBox;
    }(_react2.default.Component);

    exports.default = SearchBox;
});