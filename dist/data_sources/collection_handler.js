(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(["exports", "./ddp_api"], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require("./ddp_api"));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.ddp_api);
        global.collection_handler = mod.exports;
    }
})(this, function (exports, _ddp_api) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _ddp_api2 = _interopRequireDefault(_ddp_api);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var collection_handler = {
        addCollection: function addCollection(collectionName) {

            var subscriptions = {},
                isReady = false,
                recordsHash = {};

            _ddp_api2.default.on("ready", function (message) {

                message.subs.forEach(function (subId) {
                    var subscr = subscriptions[subId];

                    if (subscr) {
                        isReady = true;
                        subscr.cb(Object.values(recordsHash));
                    }
                });
            });

            _ddp_api2.default.on("added", function (message) {
                if (message.collection == collectionName) {
                    recordsHash[message.id] = message.fields;
                    if (isReady) {
                        for (var subscr in subscriptions) {
                            subscr.cb(Object.values(recordsHash));
                        }
                    }
                }
            });

            _ddp_api2.default.on("changed", function (message) {
                console.warn("Changed");
                console.warn(message);
            });

            return {
                addSubscription: function addSubscription(subscriptionName, paramsArr, cb) {
                    var id = _ddp_api2.default.sub(subscriptionName, paramsArr);

                    console.warn(paramsArr);

                    subscriptions[id] = {
                        id: id,
                        name: subscriptionName,
                        cb: cb
                    };

                    isReady = false;
                }
            };
        }
    };

    exports.default = collection_handler;
});