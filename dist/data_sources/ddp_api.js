(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(["exports", "ddp.js"], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require("ddp.js"));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.ddp);
        global.ddp_api = mod.exports;
    }
})(this, function (exports, _ddp) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _ddp2 = _interopRequireDefault(_ddp);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var ddpConn = new _ddp2.default({
        // endpoint:"wss://nowledge.xyz:9000/websocket",
        endpoint: "ws://ddpapi.nowledge.xyz/websocket",
        // endpoint:"ws://localhost:3400/websocket",
        // endpoint:"ws://52.51.184.63:2800/websocket",
        SocketConstructor: WebSocket
    });

    ddpConn.on("connected", function () {
        console.log("Connected");
    });

    var callbacksHash = {};

    ddpConn.on("result", function (message) {
        console.log("result", message);
        callbacksHash["_index_" + message.id](message);

        delete callbacksHash[message.id];
    });

    ddpConn.exec = function (method, params, cb) {
        var mId = "_index_" + ddpConn.method(method, params);
        callbacksHash[mId] = cb;
    };

    exports.default = ddpConn;
});