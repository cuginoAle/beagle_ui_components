(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(["exports", "./collection_handler"], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require("./collection_handler"));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.collection_handler);
        global.top_keywords = mod.exports;
    }
})(this, function (exports, _collection_handler) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _collection_handler2 = _interopRequireDefault(_collection_handler);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var Keywords = _collection_handler2.default.addCollection("Top_keywords"

    //subscriptions
    );exports.default = {
        popularSearches: function popularSearches() {
            var paramsArr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
            var cb = arguments[1];

            Keywords.addSubscription("popular_searches", paramsArr, cb);
        }
    };
});