(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(["exports", "./collection_handler"], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require("./collection_handler"));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.collection_handler);
        global.products = mod.exports;
    }
})(this, function (exports, _collection_handler) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _collection_handler2 = _interopRequireDefault(_collection_handler);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var Products = _collection_handler2.default.addCollection("Products"

    //subscriptions
    );exports.default = {
        all: function all() {
            var paramsArr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
            var cb = arguments[1];

            Products.addSubscription("all_products", paramsArr, cb);
        },
        byKeyword: function byKeyword() {
            var paramsArr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
            var cb = arguments[1];

            Products.addSubscription("products_by_keyword", paramsArr, cb);
        },
        byIsleId: function byIsleId() {
            var paramsArr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
            var cb = arguments[1];

            Products.addSubscription("products_by_aisleId", paramsArr, cb);
        },
        byShelfId: function byShelfId() {
            var paramsArr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
            var cb = arguments[1];

            Products.addSubscription("products_by_shelfId", paramsArr, cb);
        },
        by_EANBarcode: function by_EANBarcode() {
            var paramsArr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
            var cb = arguments[1];

            Products.addSubscription("products_by_EANBarcode", paramsArr, cb);
        }
    };
});