(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(["exports", "./components/accordion", "./components/js/animate", "./components/badge", "./components/button", "./components/icon_button", "./components/caption", "./components/checkbox", "./components/link", "./components/card", "./components/svg_icon", "./components/tesco_logo", "./components/tesco_monochrome_logo", "./components/code_highlighter", "./components/ios_selector", "./components/tab_menu", "./components/product_tile", "./components/spinner", "./components/message_box", "./components/modal_dialog", "./components/drawer", "./components/searchBox", "./components/searchBoxWithSuggestions", "./components/dataList", "./components/js/utility", "./components/js/styleVars"], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require("./components/accordion"), require("./components/js/animate"), require("./components/badge"), require("./components/button"), require("./components/icon_button"), require("./components/caption"), require("./components/checkbox"), require("./components/link"), require("./components/card"), require("./components/svg_icon"), require("./components/tesco_logo"), require("./components/tesco_monochrome_logo"), require("./components/code_highlighter"), require("./components/ios_selector"), require("./components/tab_menu"), require("./components/product_tile"), require("./components/spinner"), require("./components/message_box"), require("./components/modal_dialog"), require("./components/drawer"), require("./components/searchBox"), require("./components/searchBoxWithSuggestions"), require("./components/dataList"), require("./components/js/utility"), require("./components/js/styleVars"));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.accordion, global.animate, global.badge, global.button, global.icon_button, global.caption, global.checkbox, global.link, global.card, global.svg_icon, global.tesco_logo, global.tesco_monochrome_logo, global.code_highlighter, global.ios_selector, global.tab_menu, global.product_tile, global.spinner, global.message_box, global.modal_dialog, global.drawer, global.searchBox, global.searchBoxWithSuggestions, global.dataList, global.utility, global.styleVars);
        global.index = mod.exports;
    }
})(this, function (exports, _accordion, _animate, _badge, _button, _icon_button, _caption, _checkbox, _link, _card, _svg_icon, _tesco_logo, _tesco_monochrome_logo, _code_highlighter, _ios_selector, _tab_menu, _product_tile, _spinner, _message_box, _modal_dialog, _drawer, _searchBox, _searchBoxWithSuggestions, _dataList, _utility, _styleVars) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.Datalist = exports.SearchBoxWithSuggestions = exports.SearchBox = exports.Product_tile = exports.Modal_dialog = exports.Message_box = exports.Spinner = exports.Svg_icon = exports.Tab_menu = exports.IOS_selector = exports.Link = exports.Card = exports.Tesco_monochrome_logo = exports.Tesco_logo = exports.getKey = exports.Checkbox = exports.Caption = exports.Icon_button = exports.Button = exports.Badge = exports.animate = exports.Code_highlighter = exports.Drawer = exports.Accordion = exports.styleVars = exports.deb = exports.MakeSelectable = exports.WithLongTap = exports.ready = exports.$ = exports.$$ = undefined;

    var _accordion2 = _interopRequireDefault(_accordion);

    var _animate2 = _interopRequireDefault(_animate);

    var _badge2 = _interopRequireDefault(_badge);

    var _button2 = _interopRequireDefault(_button);

    var _icon_button2 = _interopRequireDefault(_icon_button);

    var _caption2 = _interopRequireDefault(_caption);

    var _checkbox2 = _interopRequireDefault(_checkbox);

    var _link2 = _interopRequireDefault(_link);

    var _card2 = _interopRequireDefault(_card);

    var _svg_icon2 = _interopRequireDefault(_svg_icon);

    var _tesco_logo2 = _interopRequireDefault(_tesco_logo);

    var _tesco_monochrome_logo2 = _interopRequireDefault(_tesco_monochrome_logo);

    var _code_highlighter2 = _interopRequireDefault(_code_highlighter);

    var _ios_selector2 = _interopRequireDefault(_ios_selector);

    var _tab_menu2 = _interopRequireDefault(_tab_menu);

    var _product_tile2 = _interopRequireDefault(_product_tile);

    var _spinner2 = _interopRequireDefault(_spinner);

    var _message_box2 = _interopRequireDefault(_message_box);

    var _modal_dialog2 = _interopRequireDefault(_modal_dialog);

    var _drawer2 = _interopRequireDefault(_drawer);

    var _searchBox2 = _interopRequireDefault(_searchBox);

    var _searchBoxWithSuggestions2 = _interopRequireDefault(_searchBoxWithSuggestions);

    var _dataList2 = _interopRequireDefault(_dataList);

    var _styleVars2 = _interopRequireDefault(_styleVars);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    exports.$$ = _utility.$$;
    exports.$ = _utility.$;
    exports.ready = _utility.ready;
    exports.WithLongTap = _utility.WithLongTap;
    exports.MakeSelectable = _utility.MakeSelectable;
    exports.deb = _utility.deb;
    exports.styleVars = _styleVars2.default;
    exports.Accordion = _accordion2.default;
    exports.Drawer = _drawer2.default;
    exports.Code_highlighter = _code_highlighter2.default;
    exports.animate = _animate2.default;
    exports.Badge = _badge2.default;
    exports.Button = _button2.default;
    exports.Icon_button = _icon_button2.default;
    exports.Caption = _caption2.default;
    exports.Checkbox = _checkbox2.default;
    exports.getKey = _utility.getKey;
    exports.Tesco_logo = _tesco_logo2.default;
    exports.Tesco_monochrome_logo = _tesco_monochrome_logo2.default;
    exports.Card = _card2.default;
    exports.Link = _link2.default;
    exports.IOS_selector = _ios_selector2.default;
    exports.Tab_menu = _tab_menu2.default;
    exports.Svg_icon = _svg_icon2.default;
    exports.Spinner = _spinner2.default;
    exports.Message_box = _message_box2.default;
    exports.Modal_dialog = _modal_dialog2.default;
    exports.Product_tile = _product_tile2.default;
    exports.SearchBox = _searchBox2.default;
    exports.SearchBoxWithSuggestions = _searchBoxWithSuggestions2.default;
    exports.Datalist = _dataList2.default;
});