(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define(["module", "./data_sources/products", "./data_sources/top_keywords"], factory);
    } else if (typeof exports !== "undefined") {
        factory(module, require("./data_sources/products"), require("./data_sources/top_keywords"));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod, global.products, global.top_keywords);
        global.data = mod.exports;
    }
})(this, function (module, _products, _top_keywords) {
    "use strict";

    var _products2 = _interopRequireDefault(_products);

    var _top_keywords2 = _interopRequireDefault(_top_keywords);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    //data_sources
    var Data = {
        Products: _products2.default,
        TopKeywords: _top_keywords2.default
    };

    module.exports = Data;
});